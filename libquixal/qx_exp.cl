// quixal - qx_exp.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// Vector exponential functions for OpenCL
//
// Required constants:
//     DIMENS - the dimensionality of the vector space (integer)
//
// Methods:
//   qx_exp(double *a, double *exp_0, double *exp_a, double *strucons, int exp_poly_order, bool isleft) calculates the
//           exponential map of `a`, using `exp(0) := exp_0`, structure constants of th algebra in `strucons`, using
//           left- or right-multiplication depending on the `isleft` flag, by approximating the Taylor polynomial up to
//           order `exp_poly_order`. The result is returned in `exp_a`. For example, if choosing left-multiplication
//           (`isleft=true`) then the first terms in the exponential map are calculated as:
//
//               `exp_a := exp_0 + a * exp_0 + (1/2!) a * (a * exp_0) + (1/3!) a * (a * (a * exo_0)) + ...`
//
//   qx_exp_g (double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order, bool isleft)
//           is the same as `qx_exp(...)`, only reading the structure constants from global memory.
//   qx_exp_left(...),
//   qx_exp_left_g(...),
//   qx_exp_right(...), and
//   qx_exp_right_g(...) are convenience methods to make it more readable whether you're using left- or
//           right-multiplication in the algebra.


void qx_exp(double *a, double *exp_0, double *exp_a, double *strucons, int exp_poly_order, bool isleft) {

  int p_ord = 1;
  double p_ord_d = 1.;

  double exp_a_prev_p_ord[DIMENS];
  double exp_a_this_p_ord[DIMENS];

  qx_copy_vector(exp_0, exp_a_this_p_ord);
  qx_copy_vector(exp_0, exp_a);

  while (p_ord <= exp_poly_order) {

    double p_ord_d_inv = 1. / p_ord_d;

    qx_mult_scalar(exp_a_this_p_ord, p_ord_d_inv, exp_a_prev_p_ord);
    qx_clear_vector(exp_a_this_p_ord);
    if (isleft) {
      qx_mult_vector(a, exp_a_prev_p_ord, exp_a_this_p_ord, strucons);
    } else {
      qx_mult_vector(exp_a_prev_p_ord, a, exp_a_this_p_ord, strucons);
    }
    qx_add_vector(exp_a, exp_a_this_p_ord, exp_a);

    p_ord_d += 1.;
    p_ord++;
  }
}


void qx_exp_g(double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order, bool isleft) {

  int p_ord = 1;
  double p_ord_d = 1.;

  double exp_a_prev_p_ord[DIMENS];
  double exp_a_this_p_ord[DIMENS];

  qx_copy_vector(exp_0, exp_a_this_p_ord);
  qx_copy_vector(exp_0, exp_a);

  while (p_ord <= exp_poly_order) {

    double p_ord_d_inv = 1. / p_ord_d;

    qx_mult_scalar(exp_a_this_p_ord, p_ord_d_inv, exp_a_prev_p_ord);
    qx_clear_vector(exp_a_this_p_ord);
    if (isleft) {
      qx_mult_vector_g(a, exp_a_prev_p_ord, exp_a_this_p_ord, strucons_g);
    } else {
      qx_mult_vector_g(exp_a_prev_p_ord, a, exp_a_this_p_ord, strucons_g);
    }
    qx_add_vector(exp_a, exp_a_this_p_ord, exp_a);

    p_ord_d += 1.;
    p_ord++;
  }
}


void qx_exp_left(double *a, double *exp_0, double *exp_a, double *strucons, int exp_poly_order) {
  qx_exp(a, exp_0, exp_a, strucons, exp_poly_order, true);
}


void qx_exp_left_g(double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order) {
  qx_exp_g(a, exp_0, exp_a, strucons_g, exp_poly_order, true);
}


void qx_exp_right(double *a, double *exp_0, double *exp_a, double *strucons, int exp_poly_order) {
  qx_exp(a, exp_0, exp_a, strucons, exp_poly_order, false);
}


void qx_exp_right_g(double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order) {
  qx_exp_g(a, exp_0, exp_a, strucons_g, exp_poly_order, false);
}


