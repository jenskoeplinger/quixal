// quixal - qx_searchlog_varneighborhood.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// For a given searcher, attempts to find a solution to exp(x) = y for a given y by performing an iterative
// local search with variable neighborhood. At the beginning of each iteration, a swarm of particles will be
// kicked off to do a limited number of random walk steps each, with different step sizes. Stepping back is
// disallowed (tabu). Depending on the best found improvement to the objective (error function), step sizes
// are adjusted for the next iteration. The iterations will abort when a solution is found, a maximum number
// of iterations threshold is exceeded, or the search has run out of bounds.
//
// Required constants:
//     DIMENS - the dimensionality of the vector space (integer)
//     NUM_PARTICLES_MAX - maximum number of particles per swarm to spawn during a single iteration
//     NUM_TABU_1_STEPS_MAX - maximum number of tabu steps (depth 1) to execute per particle in a single iteration
//
// The algorithm is documented in detail in the file README_algorithm.md
//
// Entry point is:
//     qx_searchlog_exec_varneighborhood(&searcher, &rnd_data)
//
// The searcher struct is initialized by calling:
//     void qx_searchlog_init_varneighborhood(...)
//
// See e.g. examples/ex031_032_033_searchlog_varneighborhood.cl for how to set up the many arguments.
// See qx_searchlog_varneighborhood.h, struct qx_log_varneighborhood, for more information on these parameters.


void qx_searchlog_calc_rwss_all_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    int prev_error)
{

    // get improvement on best error
    searcher->tmp_error_chg_pct = 1. - (searcher->pos_0_error / prev_error);
    searcher->pos_0_error_ema = searcher->pos_0_error_ema   * (1. - searcher->iterimprove_ema_weight)
                              + searcher->tmp_error_chg_pct * (searcher->iterimprove_ema_weight);

    if (searcher->tmp_error_chg_pct > searcher->iterimprove_substantial_pct) {
      // improvement was substantial: adjust num particles, num steps, and rwss high/low pct towards their initial defaults

      searcher->num_iterimprove_substantial = searcher->num_iterimprove_substantial + 1;

      searcher->num_particles = searcher->initial_num_particles;
      searcher->tabu_1_steps = (searcher->tabu_1_steps + searcher->initial_tabu_1_steps) / 2;
      searcher->rwss_high_pct = (searcher->rwss_high_pct + searcher->default_rwss_high_pct) / 2.;
      searcher->rwss_low_pct = (searcher->rwss_low_pct + searcher->default_rwss_low_pct) / 2.;

    } else if (searcher->tmp_error_chg_pct < searcher->iterimprove_sufficient_pct) {
      // improvement was not sufficient enough to keep searching with the same parameters

      searcher->num_iterimprove_insufficient = searcher->num_iterimprove_insufficient + 1;

      // increase the percentages at which the high (and low) random walk step sizes are above (and below) the midpoint
      // (but cap in relation to the initially requested number of steps)

      searcher->rwss_high_pct = searcher->rwss_high_pct * 1.5;
      searcher->tmp_rwss_cap_high = ((double) (searcher->initial_tabu_1_steps)) * searcher->default_rwss_high_pct;
      if (searcher->rwss_high_pct > searcher->tmp_rwss_cap_high) {
        searcher->rwss_high_pct = searcher->tmp_rwss_cap_high;
      }

      searcher->rwss_low_pct = 1. - 2. * (1. - searcher->rwss_low_pct) / 3.;
      searcher->tmp_rwss_cap_low = searcher->tmp_rwss_cap_high / (searcher->tmp_rwss_cap_high + 1.);
      if (searcher->rwss_low_pct < searcher->tmp_rwss_cap_low) {
        searcher->rwss_low_pct = searcher->tmp_rwss_cap_low;
      }

      if (searcher->pos_0_error_ema > searcher->iterimprove_sufficient_pct) {
        // While this improvement was insufficient, the exponential moving average of improvement was still OK.
        // Give it a few more random walk steps and searchers to help it along.

        searcher->num_iterimprove_insufficient_ema = searcher->num_iterimprove_insufficient_ema + 1;

        searcher->num_particles = searcher->num_particles + searcher->initial_num_particles;
        if (searcher->num_particles > NUM_PARTICLES_MAX) {
          searcher->num_particles = NUM_PARTICLES_MAX;
        }

        searcher->tabu_1_steps = searcher->tabu_1_steps + searcher->initial_tabu_1_steps;
        if (searcher->tabu_1_steps > NUM_TABU_1_STEPS_MAX) {
          searcher->tabu_1_steps = NUM_TABU_1_STEPS_MAX;
        }

      }

    }

    // (the remaining, unchecked case is a search that yielded a sufficient but not substantial improvement;
    // in that case, don't change any search parameters before the next iteration)

}


void qx_searchlog_calc_rwss_med_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    struct qx_log_swarm_rw_tabu *swarm)
{
  // From the particle that found the better solution (if so), adjust the random walk step size midpoint
  // into the direction of the improvement (same, larger, or smaller)
  // (also, tally up some telemetry on the algorithm).

  if (swarm->pos_best_particle_idx >= 0) {
    if (((swarm->particles)[swarm->pos_best_particle_idx]).random_walk_step_size < searcher->rwss_med) {
      searcher->num_rwss_decreased = searcher->num_rwss_decreased + 1;
      searcher->rwss_med = searcher->rwss_med * (1. - searcher->default_rwss_low_pct);
    } else if (((swarm->particles)[swarm->pos_best_particle_idx]).random_walk_step_size > searcher->rwss_med) {
      searcher->num_rwss_increased = searcher->num_rwss_increased + 1;
      searcher->rwss_med = searcher->rwss_med * (1. + searcher->default_rwss_high_pct);
    } else { // equality of type "double" is safe since here is the only place where it's checked and (re-)assigned
      searcher->num_rwss_unchanged = searcher->num_rwss_unchanged + 1;
    }

  } else {

    searcher->num_iterimprove_nothingfound = searcher->num_iterimprove_nothingfound + 1;
    searcher->rwss_med = searcher->rwss_med * (1. + searcher->rwss_high_pct);
    searcher->num_rwss_increased = searcher->num_rwss_increased + 1;

  }

}


bool qx_searchlog_chk_abort_varneighborhood(
    struct qx_log_varneighborhood *searcher)
{
  // See whether we've encountered an abort condition of the iteration (solution found, out of bounds,
  // or max iteration count reached).

  if (searcher->pos_0_error <= searcher->solution_found_threshold) {
    searcher->exit_code = QX_EXIT_CODE_RESULT_FOUND;
    return true;
  }

  int cdim = 0;
  while (cdim < DIMENS) {
    if ((searcher->outofbounds_abort_low[cdim] > searcher->pos_0[cdim])
        || (searcher->outofbounds_abort_high[cdim] < searcher->pos_0[cdim])) {
      searcher->exit_code = QX_EXIT_CODE_OUT_OF_BOUNDS;
      return true;
    }
    cdim++;
  }

  if (searcher->iteration_cnt >= searcher->max_iterations) {
    searcher->exit_code = QX_EXIT_CODE_MAX_ITERATIONS_EXCEEDED;
    return true;
  }

  return false;
}


void qx_searchlog_exec_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    struct qx_rnd_data *rnd_data)
{

  // main search iteration loop
  while (searcher->iteration_cnt < searcher->max_iterations) {

    (searcher->swarm->random_walk_step_sizes)[0] = searcher->rwss_med;
    (searcher->swarm->random_walk_step_sizes)[1] = searcher->rwss_med * (1. + searcher->rwss_high_pct); // rwss_high
    (searcher->swarm->random_walk_step_sizes)[2] = searcher->rwss_med * (1. - searcher->rwss_low_pct); // rwss_low;
    searcher->swarm->tabu_1_steps = searcher->tabu_1_steps;
    searcher->swarm->num_particles = searcher->num_particles;

    qx_searchlog_exec_swarm_rw_tabu(searcher->swarm, searcher->pos_0, searcher->pos_0_error, rnd_data);

    qx_copy_vector(searcher->swarm->pos_best, searcher->pos_0);
    searcher->tmp_prev_error = searcher->pos_0_error;
    searcher->pos_0_error = searcher->swarm->pos_best_error;
    searcher->iteration_cnt = searcher->iteration_cnt + 1;

    if (qx_searchlog_chk_abort_varneighborhood(searcher)) break;

    qx_searchlog_calc_rwss_med_varneighborhood(searcher, searcher->swarm);
    qx_searchlog_calc_rwss_all_varneighborhood(searcher, searcher->tmp_prev_error);

  }

}


void qx_searchlog_init_varneighborhood(

    // objective: find an x such that exp(x) = y; here, y is the target (objective)
    double *obj_target,

    // algebra:
    __global const double *strucons_g,
    int exp_poly_order,
    double *exp_0,
    bool isleft,

    // search initialization parameters:
    int initial_num_particles,
    int initial_tabu_1_steps,
    double *initial_pos,
    double initial_rwss_med,
    double default_rwss_high_pct,
    double default_rwss_low_pct,
    double iterimprove_sufficient_pct,
    double iterimprove_substantial_pct,
    double iterimprove_ema_weight,

    // iteration abort (solution found, or abort condition met):
    int max_iterations,
    double solution_found_threshold,
    double *outofbounds_abort_low,
    double *outofbounds_abort_high,

    // result (output): an initialized searcher:
    struct qx_log_varneighborhood *searcher)
{

  // allocate space for variables for swarm, initialize parameters that don't change, and link particles

  struct qx_log_swarm_rw_tabu *swarm = searcher->swarm;
  swarm->strucons_g = strucons_g;
  swarm->exp_poly_order = exp_poly_order;
  qx_copy_vector(exp_0, swarm->exp_0);
  swarm->isleft = isleft;
  qx_copy_vector(obj_target, swarm->obj_target);
  qx_copy_vector(initial_pos, swarm->pos_best);

  // allocate space for varneighborhood, initialize, and link swarm

  searcher->initial_num_particles = initial_num_particles;
  searcher->initial_tabu_1_steps = initial_tabu_1_steps;
  qx_copy_vector(initial_pos, searcher->initial_pos);
  searcher->initial_rwss_med = initial_rwss_med;
  searcher->default_rwss_high_pct = default_rwss_high_pct;
  searcher->default_rwss_low_pct = default_rwss_low_pct;
  searcher->iterimprove_sufficient_pct = iterimprove_sufficient_pct;
  searcher->iterimprove_substantial_pct = iterimprove_substantial_pct;
  searcher->iterimprove_ema_weight = iterimprove_ema_weight;

  searcher->max_iterations = max_iterations;
  searcher->solution_found_threshold = solution_found_threshold;
  qx_copy_vector(outofbounds_abort_low, searcher->outofbounds_abort_low);
  qx_copy_vector(outofbounds_abort_high, searcher->outofbounds_abort_high);

  searcher->num_particles = initial_num_particles;
  searcher->tabu_1_steps = initial_tabu_1_steps;
  qx_copy_vector(initial_pos, searcher->pos_0);
  searcher->rwss_med = initial_rwss_med;
  searcher->rwss_high_pct = default_rwss_high_pct;
  searcher->rwss_low_pct = default_rwss_low_pct;
  double function_pos_0[DIMENS];
  qx_clear_vector(function_pos_0);
  qx_exp_g(initial_pos, exp_0, function_pos_0, strucons_g, exp_poly_order, isleft);
  double pos_0_error = f_err_square_diff(function_pos_0, obj_target);
  searcher->pos_0_error = pos_0_error;
  searcher->pos_0_error_ema = iterimprove_substantial_pct;
  searcher->iteration_cnt = 0;

  searcher->exit_code = QX_EXIT_CODE_UNKNOWN;
  searcher->num_rwss_unchanged = 0;
  searcher->num_rwss_decreased = 0;
  searcher->num_rwss_increased = 0;
  searcher->num_iterimprove_insufficient = 0;
  searcher->num_iterimprove_insufficient_ema = 0;
  searcher->num_iterimprove_substantial = 0;
  searcher->num_iterimprove_nothingfound = 0;

}


