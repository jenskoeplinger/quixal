// quixal - qx_vector.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// Basic vector functions for OpenCL
//
// Required constants:
//     DIMENS - the dimensionality of the vector space (integer)
//
// Methods:
//   qx_add_vector(double *a, double *b, double *sum_ab) adds vector `a` to `b` and returns it in `sum_ab`.
//   qx_clear_vector(double *a) fills all coefficients of vector `a` with zeros.
//   qx_conjugate(double *a, double *a_bar) inverts the sign of coefficients in `a` (except `a[0]` which is unchanged).
//           Returns the result in `a_bar`.
//   qx_copy_vector(double *a, double *copy) copies all coefficients of vector `a` into vector `copy`.
//   qx_invert(double *a) inverts all coefficients of vector `a`.
//   qx_mult_dot(double *a, double *b) performs the dot product (inner product) of vectors `a` and `b`.
//   qx_mult_pairs(double *a, double *b, double *prod_ab) performs pairwise coefficient multiplication.
//   qx_mult_scalar(double *a, double scalar, double *prod_ascalar) multiplies vector `a` with "scalar" and returns
//           it in vector "prod_ascalar".
//   qx_mult_vector(double *a, double *b, double *prod_ab, double *strucons) makes the vector product of `a` and `b`,
//           given the structure constants "strucons" (in private memory space) and returns it in `prod_ab`.
//           NOTE: You must initialize the result vector `prod_ab` as a zero vector. Otherwise, the result of this
//                 vector product will be added to whatever coefficients were in `prod_ab` prior to this call.
//   qx_mult_vector_g(double *a, double *b, double *prod_ab, __global const double *strucons_g) is the same as
//           qx_mult_vector, but structure constants "strucons_g" are in global memory space.
//           NOTE: You must initialize the result vector `prod_ab` as a zero vector. Otherwise, the result of this
//                 vector product will be added to whatever coefficients were in `prod_ab` prior to this call.
//   qx_mult_vector_anticomm_g(double *a, double *b, double *prod_abplusba, __global const double *strucons_g)
//           convenience method that calculates the anticommutator, `a*b + b*a` (using `qx_mult_vector_g`).
//   qx_mult_vector_comm_g(double *a, double *b, double *prod_abminusba, __global const double *strucons_g)
//           convenience method that calculates the commutator, `a*b - b*a` (using `qx_mult_vector_g`).
//   qx_norm_squared(double *a) calculates the Euclidean square-norm of the vector `a`.
//   qx_norm_squared_diff(double *a, double *b) is the Euclidean square-norm of the difference vector of `a` and `b`
//   qx_unit_vector(double *a, int dimens) fills vector `a` with zeroes, except for the coefficient at dimension
//           "dimens" which will be filled with 1.


void qx_add_vector(double *a, double *b, double *sum_ab) {
  int i = 0;
  while (i < DIMENS) {
    sum_ab[i] = a[i] + b[i];
    i++;
  }
}


void qx_clear_vector(double *a) {
  int i = 0;
  while (i < DIMENS) {
    a[i] = 0;
    i++;
  }
}


void qx_conjugate(double *a, double *a_bar) {
  a_bar[0] = a[0];
  int i = 1;
  while (i < DIMENS) {
    a_bar[i] = -a[i];
    i++;
  }
}


void qx_copy_vector(double *a, double *copy) {
  int i = 0;
  while (i < DIMENS) {
    copy[i] = a[i];
    i++;
  }
}


void qx_invert(double *a) {
  int i = 0;
  while (i < DIMENS) {
    a[i] = -a[i];
    i++;
  }
}


double qx_mult_dot(double *a, double *b) {
  int i = 0;
  double dot_product = 0;
  while (i < DIMENS) {
    dot_product += a[i] * b[i];
    i++;
  }
  return dot_product;
}


void qx_mult_pairs(double *a, double *b, double *prod_abpairs) {
  int i = 0;
  while (i < DIMENS) {
    prod_abpairs[i] = a[i] * b[i];
    i++;
  }
}


void qx_mult_scalar(double *a, double scalar, double *prod_ascalar) {
  int i = 0;
  while (i < DIMENS) {
    prod_ascalar[i] = a[i] * scalar;
    i++;
  }
}


void qx_mult_vector(double *a, double *b, double *prod_ab, double *strucons) {

  int idx_a = 0;
  int idx_ab_offset = 0;

  while (idx_a < DIMENS) {
    double a_idx_a = a[idx_a];
    int idx_b = 0;
    while (idx_b < DIMENS) {
      double b_idx_b = b[idx_b];
      int idx_s = 0;
      while (idx_s < DIMENS) {
        double strucons_idx_s = strucons[idx_ab_offset + idx_s];
        prod_ab[idx_s] = prod_ab[idx_s] + a_idx_a * b_idx_b * strucons_idx_s;
        idx_s++;
      }
      idx_b++;
      idx_ab_offset += DIMENS;
    }
    idx_a++;
  }
}


void qx_mult_vector_g(double *a, double *b, double *prod_ab, __global const double *strucons_g) {

  int idx_a = 0;
  int idx_ab_offset = 0;

  while (idx_a < DIMENS) {
    double a_idx_a = a[idx_a];
    int idx_b = 0;
    while (idx_b < DIMENS) {
      double b_idx_b = b[idx_b];
      int idx_s = 0;
      while (idx_s < DIMENS) {
        double strucons_idx_s = strucons_g[idx_ab_offset + idx_s];
        prod_ab[idx_s] = prod_ab[idx_s] + a_idx_a * b_idx_b * strucons_idx_s;
        idx_s++;
      }
      idx_b++;
      idx_ab_offset += DIMENS;
    }
    idx_a++;
  }
}


void qx_mult_vector_anticomm_g(double *a, double *b, double *prod_abplusba, __global const double *strucons_g) {
  qx_mult_vector_g(a, b, prod_abplusba, strucons_g);
  qx_mult_vector_g(b, a, prod_abplusba, strucons_g);
}


void qx_mult_vector_comm_g(double *a, double *b, double *prod_abminusba, __global const double *strucons_g) {
  qx_mult_vector_g(b, a, prod_abminusba, strucons_g);
  qx_invert(prod_abminusba);
  qx_mult_vector_g(a, b, prod_abminusba, strucons_g);
}


double qx_norm_squared(double *a) {
  int i = 0;
  double norm_squared = 0;
  while (i < DIMENS) {
    norm_squared = norm_squared + a[i] * a[i];
    i++;
  }
  return norm_squared;
}


double qx_norm_squared_diff(double *a, double *b) {
  int i = 0;
  double norm_squared_diff = 0;
  while (i < DIMENS) {
    norm_squared_diff = norm_squared_diff + (a[i] - b[i]) * (a[i] - b[i]);
    i++;
  }
  return norm_squared_diff;
}


void qx_unit_vector(double *a, int dimens) {
  qx_clear_vector(a);
  a[dimens] = 1.;
}


