// quixal - qx_search_swarm_rw_tabu.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_search_swarm_rw_tabu.cl


struct qx_particle_rw {
  double pos[DIMENS];
  double random_walk_step_size;
  double tabu_1_steps;
  double pos_best[DIMENS];
  double pos_best_error;
  int prev_step_dimens_exclude;
};


struct qx_log_swarm_rw_tabu {
  // overall swarm parameters:

  // random walk step sizes: low, medium, and high are index 0, 1, and 2 of this array, respectively:
  double random_walk_step_sizes[3]; // [0]: rwss_med, [1]: rwss_high, [2]: rwss_low
  int tabu_1_steps;

  __global const double *strucons_g;
  int exp_poly_order;
  double exp_0[DIMENS];
  bool isleft;
  double obj_target[DIMENS];

  // list of particles:
  struct qx_particle_rw particles[NUM_PARTICLES_MAX];
  int num_particles;

  // summary from random walks of each particle:
  double pos_best[DIMENS];
  double pos_best_error;
  int pos_best_particle_idx;
};


void qx_exec_random_walk_step(
    struct qx_particle_rw *particle,
    struct qx_rnd_data *rnd_data);


void qx_searchlog_exec_random_walk_tabu(
    struct qx_particle_rw *particle,

    __global const double *strucons_g,
    int exp_poly_order,
    double *exp_0,
    bool isleft,
    double *obj_target,

    struct qx_rnd_data *rnd_data);


void qx_searchlog_exec_swarm_rw_tabu(
    struct qx_log_swarm_rw_tabu *swarm,
    double *pos_0,
    double pos_0_error,

    struct qx_rnd_data *rnd_data);


double f_err_square_diff(double *function_pos, double *obj_target);


