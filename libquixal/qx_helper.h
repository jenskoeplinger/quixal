// quixal - qx_helper.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_helper.cl


void init_int_array(int *array, int size, int value);
void copy_int_array(int *from, int *to, int size);
void copy_int_array_from_global_const(__global const int *from_g, int *to, int size, int gid);
void copy_int_array_to_global(int *from, __global int *to_g, int size, int gid);
void copy_double_array_from_global_const(__global const double *from_g, double *to, int size, int gid);
void copy_double_array_to_global(double *from, __global double *to_g, int size, int gid);


