// quixal - qx_random.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_random.cl


struct qx_rnd_data {
   __global const double *rnd_double_g;
   __global const float  *rnd_float_g;
   __global const long   *rnd_long_g;
   __global const int    *rnd_int_g;
   // index and step size to entries in the random number tables (use rnd_init(...) to randomize):
   int rnd_idx_p;
   int rnd_idx_step_p;
};

void   qx_rnd_init(struct qx_rnd_data *rnd_data, int init_id);

double qx_rnd_get_double    (struct qx_rnd_data *rnd_data);
float  qx_rnd_get_float     (struct qx_rnd_data *rnd_data);
int    qx_rnd_get_int_lehmer(int random_seed);
int    qx_rnd_get_int       (struct qx_rnd_data *rnd_data);
long   qx_rnd_get_long      (struct qx_rnd_data *rnd_data);


