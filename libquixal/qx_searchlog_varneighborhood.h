// quixal - qx_searchlog_varneighborhood.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_searchlog_varneighborhood.cl


#define QX_EXIT_CODE_UNKNOWN 0
#define QX_EXIT_CODE_RESULT_FOUND 1
#define QX_EXIT_CODE_OUT_OF_BOUNDS 2
#define QX_EXIT_CODE_MAX_ITERATIONS_EXCEEDED 3

struct qx_log_varneighborhood {

  // search initialization parameters:
  int initial_num_particles;
  int initial_tabu_1_steps;
  double initial_pos[DIMENS];
  double initial_rwss_med;
  double default_rwss_high_pct;
  double default_rwss_low_pct;
  double iterimprove_sufficient_pct;
  double iterimprove_substantial_pct;
  double iterimprove_ema_weight;

  // iteration abort (solution found, or abort condition met):
  int max_iterations;
  double solution_found_threshold;
  double outofbounds_abort_low[DIMENS];
  double outofbounds_abort_high[DIMENS];

  // pointer to swarm of particles that will be spun off at each iteration
  struct qx_log_swarm_rw_tabu *swarm;

  // variable-neighborhood search state:
  int num_particles;
  int tabu_1_steps;
  double pos_0[DIMENS];
  double rwss_med;
  double rwss_high_pct;
  double rwss_low_pct;
  double pos_0_error;
  double pos_0_error_ema;
  int iteration_cnt;

  // aggregate and result helpers:
  int exit_code;
  int num_rwss_unchanged;
  int num_rwss_decreased;
  int num_rwss_increased;
  int num_iterimprove_insufficient;
  int num_iterimprove_insufficient_ema;
  int num_iterimprove_substantial;
  int num_iterimprove_nothingfound;

  // temp/ephemeral variables (to force memory allocation, as opposed to using the stack)
  double tmp_error_chg_pct;
  double tmp_rwss_cap_high;
  double tmp_rwss_cap_low;
  double tmp_prev_error;

};


void qx_searchlog_calc_rwss_all_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    int prev_error);


void qx_searchlog_calc_rwss_med_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    struct qx_log_swarm_rw_tabu *swarm);


bool qx_searchlog_chk_abort_varneighborhood(
    struct qx_log_varneighborhood *searcher);


void qx_searchlog_exec_varneighborhood(
    struct qx_log_varneighborhood *searcher,
    struct qx_rnd_data *rnd_data);


void qx_searchlog_init_varneighborhood(

    // objective: find an x such that exp(x) = y; here, y is the target (objective)
    double *obj_target,

    // algebra:
    __global const double *strucons_g,
    int exp_poly_order,
    double *exp_0,
    bool isleft,

    // search initialization parameters:
    int initial_num_particles,
    int initial_tabu_1_steps,
    double *initial_pos,
    double initial_rwss_med,
    double default_rwss_high_pct,
    double default_rwss_low_pct,
    double iterimprove_sufficient_pct,
    double iterimprove_substantial_pct,
    double iterimprove_ema_weight,

    // iteration abort (solution found, or abort condition met):
    int max_iterations,
    double solution_found_threshold,
    double *outofbounds_abort_low,
    double *outofbounds_abort_high,

    // result (output): an initialized searcher:
    struct qx_log_varneighborhood *searcher);


