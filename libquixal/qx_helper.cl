// quixal - qx_helper.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// Convenience helpers for initializing an array with a given value, copying one array into another, with
// variations for global/private memory, double/int type.
//
//   void init_int_array(int *array, int size, int value);
//   void copy_int_array(int *from, int *to, int size);
//   void copy_int_array_from_global_const(__global const int *from_g, int *to, int size, int gid);
//   void copy_int_array_to_global(int *from, __global int *to_g, int size, int gid);
//   void copy_double_array_from_global_const(__global const double *from_g, double *to, int size, int gid);
//   void copy_double_array_to_global(double *from, __global double *to_g, int size, int gid);


void init_int_array(int *array, int size, int value) {
  int idx = 0;
  while (idx < size) {
    array[idx] = value;
    idx++;
  }
}


void copy_int_array(int *from, int *to, int size) {
  int idx = 0;
  while (idx < size) {
    to[idx] = from[idx];
    idx++;
  }
}


void copy_int_array_from_global_const(__global const int *from_g, int *to, int size, int gid) {
  int idx = 0;
  while (idx < size) {
    to[idx] = from_g[size * gid + idx];
    idx++;
  }
}


void copy_int_array_to_global(int *from, __global int *to_g, int size, int gid) {
  int idx = 0;
  while (idx < size) {
    to_g[size * gid + idx] = from[idx];
    idx++;
  }
}


void copy_double_array_from_global_const(__global const double *from_g, double *to, int size, int gid) {
  int idx = 0;
  while (idx < size) {
    to[idx] = from_g[size * gid + idx];
    idx++;
  }
}


void copy_double_array_to_global(double *from, __global double *to_g, int size, int gid) {
  int idx = 0;
  while (idx < size) {
    to_g[size * gid + idx] = from[idx];
    idx++;
  }
}


