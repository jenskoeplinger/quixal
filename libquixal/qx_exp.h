// quixal - qx_exp.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_exp.cl


void qx_exp        (double *a, double *exp_0, double *exp_a,                double *strucons,   int exp_poly_order, bool isleft);
void qx_exp_g      (double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order, bool isleft);

void qx_exp_left   (double *a, double *exp_0, double *exp_a,                double *strucons,   int exp_poly_order);
void qx_exp_left_g (double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order);
void qx_exp_right  (double *a, double *exp_0, double *exp_a,                double *strucons,   int exp_poly_order);
void qx_exp_right_g(double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order);


