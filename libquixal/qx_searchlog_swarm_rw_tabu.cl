// quixal - qx_search_swarm_rw_tabu.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// For a swarm of particles, performs a local neighborhood search by making a number of random walk steps each,
// with tabu size 1 (i.e., stepping back is prohibited).
//
// Required constants:
//     DIMENS - the dimensionality of the vector space (integer)
//
// Methods:
//
//   Entry point is:
//
//   void qx_searchlog_exec_swarm_rw_tabu(
//           struct qx_log_swarm_rw_tabu *swarm,
//           double *pos_0,
//           double pos_0_error,
//           struct qx_rnd_data *rnd_data)
//
//   See e.g. examples/ex030_searchlog_swarm_rw_tabu.cl how to set up all required data:
//
//       qx_log_swarm_rw_tabu *swarm holds information on the particle swarm that is spawned, such as references
//               to all particles, random-walk step sizes, positions, tracking data for best positions, as well
//               as algebra definitions.
//       qx_particle_rw is the struct that holds information on a single particle, e.g. its random-walk step size,
//               how many steps to execute, and tracking data for best positions.
//
//    f_err_square_diff(double *function_pos, double *obj_target) is the objective function (error function) that
//            returns a value depending on the difference between the objective (obj_target) and the current sample
//            (function_pos).


void qx_exec_random_walk_step(
    struct qx_particle_rw *particle,
    struct qx_rnd_data *rnd_data)
{

  int step_dimens = qx_rnd_get_int(rnd_data) % (DIMENS * 2);
  while (step_dimens == particle->prev_step_dimens_exclude) {
    step_dimens = qx_rnd_get_int(rnd_data) % (DIMENS * 2);
  }
  particle->prev_step_dimens_exclude = (step_dimens + DIMENS) % (DIMENS * 2);

  if (step_dimens < DIMENS) {
    particle->pos[step_dimens] = particle->pos[step_dimens] + particle->random_walk_step_size;
  } else {
    particle->pos[step_dimens - DIMENS] = particle->pos[step_dimens - DIMENS] - particle->random_walk_step_size;
  }

}


void qx_searchlog_exec_random_walk_tabu(
    struct qx_particle_rw *particle,

    __global const double *strucons_g,
    int exp_poly_order,
    double *exp_0,
    bool isleft,
    double *obj_target,

    struct qx_rnd_data *rnd_data)
{

  double function_pos[DIMENS];
  double this_error;
  int step = 0;

  while (step < particle->tabu_1_steps) {
    qx_exec_random_walk_step(particle, rnd_data);

    // specific objective: sample inverses of the exponential function
    qx_exp_g(particle->pos, exp_0, function_pos, strucons_g, exp_poly_order, isleft);
    this_error = f_err_square_diff(function_pos, obj_target);

    if (this_error < particle->pos_best_error) {
      particle->pos_best_error = this_error;
      qx_copy_vector(particle->pos, particle->pos_best);
    }

    step++;
  }

}


void qx_searchlog_exec_swarm_rw_tabu(
    struct qx_log_swarm_rw_tabu *swarm,
    double *pos_0,
    double pos_0_error,

    struct qx_rnd_data *rnd_data)
{

  swarm->pos_best_error = pos_0_error;
  swarm->pos_best_particle_idx = -1;

  int particle_idx = 0;
  while (particle_idx < swarm->num_particles) {
    struct qx_particle_rw *particle = &(swarm->particles)[particle_idx];
    qx_copy_vector(pos_0, particle->pos);
    particle->random_walk_step_size = (swarm->random_walk_step_sizes)[particle_idx % 3]; // cycle through med, high, low
    particle->tabu_1_steps = swarm->tabu_1_steps;
    particle->pos_best_error = pos_0_error;
    particle->prev_step_dimens_exclude = -1; // allow any direction in the first step

    qx_searchlog_exec_random_walk_tabu(
        particle,
        swarm->strucons_g,
        swarm->exp_poly_order,
        swarm->exp_0,
        swarm->isleft,
        swarm->obj_target,
        rnd_data);

    if (particle->pos_best_error < swarm->pos_best_error) {
      swarm->pos_best_error = particle->pos_best_error;
      qx_copy_vector(particle->pos_best, swarm->pos_best);
      swarm->pos_best_particle_idx = particle_idx;
    }

    particle_idx++;
  }

}


double f_err_square_diff(double *function_pos, double *obj_target) {
  // calculate difference from unit vector [1., 0, ..., 0]
  double diff = 0;
  int i = 0;
  while (i < DIMENS) {
    diff += (function_pos[i] - obj_target[i]) * (function_pos[i] - obj_target[i]);
    i++;
  }
  return diff;
}


