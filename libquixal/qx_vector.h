// quixal - qx_vector.h
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// prototypes for qx_vector.cl


void   qx_add_vector    (double *a, double *b, double *sum_ab);
void   qx_clear_vector  (double *a);
void   qx_conjugate     (double *a, double *a_bar);
void   qx_copy_vector   (double *a, double *copy);
void   qx_invert        (double *a);

double qx_mult_dot      (double *a, double *b);
void   qx_mult_pairs    (double *a, double *b, double *prod_abpairs);
void   qx_mult_scalar   (double *a, double scalar, double *prod_ascalar);

void   qx_mult_vector   (double *a, double *b, double *prod_ab,                double *strucons);
void   qx_mult_vector_g (double *a, double *b, double *prod_ab, __global const double *strucons_g);

void   qx_mult_vector_anticomm_g (double *a, double *b, double *prod_abplusba,  __global const double *strucons_g);
void   qx_mult_vector_comm_g     (double *a, double *b, double *prod_abminusba, __global const double *strucons_g);

double qx_norm_squared(double *a);
double qx_norm_squared_diff(double *a, double *b);

void   qx_unit_vector   (double *a, int dimens);


