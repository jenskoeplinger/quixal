// quixal - qx_random.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// Functions for pseudo-random number lookup.
//
// Required constants:
//   _RND_COUNT - number of entries in the random lookup lists; is a constant due to speed of modulo division
//                NOTE: Make sure this is a PRIME NUMBER so that the random number generator may step through
//                the entire table using different step sizes at will, and still eventually use the entire table.
//
// The struct qx_rnd_data is defined in random_helper.h, and holds the random tables, as well as index
// to the current random entry, and step size after obtaining an entry.
//
// Methods:
//   qx_rnd_init(struct qx_rnd_data *rnd_data, int init_id) helps initialize the table index and step size
//   qx_rnd_get_double(struct qx_rnd_data) returns the next random double (float64) from the table
//   qx_rnd_get_float(struct qx_rnd_data) returns the next random float (float32) from the table
//   qx_rnd_get_int(struct qx_rnd_data) return the next random int (int32) from the table
//   qx_rnd_get_int_lehmer(int random_seed) returns a random integer (int32) corresponding to the given seed
//   qx_rnd_get_long(struct qx_rnd_data) return the next random long (int64) from the table
//
// See e.g. examples/ex030_searchlog_swarm_rw_tabu.py from method init_random_tables(rnd_count) how to set up
// the random number tables.


void qx_rnd_init(struct qx_rnd_data *rnd_data, int init_id) {
  // starting index to the pseudo-random numbers: pick from the long random number list itself
  // NOTE: the long random number list must be populated with high quality pseudo-random numbers

  int init_id_mod_count = init_id % _RND_COUNT;
  rnd_data->rnd_idx_p = (int) (rnd_data->rnd_long_g[init_id_mod_count] % _RND_COUNT);

  // index step in the pseudo-random numbers, to avoid sampling the same numbers in sequence (you'll still get drift,
  // though, if the random pool is too small and used over and over for the same thing)
  rnd_data->rnd_idx_step_p = (int) ((rnd_data->rnd_long_g[rnd_data->rnd_idx_p] % (_RND_COUNT - 1)) + 1);
}


double qx_rnd_get_double(struct qx_rnd_data *rnd_data) {
  rnd_data->rnd_idx_p = (rnd_data->rnd_idx_p + rnd_data->rnd_idx_step_p) % _RND_COUNT;
  return rnd_data->rnd_double_g[rnd_data->rnd_idx_p];
}


float qx_rnd_get_float(struct qx_rnd_data *rnd_data) {
  rnd_data->rnd_idx_p = (rnd_data->rnd_idx_p + rnd_data->rnd_idx_step_p) % _RND_COUNT;
  return rnd_data->rnd_float_g[rnd_data->rnd_idx_p];
}


int qx_rnd_get_int(struct qx_rnd_data *rnd_data) {
  rnd_data->rnd_idx_p = (rnd_data->rnd_idx_p + rnd_data->rnd_idx_step_p) % _RND_COUNT;
  return rnd_data->rnd_int_g[rnd_data->rnd_idx_p];
}


int qx_rnd_get_int_lehmer(int random_seed) {
  // Lehmer's generator: Given a 32 bit seed, make a multiplication by a large number in 64 bit,
  // then truncate the high digits (by modulo division of a large prime that is just small enough to be 32 bit)
  long lehmer = (((long) random_seed) * 0x10a860c1L) % 0xfffffffbL; // * 279470273, % 4294967291 ( = 2^32 - 5)
  return (int) lehmer;
}


long qx_rnd_get_long(struct qx_rnd_data *rnd_data) {
  rnd_data->rnd_idx_p = (rnd_data->rnd_idx_p + rnd_data->rnd_idx_step_p) % _RND_COUNT;
  return rnd_data->rnd_long_g[rnd_data->rnd_idx_p];
}


