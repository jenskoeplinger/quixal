# quixal - "Quixotic algebra": Idea sink for future versions

There are tons of ideas of what else could be done. This file serves as an idea sink, with more or less detail. Let me
know if some of this interests you, either to have me code it, or to code it yourself.

I've tested all examples of this version, and they perform OK (given the limitations of the hardware platform). Please
let me know if you run into a snag!

##### Table of contents

* Algebra
    * Implement generalized exponential `x^y`
    * Sample from the generalized gamma distribution
    * Allow arbitrary functions, with given coefficients of the polynomial
    * Sample from inverses of an arbitrary polynomial function
    * Sample from chained and nested functions or inverses
* Other / sundry
    * Implement different search algorithm: Simulated annealing
    * Profiling to identify bottlenecks
    * Declared runtime: Docker, ansible, ...

# Algebra

## Implement generalized exponential `x^y`

Implement a function that calculates the general exponential,

`x^y := exp(log(x) y)`

in arbitrary algebras. This will be a convenience method that first samples `log(x)`, and then multiplies each sample
with `y`.

## Sample from the generalized gamma distribution

Implement a generalized version of the Gamma distribution for arbitrary algebras. The general structure of the Gamma
distribution is `(a^(bx)) * (cx)^d`, where `a,b,c,d` are constants and `x` the variable. This is now generalized by
definition to:

`g(x; a, b, c, d) := exp(log(a) * (bx)) * (exp(log(cx) * d)`.

For each work item, a single value of such a `g(x; a, b, c, d)` is sampled, by sampling a single value of the logarithms
each, and then forming the product as defined in the given algebra. If either search of the logarithm doesn't yield a
solution, there will be no solution for the expression altogether.

## Allow arbitrary functions, with given coefficients of the polynomial

The exponential function is currently calculated in `qx_exp` (`.cl` / `.h`) from a polynomial where the coefficient of
the next-order term is calculated by multiplying the current-order term's coefficient by

`double p_ord_d_inv = 1. / p_ord_d`

In the reals and complexes, this corresponds to the Taylor series of the exponential function around zero.

Implement a separate library `qx_poly` that takes polynomial coefficients as inputs. Just as in `qx_exp`, the library
needs to implement left- and right-multiplication methods (since algebras are generally not
power-associative, `x(xx) != (xx)x`). The library also needs to provide methods where the structure constants may
be `__global const` or private.

The libraries' six functions must have the same signature as the functions in `qx_exp`, i.e.:

```text
void qx_poly        (double *a, double *poly_coeff, double *poly_a,                double *strucons,   int poly_order, bool isleft);
void qx_poly_g      (double *a, double *poly_coeff, double *poly_a, __global const double *strucons_g, int poly_order, bool isleft);
void qx_poly_left   (double *a, double *poly_coeff, double *poly_a,                double *strucons,   int poly_order);
void qx_poly_left_g (double *a, double *poly_coeff, double *poly_a, __global const double *strucons_g, int poly_order);
void qx_poly_right  (double *a, double *poly_coeff, double *poly_a,                double *strucons,   int poly_order);
void qx_poly_right_g(double *a, double *poly_coeff, double *poly_a, __global const double *strucons_g, int poly_order);
```

Whereas in `qx_exp` the second parameter is a pointer to a single vector (`*exp_0`) that is seeded with the value
of `exp(0)`, this second parameter will now be a pointer (`*poly_coeff`) to all polynomial coefficients. Because these
coefficients will most likely get very small with increasing order, they will need to be seeded with _relative_ factors
as compared to previous nonzero coefficients, and not their absolute value. This mimics the logic in place at the
exponential function (`qx_exp(...)`) and reduces over-/underflow situations for large/small arguments and high
polynomial orders.

Example:

* If re-implementing the exponential function (which needs to be coded as an example as well, for regression testing),
  the pointer `*poly_coeff` needs to point to a list of vectors `[E, E, E/2, E/3, E/4, ...]` where
  `E = (1, 0, ..., )` the unit vector in the lowest dimension. These `*poly_coeff` will therefore replace the following
  lines in `qx_exp(...)` and multiply with the corresponding vector in `*poly_coeff` instead:

```text
    double p_ord_d_inv = 1. / p_ord_d;
    qx_mult_scalar(exp_a_this_p_ord, p_ord_d_inv, exp_a_prev_p_ord);
```

* Often the polynomials approximating a function contain coefficients that are 0. For example, the real sine function
  has a Taylor series expansion around 0 that is: `x - x^3 / 3! + x^5 / 5! - x^7 / 7! + ...` and so on. In order to
  allow for coefficients that are zero, the `*poly_coeff` pointer will be seeded such any nonzero coefficient is to be
  calculated relative to the previous nonzero coefficient, i.e.: `[0, E, 0, -E/(2*3), 0, E/(4*5), 0, -E/(6*7), ...`.

Implementation alternative: Maybe the flexibility of providing the unit element `E` with each coefficient is not needed.
It requires vector multiplication with every previous nonzero coefficient vector, in order to determine the next one.
This slows down computation with increasing dimensionality of the algebra. While it is still required to provide the
value at 0 (here: `E`), the subsequent values that `*poly_coeff` points to may instead be scalars. This would make the
values that `*poly_coeff` points to a mixed type: First a vector (`E`), then a list of scalars (one for each polynomial
order).

_Important_: The function signatures of the `qx_poly` methods _must_ match the signatures of the `qx_exp` methods, since
these functions will be passed as function pointers in following feature requirements.

While the functionality in `qx_poly` supersedes the functionality in `qx_exp`, I would nevertheless want to keep both
implementations in a "v1.0" feature set. This may allow to easier fine tune the algorithms for sampling inverses
separately, where sometimes it is easier to first tune a special case (here: exponentiation and logarithm), without
having to worry about the general case.

That said, at some later time, the `qx_exp` library may well be deprecated, and all existing code, examples, and
documentation refactored to using `qx_poly`.

## Sample from inverses of an arbitrary polynomial function

Similar to the functionality that samples from the inverse of the exponential function, implement sampling of functions
represented by arbitrary polynomials.

New function types will be defined that match the signatures of each of the functions in the `qx_exp` and `qx_poly`
libraries. A new metaheuristic search library will be implemented that parallels the existing one (which is coded
specifically for finding a logarithm.

The new library(-ies) will accept pointers to the functions from which inverses are to be sampled from, as well as,
pointers to the error function (objective function) that scores each candidate solution. The legacy functionality from
earlier versions needs to be re-implemented as an example, to serve as a regression test.

## Sample from chained and nested functions or inverses

Implement sampling results from arbitrary functions and inverses, which now may be chained and nested. This generalizes
sampling the generalized Gamma distribution from above,
`g(x; a, b, c, d) := exp(log(a) * (bx)) * (exp(log(cx) * d)`, which will become a special chain-and-nesting case (two
exponential functions, two logarithms, and five products). It needs to be re-implemented as an example, to serve as a
regression test.

# Other / sundry

## Implement different search algorithm: Simulated annealing

See whether the complex variable neighborhood algorithm can be replaced by a straightforward simulated annealing
searcher: An ensemble of searchers is initialized at a high temperature, performing one random step per iteration, and
accepting that step if it yields an improvement. With each iteration, the "temperature" of all searchers in the ensemble
is lowered, i.e., the size of the random step decreases over time. Continue until a certain threshold is reached. At
that point, searchers may or may not have found a solution.

## Profiling to identify bottlenecks

At times, compute times varies widely between different GPU compute engines, as well as on the same compute engine when
making seemingly inconsequential changes to runtime parameters. Enable profiling to identify bottlenecks (shared /
global memory access, 64-bit floating point arithmetic support, workgroup size, size of kernel, function overhead, ...).

## Declared runtime: Docker, ansible, ...

Provide a scripted runtime (e.g. docker container, ansible script) that gets all examples going without workstation
config (although that may be a pipe dream, e.g., for GPU acceleration you typically need drivers install in the host
OS _as well as_ in the container/runtime, and so far I could get containerization to work only for NVidia GPU).
