# quixal - ex030_searchlog_swarm_rw_tabu.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# This example is testing a subsystem of the algorithm that samples from inverses of a function. You
# probably shouldn't call directly into the quixal methods as it is done here. This example serves to
# confirm proper functioning of detail aspects of the algorithm (randomizer, random walk steps with tabu size 1
# per particle, swarm consolidation of the objective function).
#
# Given a starting position, each work GPU item spawns off a swarm of particles that independently perform a
# number of random walk steps. During these steps, the particles calculate the objective function
# (error function) which quantifies how close (or far away) from a suitable function inverse each
# position would be. The particles track their best position and value, respectively, and keep
# walking until a given number of steps is reached. At that point, the swarm identifies the particle
# with the best position (best objective function, lowest error function value).
#
# Starting with the simple problem of finding complex inverses to exp(x)=1, i.e., to sample from the
# branches of the complex logarithm, the aspects of the algorithm tested here are first run with minimal
# settings, and the result is plotted. Subsequent runs increase the number of random walk steps
# (1, 10, 100), the number of particles per swarm (1, 10, 100), and total iterations (30, 50). The plots
# visually confirm the proper execution of the aspects of the algorithm tested here, and its ability to
# converge on the correct results.
#
# The functionality tested here is a subset of the actual algorithm to sample from inverses of a function.
# Nevertheless, in certain "well-behaved" scenarios, it converges on proper solutions (if inefficiently).
# For demonstration, this example also samples inverses of exp(x) = 1 in the split-complex numbers
# (only one solution exists, x=0), the quaternions (solutions lie on 3D sphere surfaces spanned by the
# imaginary axes, which sphere radius an integral multiple of 2pn), and the split-quaternions (solutions
# lie on a certain mix of hyperbolae and planes in 4D).
#
# Solution samples are shown from the octonions and split-octonions as well. They're of the same kind as
# in the (split-)quaternions, only more of them.
#
# References:
#   - Particle swarm optimization https://en.wikipedia.org/wiki/Particle_swarm_optimization
#   - Random walk https://en.wikipedia.org/wiki/Random_walk
#   - Tabu search https://en.wikipedia.org/wiki/Tabu_search

import math
import random
import time

import numpy as np
import pyopencl as cl

from helpers import qx_exp_cl_helper as exp_cl
from helpers import qx_vector_cl_helper as v_cl
from helpers import strucons_helper as strucons
from helpers.common_helper import get_context, get_platform_device_info, build_program, printi, show_plot


# ========================================
# main()

def main():
    """
    Code starts executing here. All helper functions are afterwards for readability.
    """

    v_count = 1000  # how many vectors to test
    v_maxcoeff = 5. * math.pi  # maximum absolute value of an individual coefficient
    exp_poly_order = 100  # order of the polynomial approximating the exponential function (here: to the 100th power)

    # complex numbers
    xlabel = 'real part of x, re(x)'
    ylabel = 'imaginary part of x, imag(x)'
    strucons_np, v_dimens, exp_0, algebra = strucons.complex_numbers()

    # Prepare random number lookup tables
    rnd_count = 111119  # 111,119 random numbers in the tables (the size must be a prime number)
    rnd_dict = init_random_tables(rnd_count)

    random_seed = int(time.time_ns() / 1000000000)
    random.seed(random_seed)

    title = make_title(f'local search start positions (random seed = {random_seed}, count = {v_count})')
    pos_0_np, pos_0_vecs_np = get_random_vectors(v_count, v_dimens, v_maxcoeff)
    show_plot(title, xlabel, ylabel, pos_0_vecs_np)

    scenarios_to_test = [
        # Objective is to find the complex logarithm of 1, log(1), in the [-5pi, 5pi] coefficient range:
        #   - the objective function at (x) is the absolute error between |exp(x)| and 1,
        #   - call into GPU qx_searchlog_exec_swarm_rw_tabu(...) with successively deeper searches
        #     - start with one particle in the swarm and one random walk step
        #       - run this for a high number of swarms, then plot and quantify the objective function each
        {"num_particles": 1, "tabu_1_steps": 1, "iterate_from_previous_best": 30},
        #     - increase the number of random walk steps, first to 10 and then to 100
        #       - run this again for a high number of swarms; the objective function should shown an improvement
        {"tabu_1_steps": 10},
        {"tabu_1_steps": 100},
        #     - now increase the number of particles per swarm, first to 10 and then to 100
        #       - the objective function should show a significant improvement
        {"num_particles": 10, "tabu_1_steps": 1},
        {"num_particles": 100},
        #     - finally, set a higher number of particles, random walk steps, and iteration count
        #       - confirm that the objective function indicates the 5 known solutions in the search space
        {"num_particles": 10, "tabu_1_steps": 10},
        {"num_particles": 20, "tabu_1_steps": 20, "iterate_from_previous_best": 50},
        #   - split-complex numbers: exp(x) = 1 only has one solution x=0
        {"test_special": "split-complex", "v_maxcoeff": 10,
         "ylabel": "split-imaginary part of x, split-imag(x)"},
        # quaternions: exp(x) = 1 is on 3D shells in the imaginary dimensions, with radius N*2pi
        {"test_special": "quaternion", "v_maxcoeff": 15,
         "x_axis_idx": 1, "y_axis_idx": 2, "xlabel": "i-imaginary part of x", "ylabel": "j-imaginary part of x",
         "num_particles": 20, "tabu_1_steps": 20, "iterate_from_previous_best": 50},
        # ... start searchers near the (i, j) plane to see the structure in that plane
        {"test_special": "quaternion", "v_maxcoeff": 15,
         "x_axis_idx": 1, "y_axis_idx": 2, "xlabel": "i-imaginary part of x", "ylabel": "j-imaginary part of x",
         "cap_dimens": [0, 3], "cap_coeff": .2, "subtitle": "(all searchers started near the (i, j) plane)",
         "num_particles": 10, "tabu_1_steps": 10, "iterate_from_previous_best": 30},
        # split-quaternions: exp(x) = 1 has hyperbolic solution spaces; check all combinations of real,
        # imaginary, and split-imaginary axes
        {"test_special": "split-quaternion",
         "x_axis_idx": 0, "y_axis_idx": 1, "xlabel": "re(x)", "ylabel": "i-imaginary part of x",
         "cap_dimens": [], "subtitle": "",
         "num_particles": 20, "tabu_1_steps": 20, "iterate_from_previous_best": 50},
        {"test_special": "split-quaternion", "use_previous_simulation_for_plot": True,
         "y_axis_idx": 2, "ylabel": "j-split-imaginary part of x"},
        {"test_special": "split-quaternion", "use_previous_simulation_for_plot": True,
         "x_axis_idx": 1, "xlabel": "i-imaginary part of x"},
        {"test_special": "split-quaternion", "use_previous_simulation_for_plot": True,
         "x_axis_idx": 3, "xlabel": "k-split-imaginary part of x"},
        # ... start searchers in the (i, j) plane to see the structure in that plane
        {"test_special": "split-quaternion", "use_previous_simulation_for_plot": False,
         "x_axis_idx": 1, "xlabel": "i-imaginary part of x", "y_axis_idx": 2, "ylabel": "j-split-imaginary part of x",
         "cap_dimens": [0, 3], "subtitle": "(all searchers started near the (i, j) plane)",
         "num_particles": 10, "tabu_1_steps": 10, "iterate_from_previous_best": 30},
        # (split-)octonions contain the same types of solution spaces as (split-)quaternions, only more of them.
        # Because this example projects all out-of-plane dimensions into the plot, it gets a bit messy.
        # Therefore, start all searchers near the (e3, e4) plane.
        {"test_special": "octonion",
         "x_axis_idx": 3, "y_axis_idx": 4, "xlabel": "e3-imaginary part of x", "ylabel": "e4-imaginary part of x",
         "cap_dimens": [0, 1, 2, 5, 6, 7], "subtitle": "(all searchers started near the (e3, e4) plane)"},
        {"test_special": "split-octonion",
         "x_axis_idx": 3, "y_axis_idx": 4, "ylabel": "e4-split-imaginary part of x"}
    ]

    # Initialize start values (may be overridden, in the order of test parameters provided)
    num_particles = 1
    tabu_1_steps = 1
    iterate_from_previous_best = 30
    random_walk_step_size = 0.1
    x_axis_idx = 0
    y_axis_idx = 1
    silent = False
    pos_best_vecs_np_array = None
    cap_dimens = None
    cap_coeff = None
    subtitle = ""

    for scenario in scenarios_to_test:
        num_particles = scenario.get("num_particles") or num_particles
        tabu_1_steps = scenario.get("tabu_1_steps") or tabu_1_steps
        iterate_from_previous_best = scenario.get("iterate_from_previous_best") or iterate_from_previous_best
        random_walk_step_size = scenario.get("random_walk_step_size") or random_walk_step_size
        x_axis_idx = scenario.get("x_axis_idx") or x_axis_idx
        y_axis_idx = scenario.get("y_axis_idx") or y_axis_idx
        xlabel = scenario.get("xlabel") or xlabel
        ylabel = scenario.get("ylabel") or ylabel
        v_maxcoeff = scenario.get("v_maxcoeff") or v_maxcoeff
        test_special = scenario.get("test_special")
        cap_dimens = scenario.get("cap_dimens") if scenario.get("cap_dimens") is not None else cap_dimens
        cap_coeff = scenario.get("cap_coeff") if scenario.get("cap_coeff") is not None else cap_coeff
        subtitle = scenario.get("subtitle") if scenario.get("subtitle") is not None else subtitle
        if test_special == "split-complex":
            strucons_np, v_dimens, exp_0, algebra = strucons.split_complex_numbers()
        elif test_special == "quaternion":
            strucons_np, v_dimens, exp_0, algebra = strucons.quaternions()
        elif test_special == "split-quaternion":
            strucons_np, v_dimens, exp_0, algebra = strucons.split_quaternions()
        elif test_special == "octonion":
            strucons_np, v_dimens, exp_0, algebra = strucons.octonions()
        elif test_special == "split-octonion":
            strucons_np, v_dimens, exp_0, algebra = strucons.split_octonions()

        pos_0_np, pos_0_vecs_np = get_random_vectors(
            v_count, v_dimens, v_maxcoeff, cap_dimens=cap_dimens, cap_coeff=cap_coeff)

        if not scenario.get("use_previous_simulation_for_plot"):
            pos_best_vecs_np_array = None

        pos_best_vecs_np_array = run_test_simulations_with_interim(
            pos_0_np, v_count, v_maxcoeff,
            v_dimens, strucons_np, algebra, exp_poly_order,
            num_particles, tabu_1_steps, random_walk_step_size,
            rnd_dict,
            iterate_from_previous_best,
            xlabel, ylabel, x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx,
            silent=silent,
            use_previous_simulation_for_plot=pos_best_vecs_np_array,
            subtitle=subtitle
        )

        silent = True


# ========================================
# helper functions

def run_test_simulations_with_interim(
        pos_0_np, v_count, v_maxcoeff,
        v_dimens, strucons_np, algebra, exp_poly_order,
        num_particles, tabu_1_steps, random_walk_step_size,
        rnd_dict,
        iterate_from_previous_best,
        xlabel, ylabel,
        silent,
        size_x=7, size_y=7,
        x_axis_idx=0, y_axis_idx=1,
        use_previous_simulation_for_plot=None,
        subtitle=""
):
    """
    Runs some test simulations with fixed random walk step size, iteratively such that
    the best results from the previous iteration become the starting points of the next one.
    This won't be the actual algorithm (which will include a variable neighborhood search
    and adjust the random walk step size) but is meant to test and confirm that the random
    walk, number of random walk steps with tabu size 1, and particle swarms work as intended.
    """

    if subtitle is not None and subtitle != "":
        subtitle = f"\n{subtitle}"

    title = make_title(
        f'{v_count} independent swarms '
        f'performing {iterate_from_previous_best} iteration{"s" if iterate_from_previous_best != 1 else ""} of random walk\n'
        f'Objective: sample {algebra} inverses of exp(x) = 1 (i.e., log 1)\n'
        f'({num_particles} particle{"s" if num_particles != 1 else ""} per swarm, '
        f'{tabu_1_steps} tabu step{"s" if tabu_1_steps != 1 else ""} per particle, '
        f'step size {random_walk_step_size})'
        f'{subtitle}')

    if use_previous_simulation_for_plot is None:
        printi('Start simulation ...')

        pos_best_vecs_np_list = []
        for this_iter in range(iterate_from_previous_best):
            if silent:
                print('.', end='')
                if ((this_iter + 1) % 10 == 0):
                    print(f'{this_iter + 1}/{iterate_from_previous_best}', end='')
            pos_best_vecs_np, pos_best_error_np, pos_best_particle_idx_np = \
                opencl_build_and_run(
                    pos_0_np, v_count, v_dimens, strucons_np, exp_poly_order,
                    num_particles, tabu_1_steps, random_walk_step_size,
                    rnd_dict,
                    verbose=(this_iter == 0),
                    silent=silent)
            # replace all coefficients greater than the search space with zero
            pos_best_vecs_np = np.where(pos_best_vecs_np > v_maxcoeff, 0, pos_best_vecs_np)
            pos_best_vecs_np = np.where(pos_best_vecs_np < -v_maxcoeff, 0, pos_best_vecs_np)
            # record this iteration's best positions, and prepare them as "position 0" for the next iteration
            pos_best_vecs_np_list.append(pos_best_vecs_np)
            pos_0_np = pos_best_vecs_np.reshape((v_count * v_dimens))

        pos_best_vecs_np_array = np.array(pos_best_vecs_np_list)
        printi('Done.')

    else:
        pos_best_vecs_np_array = use_previous_simulation_for_plot

    show_plot(title, xlabel, ylabel, pos_best_vecs_np_array,
              size_x=size_x, size_y=size_y,
              c=get_grays_for_iteration(iterate_from_previous_best),
              x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx)

    return pos_best_vecs_np_array


def get_grays_for_iteration(iter_count):
    # gray scale from 10% through black, getting darker with increasing iter_count
    shades = [str(math.sqrt(((iter_count - (cnt + 1)) / 1.1) / iter_count))
              for cnt in range(0, iter_count)]
    return shades


def get_random_vectors(v_count, v_dimens, v_maxcoeff, nonreal=False,
                       cap_dimens=None, cap_coeff=None):
    # create vectors with random numbers
    a_np = np.random.rand(v_count * v_dimens).astype(np.float64)
    if nonreal:
        # only return nonreal values, i.e., set the real component to 0
        a_np[0::v_dimens] = 0

    a_np = a_np * 2. * v_maxcoeff - v_maxcoeff  # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range

    if cap_dimens is not None:
        # restrict certain dimensions to a minimum and maximum value
        for this_dimens in cap_dimens:
            a_np[this_dimens::v_dimens] = a_np[this_dimens::v_dimens] * (cap_coeff / v_maxcoeff)

    a_vecs_np = a_np.reshape((v_count, v_dimens))  # reshaped as v_count vectors of dimension v_dimens
    return a_np, a_vecs_np


def make_title(title):
    # primitive builder to print the title and echo it back
    printi(f'- {title}')
    return title


def init_random_tables(rnd_count, random_seed=None, randlong_high=1.e9, randint_high=1.e5):
    """
    Initializes the random number lookup tables.

    Inputs:
      - rnd_count - size of the pseudo-number tables
      - random_seed (optional) - set to specific number to guarantee the same pseudo-number tables
      - randint_high (optional) - upper bound for random integer (default: 1.e5)
      - randlong_high (optional) - upper bound for random long (default: 1.e9)
    """

    if random_seed is None:
        random_seed = int(time.time_ns() / 1000000000)

    printi(f'Setting up {rnd_count} random numbers (float32, float64, int32, and int64). Random seed: {random_seed}.')
    random.seed(random_seed)

    # create random number lookup tables as one-dimensional numpy arrays
    rnd_float = np.random.rand(rnd_count).astype(np.float32)
    rnd_double = np.random.rand(rnd_count).astype(np.float64)
    rnd_int_pre = np.random.rand(rnd_count).astype(np.float32)
    rnd_int = (rnd_int_pre * randint_high).astype(np.int32)
    rnd_long_pre = np.random.rand(rnd_count).astype(np.float64)
    rnd_long = (rnd_long_pre * randlong_high).astype(np.int64)

    return {'rnd_float': rnd_float,
            'rnd_double': rnd_double,
            'rnd_int': rnd_int,
            'rnd_long': rnd_long}


def opencl_build_and_run(pos_0_np, v_count, v_dimens, strucons_np, exp_poly_order,
                         num_particles, tabu_1_steps, random_walk_step_size,
                         rnd_dict, verbose=True, silent=False):
    """
    OpenCL context, command queue, memory flags, input/output, platforms, devices, build GPU program and create kernel.

    Inputs:
      pos_0_np - list of v_count local search start vectors in dimension v_dimens, represented as 1D numpy array;
                 one particle swarm will start on each start vector
      strucons_np - structure constants describing the algebra, represented as 1D numpy array
      exp_poly_order - polynomial order up to which to approximate the power series
      num_particles - number of particles per swarm
      tabu_1_steps - number of random walk steps each particle will do with tabu size 1 (i.e. stepping back prohibited)
      random_walk_step_size - how far to go on each step
      rnd_dict - dictionary with random number lookup tables (float32, float64, int32, and int64)

    Outputs:
      pos_best_vecs_np - list of v_count best found positions
      pos_best_error - list of errors (values from the objective function)
      pos_best_particle_idx - list of particle index on which the best result (lowest error) was found
    """

    if silent:
        verbose = False

    v_cl.v_dimens = v_dimens
    exp_cl.v_dimens = v_dimens

    rnd_int = rnd_dict['rnd_int']
    rnd_long = rnd_dict['rnd_long']
    rnd_count = len(rnd_long)

    cl_info = get_platform_device_info()
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose)

    # inputs
    pos_0_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=pos_0_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
    rnd_int_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=rnd_int)
    rnd_long_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=rnd_long)

    # outputs
    pos_best_np = np.empty_like(pos_0_np)
    pos_best = cl.Buffer(ctx, mf.WRITE_ONLY, pos_best_np.nbytes)
    pos_best_error_np = np.empty(v_count, dtype=np.float64)
    pos_best_error = cl.Buffer(ctx, mf.WRITE_ONLY, pos_best_error_np.nbytes)
    pos_best_particle_idx_np = np.empty(v_count, dtype=np.int32)
    pos_best_particle_idx = cl.Buffer(ctx, mf.WRITE_ONLY, pos_best_particle_idx_np.nbytes)

    # code
    LIBQUIXAL_DIR = "../libquixal/"

    prg = build_program(
        ctx,
        defines_list=[f'DIMENS {v_dimens}', f'_RND_COUNT {rnd_count}', f'NUM_PARTICLES_MAX {num_particles}'],
        files_list=[f'{LIBQUIXAL_DIR}qx_vector.h', f'{LIBQUIXAL_DIR}qx_exp.h',
                    f'{LIBQUIXAL_DIR}qx_random.h',
                    f'{LIBQUIXAL_DIR}qx_searchlog_swarm_rw_tabu.h',
                    f'{LIBQUIXAL_DIR}qx_vector.cl', f'{LIBQUIXAL_DIR}qx_exp.cl',
                    f'{LIBQUIXAL_DIR}qx_random.cl',
                    f'{LIBQUIXAL_DIR}qx_searchlog_swarm_rw_tabu.cl',
                    'ex030_searchlog_swarm_rw_tabu.cl'],
        verbose=verbose
    )
    knl = prg.test_searchlog_swarm_rw_tabu

    # --------------------
    if not silent:
        printi(f'Call kernel in GPU {device.name}'
               f' [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}] ...')

    knl(queue, [v_count], None,
        pos_0_g, np.int32(exp_poly_order), np.int32(num_particles), np.int32(tabu_1_steps),
        np.float64(random_walk_step_size),
        np.int32(np.random.randint(0, 1e5)),
        strucons_g,
        rnd_int_g, rnd_long_g,
        pos_best, pos_best_error, pos_best_particle_idx)

    cl.enqueue_copy(queue, pos_best_np, pos_best)
    cl.enqueue_copy(queue, pos_best_error_np, pos_best_error)
    cl.enqueue_copy(queue, pos_best_particle_idx_np, pos_best_particle_idx)

    if not silent:
        printi('... done.')
    # --------------------

    # reshape the result as v_count vectors of dimension v_dimens
    pos_best_vecs_np = pos_best_np.reshape((v_count, v_dimens))

    return pos_best_vecs_np, pos_best_error_np, pos_best_particle_idx_np


# ========================================
# execute

main()
