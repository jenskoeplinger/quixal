# quixal - ex015_octooctonion_multiplication.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# This example tests some advanced algebraic properties from tensoring composition algebras from the previous
# examples. Here, "tensoring" is meant to pick two algebras A and B, and use elements from algebra A as coefficients
# to algebra B. This can be written as A(x)B. The resulting vector space of A(x)B has the dimensionality of the product
# of the dimensions of vectors spaces from A and B, and elements in A(x)B can be understood as the Cartesian product
# between all elements in A and B.
#
# As an example, the "bicomplex numbers" can be understood as two tensored copies of the complexes, C(x)C.
# You could write a regular complex number a := a_r + a_i i, where a_r and a_i are real number coefficients, and
# "i" the imaginary axis, i*i = -1. For a bicomplex number you replace these real number a_r and a_i with
# complex numbers B_R and B_I, and write e.g. B := B_R + B_I i. Spelled out explicitly, these B_R and B_I would be
# B_R := B_Rr + B_Ri i' and B_I := B_Ir + B_Ii i' with all of the { B_Rr, B_Ri, B_Ir, B_Ii } now real numbers again.
# Note that the imaginary axis "i'" here is different from the imaginary axis "i" of the bicomplex number
# B := B_R + B_I i. Both imaginary axes, "i" and "i'", satisfy i*i = i'*i' = -1. But they are separate and have
# to be treated fully independently (i.e., they commute and associate). This means that the resulting basis for the
# bicomplex numbers has four basis vectors: {1, i, i', i*i'}, i.e., the dimension of the vector space (and with that,
# the algebra) is 2*2 = 4 as required.
#
# Some of the algebras analyzed here occur e.g. in the Freudenthal magic square, for the construction of certain
# Lie algebras and their Lie groups. In particular, octonions are used in the construction of the exceptional
# Lie groups F_4, E_6, E_7, and E_8.
#
# Taken three elements from the algebra, {a, b, c}, the following tests are then executed in GPU code (see function
# `get_algebra_property_description(...)` for translation of the result bitmap to human-friendly display):
#
#   - commutativity: a * b = b * a
#   - associativity: (a * b) * c = a * (b * c)
#   - alternativity: (a * a) * b = a * (a * b)
#   - flexibility: (a * b) * a = a * (b * a)
#   - power-associativity (3-factor): (a * a) * a = a * (a * a)
#   - power-associativity (4-factor): ((a * a) * a) * a = (a * a) * (a * a)
#
# (Note: Two tests for power-associativity are both needed to conclude that the entire algebra is power-associative)
#
# In addition, the code also tests whether one, both, no none of the factors are zero. If that's the case, then
# the samples fall into a "null space" (kernel) of the algebra.
#
# Two additional properties are returned in order to fully understand what's going on in a particular sample:
#
#   - nilpotence: a * a = 0 (if this is true then both indicators for power-associativity will always hold and be null)
#   - the interim result of b * c if it 0 (to understand the (non)associative null spaces from the "a" and "b")
#
# More info:
#   - bicomplex numbers, CxC: https://en.wikipedia.org/wiki/Bicomplex_number
#   - "Dixon Algebra" (Rx)CxHxO after G. M. Dixon, "Division Algebras: Octonions Quaternions Complex Numbers and
#      the Algebraic Design of Physics", Kluwer 1994; or "Towards a unified theory of ideals" (2010; paper by C. Furey
#      at https://arxiv.org/abs/1002.1497)
#   - Freudenthal magic square: https://en.wikipedia.org/wiki/Freudenthal_magic_square
#   - "null space" (kernel of an algebra): https://en.wikipedia.org/wiki/Kernel_(linear_algebra)
#   - power-associativity: https://en.wikipedia.org/wiki/Power_associativity

import pprint
import random

import numpy as np
import pyopencl as cl
import time

from helpers.common_helper import printi, get_context, get_platform_device_info
from helpers.strucons_helper import real_numbers, complex_numbers, quaternions, octonions, split_octonions
from helpers.strucons_helper import tensor_algs

# Debug output flags
DEBUG_PRINT_STRUCONS = False  # If true, prints the structure constants of the tensored algebra
DEBUG_PRINT_ALG_PROPS_RAW = False  # If true, prints the algebra properties bitmap results for all tests raw
DEBUG_PRINT_NUM_EXAMPLES = 1  # Set to the number of examples to show for each unique algebra property

# Enable to show lots of detail in stdout (e.g. for visual inspection, troubleshooting)
np.set_printoptions(threshold=999999)
np.set_printoptions(linewidth=300)

# Base factor for number of tests (it will be multiplied as needed later). The effective number of
# tests scales linearly with this factor:
BASE_NUM_TESTS = 1


def main():
    """
    Code execution starts here.
    """

    test_algebras = [
        {'name': 'complex numbers',
         'algebras': [real_numbers, complex_numbers],
         'verbose': True},
        {'name': 'bicomplex numbers ("Tessarines", CxC)',
         'algebras': [complex_numbers, complex_numbers],
         'verbose': False},
        {'name': 'quaternions',
         'algebras': [real_numbers, quaternions],
         'verbose': False},
        {'name': 'quad-quaternions (HxH)',
         'algebras': [quaternions, quaternions],
         'verbose': False},
        # this algebra is commented out, since it's 256-dimensional it would take a bit of time
        # {'name': 'quaternary quaternions ("quatquats", HxHxHxH)',
        #  'algebras': [quaternions, quaternions, quaternions, quaternions],
        #  'verbose': False},
        {'name': 'octonions',
         'algebras': [real_numbers, octonions],
         'verbose': False},
        {'name': 'complex octonions (CxO)',
         'algebras': [complex_numbers, octonions],
         'verbose': False},
        {'name': 'quad-octonions (HxO)',
         'algebras': [quaternions, octonions],
         'verbose': False},
        {'name': 'complex quad-octonions ("Dixon algebra", CxHxO)',
         'algebras': [complex_numbers, quaternions, octonions],
         'verbose': False},
        {'name': 'octo-octonions (OxO)',
         'algebras': [octonions, octonions],
         'verbose': False},
        {'name': 'split-octo-octonions (O\'xO)',
         'algebras': [split_octonions, octonions],
         'verbose': False},
        {'name': 'split-octo-split-octonions (O\'xO\')',
         'algebras': [split_octonions, split_octonions],
         'verbose': False}
    ]

    # If you want to test only a subset of algebras, match the "name" key in test_algebras:
    test_only_names = None
    # test_only_names = ['quaternary quaternions ("quatquats", HxHxHxH)']
    # test_only_names = ['octo-octonions (OxO)']

    force_first_verbose = (test_only_names is not None)

    for this_algebra_dict in test_algebras:
        this_alg_name = this_algebra_dict["name"]

        if test_only_names is not None and this_alg_name not in test_only_names:
            printi(f'(selected to test only algebra(s) "{test_only_names}", skipping {this_alg_name}')
            continue

        printi(f'----------------------------------------------')
        printi(f'>>>> {this_alg_name.upper()}')
        printi(f'----------------------------------------------')

        strucons_np, v_dimens = None, None
        v_dimens_list = []  # list of dimensions, for vectorized human-friendly display later

        algebras = this_algebra_dict["algebras"]
        printi(f'Build tensor product of {len(algebras)} algebras to form {this_alg_name}')

        for this_alg in algebras:
            if strucons_np is None:
                strucons_np, v_dimens, exp_0, name = this_alg()
                printi(f'  - starting with: {name}')
                v_dimens_list.append(v_dimens)
            else:
                this_strucons_np, this_v_dimens, this_exp_0, this_name = this_alg()
                printi(f'  - ... tensoring with: {this_name} (on the "right", with fast-moving index)')
                strucons_np, v_dimens = tensor_algs(strucons_np, v_dimens, this_strucons_np, this_v_dimens)
                v_dimens_list.append(this_v_dimens)

        if DEBUG_PRINT_STRUCONS:
            printi(f'   Got {v_dimens}-dimensional algebra with structure constants (linear):\n{strucons_np}')
            strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
            printi(f'   Structure constants as vectors:\n{strucons_vec_np}')

        # -----------------------------------
        # First, test some random vectors. These should expose the typical algebra properties. For division algebras,
        # random products rarely are in a null space (since their dimensionality is smaller than the dimensionality
        # of the vector space).

        num_tests = BASE_NUM_TESTS * 100
        v_maxcoeff = 1.
        a_np, b_np, c_np = create_random_vectors(v_dimens, num_tests, v_maxcoeff)
        random_type = f'all coefficients random between -{v_maxcoeff} and {v_maxcoeff}'

        execute_test(num_tests, random_type, this_alg_name,
                     a_np, b_np, c_np,
                     strucons_np,
                     v_dimens, v_dimens_list,
                     verbose=(this_algebra_dict['verbose'] or force_first_verbose))

        force_first_verbose = False

        # -----------------------------------
        # Second, test select vectors that are of a kind that could like in a null space in the tested algebra.
        # This is of course not an exhaustive test, and vectors need to be chosen to fit the particular algebra.
        # Here, we only probe for existence of null spaces.

        num_tests_base = BASE_NUM_TESTS * 10

        for number_of_ones in range(1, 9):
            # create random vectors that have a coefficient 1. or -1. occurring exactly number_of_ones times
            # (if the algebra doesn't have enough dimensions, abort)

            if number_of_ones > v_dimens:
                break

            num_tests = num_tests_base * number_of_ones * v_dimens

            a_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones)
            b_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones)
            c_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones)
            random_type = f'vectors on unit grid with {number_of_ones} nonzero coefficients (1. or -1.)'

            execute_test(num_tests, random_type, this_alg_name,
                         a_np, b_np, c_np,
                         strucons_np,
                         v_dimens, v_dimens_list)

            if number_of_ones > 1:  # pointless if number_of_ones == 1
                a_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones, force_real_one=True)
                b_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones, force_real_one=True)
                c_np = create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones, force_real_one=True)
                random_type = f'vectors on unit grid with {number_of_ones} nonzero coefficients (1. or -1.), ' \
                              f'with real axis (0-dimension) forced to 1.'

                execute_test(num_tests, random_type, this_alg_name,
                             a_np, b_np, c_np,
                             strucons_np,
                             v_dimens, v_dimens_list)


def create_random_vectors(v_dimens, num_tests, v_maxcoeff):
    """
    Create three sets of vectors with random numbers in v_dimens dimensions.

    Input:
      - v_dimens: vector space dimension
      - num_tests: number of vectors to create in each set

    Returns:
      - a_np, b_np, c_np: three sets of num_tests vectors each
    """

    random_seed = int(time.time_ns() / 1000000000)
    printi(f'Setting up {num_tests} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
    random.seed(random_seed)
    a_np = np.random.rand(v_dimens * num_tests).astype(np.float64)
    b_np = np.random.rand(v_dimens * num_tests).astype(np.float64)
    c_np = np.random.rand(v_dimens * num_tests).astype(np.float64)
    # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
    a_np = a_np * 2. * v_maxcoeff - v_maxcoeff
    b_np = b_np * 2. * v_maxcoeff - v_maxcoeff
    c_np = c_np * 2. * v_maxcoeff - v_maxcoeff

    return a_np, b_np, c_np


def create_random_vectors_on_unit_grid(v_dimens, num_tests, number_of_ones, force_real_one=False):
    """
    create random vectors that have a coefficient 1. or -1. occurring exactly number_of_ones times
    """

    rand_np = np.zeros(num_tests * v_dimens).astype(np.float64)

    for this_num in range(num_tests):

        available_dimens = list(range(v_dimens))  # [0, 1, ..., (v_dimens - 1)]
        if force_real_one:
            # forces the 0-dimension of the vector to be 1.
            this_dimens = available_dimens.pop(0)
            rand_np[this_num * v_dimens + this_dimens] = 1
            start_cnt = 1
        else:
            start_cnt = 0

        for cnt in range(start_cnt, number_of_ones):  # cnt = [0,] 1, ..., (number_of_ones - 1)
            dimens_idx = random.randrange(v_dimens - cnt)
            this_dimens = available_dimens.pop(dimens_idx)
            plusminus = (random.randrange(2) * 2) - 1
            rand_np[this_num * v_dimens + this_dimens] = plusminus

    return rand_np


def create_gpu_kernel_and_call(a_np, b_np, c_np,
                               strucons_np,
                               v_dimens,
                               verbose=False):
    """
    Performs all GPU functionality: Assembles the code, compiles it, prepares the input and output buffers,
    loads the input to the GPU, runs the code, reads the output from the GPU, and returns the output.
    """

    printi('Create OpenCL context, command queue, memory flags, input/output parameter buffer', verbose=verbose)
    cl_info = get_platform_device_info()
    printi(f'Available OpenCL platforms and devices:\n\n{pprint.pformat(cl_info)}\n', verbose=verbose)
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose=verbose)
    printi(f'Selected environment:\n\n{pprint.pformat(env)}\n', verbose=verbose)

    # Input parameters:
    v_count = int(a_np.size / v_dimens)
    printi(f'Number of vectors: {v_count}', verbose=verbose)
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
    c_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=c_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)

    # Return parameters:
    alg_props_np = np.zeros(v_count).astype(np.int32)
    number_of_bytes = alg_props_np.nbytes
    alg_props = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

    # Read static OpenCL files (.cl) into string.
    LIBQUIXAL_DIR = "../libquixal/"

    defines = f"""
    #define DIMENS {v_dimens}
    """

    with open(f'{LIBQUIXAL_DIR}qx_vector.h', 'r') as vector_h:
        prg_vector_h = vector_h.read()
    with open(f'{LIBQUIXAL_DIR}qx_vector.cl', 'r') as vector_cl:
        prg_vector_cl = vector_cl.read()
    with open('ex015_octooctonion_multiplication.cl', 'r') as file_vector_multiplication_cl:
        prg_kernel_cl = file_vector_multiplication_cl.read()

    prg_text = defines \
               + '\n\nint test_two_vectors(double *a_vec, double *b_vec, int equal_offset);\n\n' \
               + prg_vector_h \
               + prg_vector_cl \
               + prg_kernel_cl

    printi(f'Build GPU program and create kernels:\n{prg_text}\n', verbose=verbose)
    prg = cl.Program(ctx, prg_text).build()
    knl = prg.vector_multiplication

    printi('Call kernel in GPU ...', verbose=verbose)
    knl(queue, [v_count], None, a_g, b_g, c_g, strucons_g, alg_props)
    printi('... read result ...', verbose=verbose)
    cl.enqueue_copy(queue, alg_props_np, alg_props)
    printi('... done.', verbose=verbose)

    return alg_props_np


def get_algebra_property_description(alg_prop):
    """
    Given an algebra property bitmap, returns a list of strings that describe these properties.
    """

    bitmap_dict = {
        0: 'commutative: a * b = b * a',
        1: 'associative: (a * b) * c = a * (b * c)',
        2: 'alternative: (a * a) * b = a * (a * b)',
        3: 'flexible: (a * b) * a = a * (b * a)',
        4: 'power-associative (3-factor): (a * a) * a = a * (a * a)',
        5: 'power-associative (4-factor): ((a * a) * a) * a = (a * a) * (a * a)',

        8: 'commutative null space: a * b = b * a = 0',
        9: 'associative null space: (a * b) * c = a * (b * c) = 0',
        10: 'alternative null space: (a * a) * b = a * (a * b) = 0',
        11: 'flexible null space: (a * b) * a = a * (b * a) = 0',
        12: 'power-associative null space (3-factor): (a * a) * a = a * (a * a) = 0',
        13: 'power-associative null space (4-factor): ((a * a) * a) * a = (a * a) * (a * a) = 0',
        14: 'nilpotent: a * a = 0',

        16: 'noncommutative null space: a * b not b * a, with a * b = 0',
        17: 'nonassociative null space: (a * b) * c not a * (b * c), with (a * b) * c = 0',
        18: 'nonalternative null space: (a * a) * b not a * (a * b), with (a * a) * b = 0',
        19: 'nonflexible null space: (a * b) * a not a * (b * a), with (a * b) * a = 0',
        20: 'non-power-associative null space (3-factor): (a * a) * a not a * (a * a), with (a * a) * a = 0',
        21: 'non-power-associative null space (4-factor): ((a * a) * a) * a not (a * a) * (a * a), with ((a * a) * a) * a = 0',
        22: 'interim result FYI: b * c = 0',

        24: 'noncommutative null space: a * b not b * a, with b * a = 0',
        25: 'nonassociative null space: (a * b) * c not a * (b * c), with a * (b * c) = 0',
        26: 'nonalternative null space: (a * a) * b not a * (a * b), with a * (a * b) = 0',
        27: 'nonflexible null space: (a * b) * a not a * (b * a), with a * (b * a) = 0',
        28: 'non-power-associative null space (3-factor): (a * a) * a not a * (a * a), with a * (a * a) = 0',
        29: 'non-power-associative null space (4-factor): ((a * a) * a) * a not (a * a) * (a * a), with (a * a) * (a * a) = 0',
    }

    ret_s = []
    for bit in bitmap_dict:
        if (alg_prop & (1 << bit)):
            ret_s.append(f'[{bit:2d}] {bitmap_dict[bit]}')

    if len(ret_s) == 0:
        ret_s = [
            '[--] not commutative, associative, alternative, flexible, or power-associative (3- and 4-factor); not in null space'
        ]

    return ret_s


def execute_test(num_tests, random_type, alg_name,
                 a_np, b_np, c_np,
                 strucons_np,
                 v_dimens, v_dimens_list,
                 verbose=False):
    """
    Given a set of test vectors (random, curated, ...) tests their algebraic properties, prints the result.
    """

    printi(f'Testing algebraic properties in {alg_name} from {num_tests} random samples:')
    printi(f'    {random_type}')

    alg_props_np = create_gpu_kernel_and_call(a_np, b_np, c_np,
                                              strucons_np,
                                              v_dimens,
                                              verbose=verbose)

    if DEBUG_PRINT_ALG_PROPS_RAW:
        printi(f'Raw result:\n{alg_props_np}')
        printi(f'Raw result as bitmap:\n{np.vectorize(np.binary_repr)(alg_props_np, width=32)}')

    if DEBUG_PRINT_NUM_EXAMPLES is not None and DEBUG_PRINT_NUM_EXAMPLES > 0:
        # example printing is requested; prepare human-friendly display:
        #   - vectors in the final (tensored) algebra; the right-most algebra has the fastest-moving index
        a_vecs_np = a_np.reshape((num_tests, v_dimens))
        b_vecs_np = b_np.reshape((num_tests, v_dimens))
        c_vecs_np = c_np.reshape((num_tests, v_dimens))
        #   - vectors written as tensors in the constituent algebras
        individual_alg_shape = [num_tests]
        individual_alg_shape.extend(v_dimens_list)
        a_vecs_individual_alg_np = a_np.reshape(individual_alg_shape)
        b_vecs_individual_alg_np = b_np.reshape(individual_alg_shape)
        c_vecs_individual_alg_np = c_np.reshape(individual_alg_shape)

    else:
        # no human-friendly example display requested; save some time
        a_vecs_np = None
        b_vecs_np = None
        c_vecs_np = None
        a_vecs_individual_alg_np = None
        b_vecs_individual_alg_np = None
        c_vecs_individual_alg_np = None

    # get the count of algebra properties
    unique, counts = np.unique(alg_props_np, return_counts=True)
    alg_props_with_count = np.column_stack((unique, counts))

    printi(f'These {num_tests} samples had the following property(-ies) in {alg_name} ({random_type}):')

    for prop_w_c in alg_props_with_count:
        test_prop = prop_w_c[0]
        test_count = prop_w_c[1]
        test_prop_descs = get_algebra_property_description(prop_w_c[0])
        test_pct = 100. * float(test_count) / float(num_tests)
        printi(f'  - {test_count}/{num_tests} ({test_pct:.1f}%) '
               f'had result bitmap {np.binary_repr(test_prop, width=32)} (integer: {test_prop}) '
               f'in {alg_name} ({random_type}):')
        for this_desc in test_prop_descs:
            printi(f'      {this_desc}')

        if DEBUG_PRINT_NUM_EXAMPLES is not None and DEBUG_PRINT_NUM_EXAMPLES > 0:
            # print a few samples

            test_prop_idx_list = np.where(alg_props_np == test_prop)[0]
            printi(f'  - print up to {DEBUG_PRINT_NUM_EXAMPLES} examples (out of the {test_count} found):')
            for example_idx in range(min(DEBUG_PRINT_NUM_EXAMPLES, test_prop_idx_list.size)):
                this_idx = test_prop_idx_list[example_idx]
                printi(f'    sample at index {this_idx}:')
                printi(f'      a := {a_vecs_np[this_idx]} = \n{a_vecs_individual_alg_np[this_idx]}')
                printi(f'      b := {b_vecs_np[this_idx]} = \n{b_vecs_individual_alg_np[this_idx]}')
                printi(f'      c := {c_vecs_np[this_idx]} = \n{c_vecs_individual_alg_np[this_idx]}')

    return alg_props_np


# -------------------------

# all functions defined; start code execution

main()
