// quixal - ex001_hello_world.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by ex001_hello_world.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space (here: 1)
//
// Required include:
//    libquixal/qx_vector.cl


__kernel void hello_world(
    __global const double *a_g,
    __global const double *b_g,
    __global double *res_g)
{

  // All this example does is add two numbers, which are single coefficients at
  // a_g[0] and b_g[0], and return the result in res_g[0]. The OpenCL code for
  // this could simply be one line:
  //
  //    res_g[0] = a_g[0] + b_g[0]
  //
  // This example shows how to use the quixal method qx_add_vector(...), which requires arguments in private
  // memory space of each work item. In this primitive example, which doesn't use any GPU parallelism, this would be
  // pointless because the code will only be called once. It's just for demonstration.

  double a_vec[1]; // define an array with a single element, in private memory space of this work item
  double b_vec[1];

  a_vec[0] = a_g[0];
  b_vec[0] = b_g[0];

  // perform the calculation

  double result_vec[1];
  qx_add_vector(a_vec, b_vec, result_vec);

  // put result back into the global output

  res_g[0] = result_vec[0];

}


