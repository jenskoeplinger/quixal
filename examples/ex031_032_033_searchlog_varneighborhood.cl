// quixal - ex031_032_033_searchlog_varneighborhood.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by ex031_searchlog_varneighborhood.py, ex032_searchlog_varneighborhood_general.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space
//    _RND_COUNT - size of the random number tables (random.cl)
//    NUM_PARTICLES_MAX - maximum number of particles per swarm
//    NUM_TABU_1_STEPS_MAX - maximum number of random walk steps (tabu size 1)
//
// Required include:
//    libquixal/qx_helper.cl
//    libquixal/qx_vector.cl
//    libquixal/qx_exp.cl
//    libquixal/qx_random.cl
//    libquixal/qx_searchlog_swarm_rw_tabu.cl
//    libquixal/qx_searchlog_varneighborhood.cl

__kernel void test_searchlog_varneighborhood(

    // INPUTS

    // objective: find an x such that exp(x) = y; here, y is the target (objective)
    __global const double *obj_target_g,

    // algebra:
    __global const double *strucons_g,
    int exp_poly_order,
    __global const double *exp_0_g,
    int isleft_int,

    // search initialization parameters:
    int initial_num_particles,
    int initial_tabu_1_steps,
    __global const double *initial_pos_g,
    double initial_rwss_med,
    double default_rwss_high_pct,
    double default_rwss_low_pct,
    double iterimprove_sufficient_pct,
    double iterimprove_substantial_pct,
    double iterimprove_ema_weight,

    // iteration abort (solution found, or abort condition met):
    int max_iterations,
    double solution_found_threshold,
    __global const double *outofbounds_abort_low_g,
    __global const double *outofbounds_abort_high_g,

    // randomizer:
    int random_seed,
    __global const int *rnd_int_g,
    __global const long *rnd_long_g,

    // OUTPUTS

    __global double *pos_best_g,
    __global double *pos_best_error_g,

    __global int *iteration_cnt_g,
    __global int *exit_code_g,
    __global int *num_rwss_unchanged_g,
    __global int *num_rwss_decreased_g,
    __global int *num_rwss_increased_g,
    __global int *num_iterimprove_insufficient_g,
    __global int *num_iterimprove_insufficient_ema_g,
    __global int *num_iterimprove_substantial_g,
    __global int *num_iterimprove_nothingfound_g)
{

  int gid = get_global_id(0);

  // set up randomizer

  struct qx_rnd_data rnd_data;
  rnd_data.rnd_int_g = rnd_int_g;
  rnd_data.rnd_long_g = rnd_long_g;
  qx_rnd_init(&rnd_data, gid + random_seed);

  // prepare searcher struct, copy global parameters to private memory where needed

  struct qx_log_varneighborhood searcher;
  struct qx_log_swarm_rw_tabu swarm;
  searcher.swarm = &swarm;

  double obj_target[DIMENS];
  double exp_0[DIMENS];
  double initial_pos[DIMENS];
  double outofbounds_abort_low[DIMENS];
  double outofbounds_abort_high[DIMENS];

  copy_double_array_from_global_const(obj_target_g, obj_target, DIMENS, 0);
  copy_double_array_from_global_const(exp_0_g, exp_0, DIMENS, 0);
  copy_double_array_from_global_const(initial_pos_g, initial_pos, DIMENS, gid);
  copy_double_array_from_global_const(outofbounds_abort_low_g, outofbounds_abort_low, DIMENS, 0);
  copy_double_array_from_global_const(outofbounds_abort_high_g, outofbounds_abort_high, DIMENS, 0);

  bool isleft = (isleft_int > 0); // work around some OpenCL weirdness when passing booleans to the kernel

  qx_searchlog_init_varneighborhood(
      obj_target,
      strucons_g, exp_poly_order, exp_0, isleft,
      initial_num_particles, initial_tabu_1_steps, initial_pos,
      initial_rwss_med, default_rwss_high_pct, default_rwss_low_pct,
      iterimprove_sufficient_pct, iterimprove_substantial_pct, iterimprove_ema_weight,
      max_iterations, solution_found_threshold,
      outofbounds_abort_low, outofbounds_abort_high,
      &searcher);

  // execute the search

  qx_searchlog_exec_varneighborhood(&searcher, &rnd_data);

  // distribute result (and telemetry) into the global output

  copy_double_array_to_global(searcher.pos_0, pos_best_g, DIMENS, gid);
  pos_best_error_g[gid] = searcher.pos_0_error;

  iteration_cnt_g[gid] = searcher.iteration_cnt;
  exit_code_g[gid] = searcher.exit_code;
  num_rwss_unchanged_g[gid] = searcher.num_rwss_unchanged;
  num_rwss_decreased_g[gid] = searcher.num_rwss_decreased;
  num_rwss_increased_g[gid] = searcher.num_rwss_increased;
  num_iterimprove_insufficient_g[gid] = searcher.num_iterimprove_insufficient;
  num_iterimprove_insufficient_ema_g[gid] = searcher.num_iterimprove_insufficient_ema;
  num_iterimprove_substantial_g[gid] = searcher.num_iterimprove_substantial;
  num_iterimprove_nothingfound_g[gid] = searcher.num_iterimprove_nothingfound;

}


