# quixal - ex012_split-complex_multiplication.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate random samples of split-complex number multiplication.
# This file performs the identical calculation as in ex011_complex multiplication.py,
# but here using the structure constants representing split-complex multiplication.
#
#   - Get two lists of split-complex numbers, represented as a 2D vector each,
#   - define the structure constants for split-complex number multiplication
#     (i.e., the multiplication table),
#   - for each pair of numbers, calculate the split-complex number product,
#   - test it against what would be expected from Python; include a few "friendly"
#     tests for known pairs, e.g. "j * j = 1", "(1 + j) * (1 + j) = 2 + 2j"
#     (i.e. from one of the ideals of the algebra), and "(1 - j) * (1 + j) = 0" (i.e. from its
#     zero-divisor subspace).
#
# For more information see:
#   - split-complex number https://en.wikipedia.org/wiki/Split-complex_number
#   - ideal of an algebra https://en.wikipedia.org/wiki/Ideal_(ring_theory)
#   - zero-divisors https://en.wikipedia.org/wiki/Zero_divisor

import random
import time

import numpy as np

from ex011_012_013_014_vector_multiplication import execute_vector_multiplication
from helpers.common_helper import printi

v_dimens = 2  # dimensionality the vector space (split-complex numbers are 2D)
v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 5.  # maximum absolute value of an individual coefficient

num_tests = 1000  # how many spot-wise tests to do once finished
num_friendly_tests = 4  # output the first few tests human-friendly at the end (for visual inspection)

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Setting up {v_count} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_dimens * v_count).astype(np.float64)
b_np = np.random.rand(v_dimens * v_count).astype(np.float64)
# scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff
b_np = b_np * 2. * v_maxcoeff - v_maxcoeff

# Make the first four vector pairs special, so that we can test these simple examples just by looking at them
#   - "j * j = 1", here represented as [0, 1] * [0, 1] = [1, 0]
a_np[0] = 0
a_np[1] = 1
b_np[0] = 0
b_np[1] = 1
#   - "(1 + j) * (1 + j) = 2 + 2j", represented as [1, 1] * [1, 1] = [2, 2] (with index shift +2)
a_np[2] = 1
a_np[3] = 1
b_np[2] = 1
b_np[3] = 1
#   - "(1 - j) * (1 + j) = 0", represented as [1, -1] * [1, 1] = [0, 0] (with index shift +4)
a_np[4] = 1
a_np[5] = -1
b_np[4] = 1
b_np[5] = 1
#   - "2 * 3 = 6", represented as [2, 0] * [3, 0] = [6, 0] (with index shift +6)
a_np[6] = 2
a_np[7] = 0
b_np[6] = 3
b_np[7] = 0

printi(f'Set up structure constants modeling complex number multiplication:')
strucons_np = np.array([
    #  1 * ...:
    1, 0,  # 1 * 1 = 1
    0, 1,  # 1 * j = j
    # j * ...:
    0, 1,  # j * 1 = j
    1, 0  # j * j = 1
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np
)
# ------------------------
