# quixal - ex033_searchlog_select.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# The combination of select exponential maps and logarithms may give an idea on the geometry that's
# encoded in a given algebra. This example graphs select exponential maps, `exp( b alpha )` with fixed
# vector `b` in the algebra and real `alpha`, and samples from select logarithms. You can compare
# the exponential maps with the logarithms, and find the respective inverses.
#
# There are four algebras prepared here: The bicomplex numbers (complex numbers with complex coefficients,
# understood as four-dimensional algebra over the reals), split-quaternions (as before), a "nuReal" 3D algebra
# that's investigated by John A Shuster, and the 6D algebra part of a system proposed in online discussion by
# user "lalo.sigma".
#
# References:
#   - bicomplex numbers: https://en.wikipedia.org/wiki/Bicomplex_number
#   - split-quaternions: https://en.wikipedia.org/wiki/Split-quaternion
#   - John A Shuster's "nuReals" (2016/2017) - see method get_john_s_nu_real() below
#   - user "lalo.sigma"'s 6D algebra part of a wider system - see method get_lalo1_27jan22() below

import math

import matplotlib.pyplot as plt
import numpy as np
import pyopencl as cl

import ex032_033_common
from helpers import qx_vector_cl_helper as v_cl
from helpers import strucons_helper as strucons
from helpers.common_helper import get_context, get_platform_device_info, build_program, printi, show_plot


def main():
    """
    Code starts executing here.

    After selecting one of the algebra, it first calculates values of the exponential function in
    that algebra, and then some logarithms.
    """

    test_num = int(input('Select algebra to test:\n'
                         '  1 - bicomplex numbers (i.e., complexes with complex coefficients,\n'
                         '  2 - split-quaternions,\n'
                         '  3 - nuReal numbers after John A. Shuster (2016/2017),\n'
                         '  4 - a 6D algebra proposed by user lalo.sigma online on 27 January 2022.\n'))

    # Prepare argument for testing the exponential function

    if test_num == 1:
        # -------------------------------------------------------------------------------------
        # first test: bicomplex numbers (i.e., complex numbers with complex coefficients)
        strucons_np, v_dimens, exp_0_np, algebra_disp = strucons.bicomplex()
        algebra = (strucons_np, v_dimens, exp_0_np, algebra_disp)

        # get scalar parameter
        v_maxcoeff = 10.
        np_scalar_params = prepare_scalar_parameter_samples(v_maxcoeff)

        # exponentials of the unit axes
        # - real axis x[0]: this is the real exponential function
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0, 0], verbose=True)
        # - first imaginary axis x[1]: this gives cosine and sine in the (x[0], x[1]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 1, 0, 0])
        # - split-imaginary axis x[2]: this gives the hyperbolic cosine and sine in the (x[0], x[2]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 1, 0])
        # - second imaginary axis x[3]: this gives the cosine and sine in the (x[0], x[3]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, 1])

        # sample exponentials of off-axis unit vectors:
        test_single_algebra_exp(algebra, np_scalar_params, [0, math.sqrt(1 / 2), math.sqrt(1 / 2), 0])
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, math.sqrt(1 / 2), math.sqrt(1 / 2)])

        # log(1) samples
        # (all solutions lie in the plane spanned by the imaginary axes)
        test_single_algebra_log(algebra, graph_only_xy=[(1, 3)], max_iterations=400)

    if test_num == 2:
        # -------------------------------------------------------------------------------------
        # second test: split-quaternions
        strucons_np, v_dimens, exp_0_np, algebra_disp = strucons.split_quaternions()
        algebra = (strucons_np, v_dimens, exp_0_np, algebra_disp)

        # get scalar parameter
        v_maxcoeff = 10.
        np_scalar_params = prepare_scalar_parameter_samples(v_maxcoeff)

        # exponentials of the unit axes
        # - real axis x[0]: this is the real exponential function
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0, 0], verbose=True)
        # - imaginary axis x[1]: this gives cosine and sine in the (x[0], x[1]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 1, 0, 0])
        # - first split-imaginary axis x[2]: this gives the hyperbolic cosine and sine in the (x[0], x[2]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 1, 0])
        # - second split-imaginary axis x[3]: this gives the hyperbolic cosine and sine in the (x[0], x[3]) plane
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, 1])

        # sample exponentials of off-axis unit vectors:
        test_single_algebra_exp(algebra, np_scalar_params, [0, math.sqrt(1 / 2), math.sqrt(1 / 2), 0])
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, math.sqrt(1 / 2), math.sqrt(1 / 2)])

        # log(1) samples
        # (all solutions have no real part, and are hyperboloids of two sheets in the 3 nonreal dimensions (space by
        # 2pi on the imaginary x[1] axis)
        test_single_algebra_log(algebra, graph_only_xy=[(0, 1), (1, 2), (2, 3)], max_iterations=200, v_count=10000)

    if test_num == 3:
        # -------------------------------------------------------------------------------------
        # third test: John A Shuster's nuReal numbers (as of 2021)
        algebra = get_john_s_nu_real()
        v_dimens = algebra[1]

        # get scalar parameter
        v_maxcoeff = 10.
        np_scalar_params = prepare_scalar_parameter_samples(v_maxcoeff)

        # exponentials of the vector space unit axes
        # (note: the multiplicative unit, 1 == exp(0), is (1, -1, 1) in this representation;

        # exponential of random samples across 3D
        v_count = 10000
        v_maxcoeff = 10.

        random_np, random_vecs_np = ex032_033_common.get_random_vectors(v_count, v_dimens, v_maxcoeff)
        test_single_algebra_exp_projected(algebra, random_np, v_count, verbose=True,
                                          xylim=[[-5., 5.], [-5., 5.], [-5., 5.]])

        # this is part of the algebra tuple)
        # Each of the vector space basis elements are idempotents, i.e., the exponential map must correspond to the
        # real exponential function in that dimension. The other dimensions must remain 1 (p, q) and -1 (G), respectively.
        # - exp(x[0]) is the real exponential function on the "p" axis, with q=1, G=-1 constant
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0])
        # - exp(x[1]) is the real exponential function on the "G" axis, with p=1, q=1 constant
        test_single_algebra_exp(algebra, np_scalar_params, [0, 1, 0])
        # - exp(x[2]) is the real exponential function on the "q" axis, with p=1, G=-1 constant
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 1])

        # Confirming that 1 = exp(0) = (1, -1, 1) = p - G + q is indeed the identity element in the algebra:
        test_single_algebra_exp(algebra, np_scalar_params, [1, -1, 1])

        # Exponential map of idempotents that are not in the x[0]=0 and x[2]=0 planes
        test_single_algebra_exp(algebra, np_scalar_params, [1, -2, 1])
        test_single_algebra_exp(algebra, np_scalar_params, [-1, 0, 1])
        test_single_algebra_exp(algebra, np_scalar_params, [0.3, -1, 0.7])

        # Exponential maps of some of the invertible numbers (which are most of the 3D space)
        test_single_algebra_exp(algebra, np_scalar_params, [0.5, 0.5, 0.5])
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 1])
        test_single_algebra_exp(algebra, np_scalar_params, [1, 1, -1])
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0.5, -2])
        test_single_algebra_exp(algebra, np_scalar_params, [0.3, 1, -0.2])
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0.5])

        # samples of log(id) = log((1, -1, 1)) = log(p - G + q)
        test_single_algebra_log(algebra, obj_target=[1., -1., 1.], graph_only_xy=[(0, 1)])

        # sample of log(p), i.e., log of an idempotent other than the multiplicative unit (cannot have a solution)
        test_single_algebra_log(algebra, obj_target=[1., 0, 0], graph_only_xy=[(1, 2)])

        # Same as before, but with much smaller "solution found" threshold. It shows that there are no actual
        # solutions here, the ones in the previous run were just artifacts of a power series that approximates
        # the solution somewhere towards infinity:
        test_single_algebra_log(algebra, obj_target=[1., 0, 0], graph_only_xy=[(1, 2)],
                                solution_found_threshold=1.e-12)

        # testing the logarithm of sample near the above exponentials of invertible numbers
        test_single_algebra_log(algebra, obj_target=[2.8, -2.8, 2.8],
                                solution_found_threshold=1.e-12, graph_only_xy=[(0, 1), (1, 2)])
        test_single_algebra_log(algebra, obj_target=[1.8, 1., 1.8],
                                solution_found_threshold=1.e-12, graph_only_xy=[(0, 1), (1, 2)])
        test_single_algebra_log(algebra, obj_target=[math.e, -1. / math.e, 1. / math.e],
                                solution_found_threshold=1.e-8, max_iterations=400, graph_only_xy=[(0, 1), (1, 2)])
        test_single_algebra_log(algebra, obj_target=[1.4, 1, 1],
                                solution_found_threshold=1.e-12, graph_only_xy=[(0, 1), (1, 2)])

    if test_num == 4:
        # -------------------------------------------------------------------------------------
        # fourth test: lalo.sigma's 6D algebra from 27 January 2022
        algebra = get_lalo1_27jan22()
        v_dimens = algebra[1]

        # exponential of random samples across 6D
        v_count = 10000
        v_maxcoeff = 10.
        random_np, random_vecs_np = ex032_033_common.get_random_vectors(v_count, v_dimens, v_maxcoeff)

        # Plot only the 0-dimension against all others; the algebra is symmetric, the shape of the other projections
        # can be inferred from the ones here and properly cycling through the (0,1,2) and (3,4,5) dimensions:
        test_single_algebra_exp_projected(algebra, random_np, v_count, isleft_int=0, verbose=True,
                                          xylim=[[-5., 5.], [-5., 5.], [-5., 5.], [-5., 5.], [-5., 5.], [-5., 5.]],
                                          graph_only_xy=[(0, 1), (0, 2), (0, 3), (0, 4), (3, 4)])
        # (the (0, 2) sample is already unneeded because it has to match the (0, 1) sample; it's here just to
        # show this symmetry visually)

        # Exponential function along each of the basis element, exp(b alpha) where b a basis and alpha a real number.
        # Plotting all 6 dimensions, to prove out that the structure constants are set up properly, making the
        # (0,1,2) and (3,4,5) dimensions dual to one another.
        v_mincoeff = -17.
        v_maxcoeff = 3.
        np_scalar_params = prepare_scalar_parameter_samples(v_maxcoeff, v_mincoeff=v_mincoeff)

        for basis_dimens in range(v_dimens):
            basis_vec = [0, 0, 0, 0, 0, 0]
            basis_vec[basis_dimens] = 1
            test_single_algebra_exp(algebra, np_scalar_params, basis_vec, isleft_int=0)

        # provide zooms for a representative (x[0], x[3])
        v_mincoeff = -32.
        v_maxcoeff = 8.
        np_scalar_params = prepare_scalar_parameter_samples(v_maxcoeff, v_mincoeff=v_mincoeff)

        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, 1, 0, 0], isleft_int=0,
                                ylim_min=-30., ylim_max=60., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, 1, 0, 0], isleft_int=0,
                                ylim_min=-20., ylim_max=700., exp_poly_order=150)

        # run representative exponentials with two basis elements (note the over-/underflow at very small values)
        test_single_algebra_exp(algebra, np_scalar_params, [1, 1, 0, 0, 0, 0], isleft_int=0,
                                ylim_min=-30., ylim_max=60., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [-1, 1, 0, 0, 0, 0], isleft_int=0,
                                ylim_min=-60., ylim_max=60., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0, 1, 0, 0], isleft_int=0,
                                ylim_min=-30., ylim_max=60., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [-1, 0, 0, 1, 0, 0], isleft_int=0,
                                ylim_min=-35., ylim_max=55., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [1, 0, 0, 0, 1, 0], isleft_int=0,
                                ylim_min=-30., ylim_max=30., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [-1, 0, 0, 0, 1, 0], isleft_int=0,
                                ylim_min=-15., ylim_max=15., exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, 1, 1, 0], isleft_int=0,
                                ylim_min=-5., ylim_max=5., exp_poly_order=150)

        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, -1, 1, 0], isleft_int=0,
                                ylim_min=-3.1, ylim_max=3.1, exp_poly_order=150)
        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, -1, 0, 1], isleft_int=0,
                                ylim_min=-3.1, ylim_max=3.1, exp_poly_order=150)

        # run representative exponentials with three basis elements (fuzziness indicates loss of precision)
        # left-unit (this should just be the exponential function in the (0,1,2) dimensions)
        test_single_algebra_exp(algebra, np_scalar_params, [1, 1, 1, 0, 0, 0], isleft_int=0,
                                ylim_min=-30., ylim_max=60., exp_poly_order=150)

        test_single_algebra_exp(algebra, np_scalar_params, [0, 0, 0, -1, 1, 1], isleft_int=0,
                                ylim_min=-3.1, ylim_max=3.1, exp_poly_order=150)

        # Samples of log(e_l) = log((1, 1, 1, 0, 0, 0)) = log(i + j + k).
        # Even though there's a really high solution-found threshold, the algorithm still only finds the solution
        # at 0 and not the one around 12.5 * (0, 0, 0, -1, 1, 0) that has to exist. Chances are that the dynamic
        # neighborhood search gets impatient and abandons an otherwise good path. Not sure whether this can
        # be tuned, or whether maybe a different search algorithm should be used (some simulated annealing maybe).
        test_single_algebra_log(algebra, isleft_int=0,
                                exp_poly_order=150, v_count=100,
                                max_iterations=500, show_interim=False,
                                initial_rwss_med=0.3, initial_num_particles=30, initial_tabu_1_steps=2,
                                obj_target=[1., 1., 1., 0, 0, 0],
                                v_maxcoeff=17.,
                                solution_found_threshold=1.e-2,
                                graph_only_xy=[(0, 1), (2, 3), (4, 5)])

        # Samples of log((1, 0, 0, 0, 1, 1)) = log(i + ~j + ~k).
        # Similar to log(e_l) above, the algorithm only finds one solution around -3. * (0, 0, 0, -1, 1, 0) with a
        # very poor solution-found threshold. It doesn't find the one, say, near 10. * (0, 0, 0, -1, 1, 0). Also,
        # I'm not sure whether the additional "solutions" are real or artifacts.
        test_single_algebra_log(algebra, isleft_int=0,
                                exp_poly_order=150, v_count=100,
                                max_iterations=500, show_interim=False,
                                initial_rwss_med=0.3, initial_num_particles=30, initial_tabu_1_steps=2,
                                obj_target=[1., 0, 0, 0, 1., 1.],
                                v_maxcoeff=17.,
                                solution_found_threshold=1.e-2,
                                graph_only_xy=[(0, 1), (2, 3), (4, 5)])


def get_lalo1_27jan22():
    """
    Returns the structure constants, dimension, value of exp(0), and algebra name
    of an algebra proposed by user "lalo sigma" online on 27 January 2022. The user was
    pondering a six-dimensional number system. I offered to calculate values of log(1) in
    that system, both for his interests, as well as testing my code so far under a "real-world"
    situation: An algebra has been constructed from some motivation elsewhere, and the researcher
    would like to get a quick first-look at some geometric subspaces that may be tucked-away in
    that algebra.

    Post with structure constants: https://groups.io/g/hypercomplex/message/78
    Link to all posts in this topic: https://groups.io/g/hypercomplex/topic/88664894#78

    Structure constants (all multiplied by a factor 2 for readability):

           |  i           j           k           ~i          ~j          ~k
        ---+-----------------------------------------------------------------------
        i  |  (i + ~k)    (i + j)     ( 0 )       (j + ~i)    (~i + ~k)   (j + ~k)
        j  |  ( 0 )       (j + ~i)    (j + k )    (k + ~i)    (k + ~j)    (~i + ~j)
        k  |  (i + k)     ( 0 )       (k + ~j)    (~j + ~k)   (i + ~j)    (i + ~k)
        ~i |  (k + ~i)    (~i + ~j)   ( 0 )       (i + ~j)    (i + k)     (k + ~j)
        ~j |  ( 0 )       (i + ~j)    (~j + ~k)   (i + ~k)    (j + ~k)    (i + j)
        ~k |  (~i + ~k)   ( 0 )       (j + ~k)    (j + k)     (j + ~i)    (k + ~i)

    Vector space basis is fixed as: (i, j, k, ~i, ~j, ~k)

    Because the number system is not power-associative and doesn't have a two-sided multiplicative
    unit, special care has to be taken in respect to which multiplication is used. Here we use
    right-multiplication, and the left-"identity" e_l = (1, 1, 1, 0, 0, 0) for exp_r(0) (not quite an identity
    element under multiplication, but the author interprets it as such).

       e_l = i + j + k =: exp_r(0)

    With this, the exponential map is calculated as:

       exp_r(x) = e_l + e_l*x + (1/2)(e_l*x)*x) + (1/3!)((e_l*x)*x)*x + ...

    The logarithm then is the set of inverses as usual:

       log(y) = { x | exp_r(x) = y }
    """

    v_dimens = 6
    exp_0 = [1., 1., 1., 0, 0, 0]  # (1, 1, 1, 0, 0, 0) == i + j + k == e_l the left-"identity"

    # First define all structure constants multiplied by 2 for readability:
    strucons_np = np.array([
        #  i * ...:
        1, 0, 0, 0, 0, 1,  # i * i = i + ~k
        1, 1, 0, 0, 0, 0,  # i * j = i + j
        0, 0, 0, 0, 0, 0,  # i * k = 0
        0, 1, 0, 1, 0, 0,  # i * ~i = j + ~i
        0, 0, 0, 1, 0, 1,  # i * ~j = ~i + ~k
        0, 1, 0, 0, 0, 1,  # i * ~k = j + ~k
        #  j * ...:
        0, 0, 0, 0, 0, 0,  # j * i = 0
        0, 1, 0, 1, 0, 0,  # j * j = j + ~i
        0, 1, 1, 0, 0, 0,  # j * k = j + k
        0, 0, 1, 1, 0, 0,  # j * ~i = k + ~i
        0, 0, 1, 0, 1, 0,  # j * ~j = k + ~j
        0, 0, 0, 1, 1, 0,  # j * ~k = ~i + ~j
        #  k * ...:
        1, 0, 1, 0, 0, 0,  # k * i = i + k
        0, 0, 0, 0, 0, 0,  # k * j = 0
        0, 0, 1, 0, 1, 0,  # k * k = k + ~j
        0, 0, 0, 0, 1, 1,  # k + ~i = ~j + ~k
        1, 0, 0, 0, 1, 0,  # k * ~j = i + ~j
        1, 0, 0, 0, 0, 1,  # k * ~k = i + ~k
        #  ~i * ...:
        0, 0, 1, 1, 0, 0,  # ~i * i = k + ~i
        0, 0, 0, 1, 1, 0,  # ~i * j = ~i + ~j
        0, 0, 0, 0, 0, 0,  # ~i * k = 0
        1, 0, 0, 0, 1, 0,  # ~i * ~i = i + ~j
        1, 0, 1, 0, 0, 0,  # ~i * ~j = i + k
        0, 0, 1, 0, 1, 0,  # ~i * ~k = k + ~j
        #  ~j * ...:
        0, 0, 0, 0, 0, 0,  # ~j * i = 0
        1, 0, 0, 0, 1, 0,  # ~j * j = i + ~j
        0, 0, 0, 0, 1, 1,  # ~j * k = ~j + ~k
        1, 0, 0, 0, 0, 1,  # ~j * ~i = i + ~k
        0, 1, 0, 0, 0, 1,  # ~j * ~j = j + ~k
        1, 1, 0, 0, 0, 0,  # ~j * ~k = i + j
        #  ~k * ...:
        0, 0, 0, 1, 0, 1,  # ~k * i = ~i + ~k
        0, 0, 0, 0, 0, 0,  # ~k * j = 0
        0, 1, 0, 0, 0, 1,  # ~k * k = j + ~k
        0, 1, 1, 0, 0, 0,  # ~k * ~i = j + k
        0, 1, 0, 1, 0, 0,  # ~k * ~j = j + ~i
        0, 0, 1, 1, 0, 0  # ~k * ~k = k + ~i
    ]).astype(np.float64)

    # Then divide all by a factor 2:
    strucons_np = strucons_np / 2.

    return (strucons_np, v_dimens, exp_0, "lalo1 (27-Jan-2022)")


def get_john_s_nu_real():
    """
    John A Shuster propose a number system, "nuReal" numbers, that is a commutative and associative
    algebra in three real dimensions. For the version implemented here, see e.g.:

    https://bitbucket.org/jenskoeplinger/yahoo-hypercomplex-2019-backup/src/master/GroupsIoFilesBackup/JohnAShuster_UsingNuRealsOpInstructions_v30_2016-2017.pdf

    The multiplication table (structure constants) for the three basis elements (p, G, q) is:

         | p | G | q |
       --+---+---+---+
       p | p | G | q |
       G | G | G | G |
       q | G | G | q |

    The unique unit element is

       1 = (1, -1, 1) = p - G + q
    """

    v_dimens = 3
    exp_0 = [1., -1., 1.]  # (1, -1, 1) == p - G + p
    strucons_np = np.array([
        #  p * ...:
        1, 0, 0,  # p * p = p
        0, 1, 0,  # p * G = G
        0, 1, 0,  # p * q = G
        # G * ...:
        0, 1, 0,  # G * p = G
        0, 1, 0,  # G * G = G
        0, 1, 0,  # G * q = G
        # q * ...:
        0, 1, 0,  # q * p = G
        0, 1, 0,  # q * G = G
        0, 0, 1  # q * q = q
    ]).astype(np.float64)

    return (strucons_np, v_dimens, exp_0, "nuReal (John S. 2016/2017)")


def prepare_scalar_parameter_samples(v_maxcoeff, v_mincoeff=None):
    """
    Returns a list of scalars for use in the exponential functions. Numbers in the list will have a different
    density, to serve as a visual aid:
      - across the entire parameter space there will be an equally spaced, lesser-dense set of scalars
      - near whole numbers there will be a denser set of scalars
    """

    if v_mincoeff is None:
        v_mincoeff = -v_maxcoeff
    total_interval = v_maxcoeff - v_mincoeff
    total_interval_int = int(total_interval)

    # - equally spaced solution samples across the entire parameter range
    v_count_equal = total_interval_int * 20 + 1
    # - space parameter samples more densely near whole-number values (as a visual aid)
    v_count_whole = total_interval_int - 2
    v_whole_interval = 0.1
    v_maxcoeff_whole = v_whole_interval * (1. - 1. / v_count_whole)

    # build a single (real) parameter array
    test_scalar_np = np.linspace(v_mincoeff, v_maxcoeff, v_count_equal)
    for this_whole in range(int(v_mincoeff), int(v_maxcoeff) + 1):
        test_scalar_dense_np = np.linspace(this_whole - v_maxcoeff_whole, this_whole + v_maxcoeff_whole, v_count_whole)
        test_scalar_np = np.concatenate([test_scalar_np, test_scalar_dense_np])

    return test_scalar_np


def test_single_algebra_exp_projected(
        algebra, a_np, v_count, exp_poly_order=100,
        isleft_int=0,  # whether to use left-multiplication (1) or right-multiplication (0) in the power series
        verbose=False,
        subtitle=None,
        graph_only_xy=None,  # if given, only plots certain axes (if None, plots all possible 2D projections)
        xylim=None  # list of limits (low, high) for each dimension, e.g. [[-1,1], [-1,1], [-1,1]] for a 3D algebra
):
    """
    Creates one or more projection charts of values of the exponential function from the given arguments.

    algebra - the structure constants, dimensionality, value of exp(0), and display name of the algebra
    a_np - argument vector
    exp_poly_order - maximum exponential power up to which to approximate the power series
    is_left_int - whether to use left-multiplication x(xx) or right-multiplication (xx)x when calculating powers
    """

    strucons_np = algebra[0]
    v_dimens = algebra[1]
    exp_0 = algebra[2]
    algebra_disp = algebra[3]

    res_np, res_vecs_np = \
        exp_opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, exp_0, isleft_int,
                                 verbose=verbose)

    if subtitle is None:
        subtitle = f'{v_count} samples in {v_dimens} dimensions'
    title = make_title(f'exp(x) ({algebra_disp}), poly order {exp_poly_order}\n'
                       f'({subtitle})')

    if xylim is None:
        xylim = []
        for this_dimens in range(v_dimens):
            this_xylim = [res_vecs_np[:, this_dimens].min(), res_vecs_np[:, this_dimens].max()]
            xylim.append(this_xylim)

    for x_axis_idx in range(v_dimens - 1):
        for y_axis_idx in range(x_axis_idx + 1, v_dimens):
            if graph_only_xy is not None:
                if (x_axis_idx, y_axis_idx) not in graph_only_xy:
                    continue

            # run the simulation once, then visualize the result in every axis combination pair
            xlabel = f"x[{x_axis_idx}]"
            ylabel = f"x[{y_axis_idx}]"

            show_plot(title, xlabel, ylabel, res_vecs_np,
                      xlim=xylim[x_axis_idx], ylim=xylim[y_axis_idx],
                      x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx)


def test_single_algebra_exp(
        algebra, params_np, basis_vec, exp_poly_order=100,
        isleft_int=0,  # whether to use left-multiplication (1) or right-multiplication (0) in the power series
        v_maxcoeff_min=None,  # lower argument plot limit
        v_maxcoeff_max=None,  # upper argument plot limit
        ylim_min=None,  # lower result plot limit
        ylim_max=None,  # upper result plot limit
        verbose=False,
        param_shift_factor=0.005  # shift parameters in different dimensions for visibility
):
    """
    Calculates the exponential function of an element in the algebra (basis_vec) multiplied
    with a scalar (alpha). Plots the result.
    """

    strucons_np = algebra[0]
    v_dimens = algebra[1]
    exp_0 = algebra[2]
    algebra_disp = algebra[3]
    if basis_vec is None:
        base_vec_np = np.zeros(v_dimens, dtype=np.float64)
        base_vec_np[0] = 1.
    else:
        base_vec_np = np.array(basis_vec).astype(np.float64)

    if v_maxcoeff_min is None:
        v_maxcoeff_min = params_np.min()
    if v_maxcoeff_max is None:
        v_maxcoeff_max = params_np.max()
    if ylim_min is None:
        ylim_min = -5.
    if ylim_max is None:
        ylim_max = 5.

    # calculate the exponential function
    #   - params_np is a one-dimensional array of scalars ("alpha")
    #   - base_vec_np is the vector along which to trace out the orbit of exp(base_vec * alpha)
    v_count = params_np.size
    a_np = np.multiply(base_vec_np, params_np[:, np.newaxis]).flatten()
    res_np, res_vecs_np = \
        exp_opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, exp_0, isleft_int,
                                 verbose=verbose)

    # prepare a plot where the x-axis is the real parameter, and the y-axis shows
    # each dimension of the exponential map in a different color

    # ... aggregate plot of all polynomial orders
    fig, ax = plt.subplots(figsize=(14, 7))
    title = make_title(f'exp({basis_vec} * alpha) in {algebra_disp}'
                       f'\n(samples are denser near whole-valued alpha = 0, +/-1, +/-2, ...)')
    plt.title(title)
    xlabel = 'alpha'
    plt.xlabel(xlabel)
    plt.ylabel(f'exp({basis_vec} * alpha)')
    plt.xlim([v_maxcoeff_min, v_maxcoeff_max])
    plt.ylim([ylim_min, ylim_max])
    ax.axhline(y=0, color='k', linewidth=(0.5))
    ax.axvline(x=0, color='k', linewidth=(0.5))

    colors = ['blue', 'orange', 'green', 'lightgrey', 'yellow', 'grey', 'red', 'purple']

    for this_dimens in range(v_dimens):
        param_shift = this_dimens * (param_shift_factor / v_dimens)
        plt.scatter(x=(params_np + param_shift), y=res_vecs_np[:, this_dimens],
                    s=1, c=colors[this_dimens],
                    label=f'x[{this_dimens}]')

    lgnd = plt.legend()
    for this_dimens in range(v_dimens):
        lgnd.legendHandles[this_dimens]._sizes = [30]
    plt.show()


def make_title(title):
    # primitive builder to print the title and echo it back
    printi(f'- {title}')
    return title


def exp_opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, exp_0, isleft_int,
                             verbose=False):
    """
    (copied from ex021_composition_algebra_exp.py, then modified here to allow additional parameters)

    Given a set of arguments, calculates their values of the exponential function each

    Inputs:
      a_np - list of v_count argument vectors in dimension v_dimens, represented as 1D numpy array
      strucons_np - structure constants describing the algebra, represented as 1D numpy array
      exp_poly_order - polynomial order up to which to approximate the power series
      exp_0 - list of the result of exp(0) (in a unital algebra, this is the unit element)
      isleft_int - if 0 then right-multiplication is to be performed in the power series; otherwise left

    Outputs:
      res_np - result vectors, represented as 1D numpy array
      res_vecs_np - result vectors, reshaped as v_count vectors of dimension v_dimens
    """

    v_cl.v_dimens = v_dimens
    exp_0_np = np.array(exp_0).astype(np.float64)

    cl_info = get_platform_device_info()
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose)
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
    exp_0_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=exp_0_np)
    number_of_bytes = a_np.nbytes
    res_np = np.empty_like(a_np)
    res = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

    LIBQUIXAL_DIR = "../libquixal/"

    prg = build_program(
        ctx,
        defines_list=[f'DIMENS {v_dimens}'],
        files_list=[f'{LIBQUIXAL_DIR}qx_helper.h', f'{LIBQUIXAL_DIR}qx_vector.h', f'{LIBQUIXAL_DIR}qx_exp.h',
                    f'{LIBQUIXAL_DIR}qx_helper.cl', f'{LIBQUIXAL_DIR}qx_vector.cl', f'{LIBQUIXAL_DIR}qx_exp.cl',
                    'ex033_exponential_function.cl'],
        verbose=verbose
    )
    knl = prg.exponential_function

    # --------------------
    printi(f'Call kernel in GPU {device.name}'
           f' [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}] ...')
    knl(queue, [v_count], None, a_g, np.int32(exp_poly_order), strucons_g, exp_0_g, res, np.int32(isleft_int))
    cl.enqueue_copy(queue, res_np, res)
    printi('... done.')
    # --------------------

    # reshape the result as v_count vectors of dimension v_dimens
    res_vecs_np = res_np.reshape((v_count, v_dimens))

    return res_np, res_vecs_np


def test_single_algebra_log(
        algebra,
        v_count=1000,  # how many vectors to test
        exp_poly_order=100,  # order of the polynomial approximating the exponential function
        initial_num_particles=3,
        initial_tabu_1_steps=10,
        max_iterations=100,
        initial_rwss_med=0.6,
        v_maxcoeff=5. * math.pi,  # maximum absolute value of an individual coefficient
        silent=False,
        cap_dimens=None,  # project all results into the (X[0], X[1] plane)
        cap_coeff=None,  # initialize all searchers anywhere in the (v_dimens)-dimensional vector space
        show_interim=False,  # Don't show interim tracers during the algorithm (much, much faster)
        isleft_int=0,  # whether to use left-multiplication (1) or right-multiplication (0) in the power series
        obj_target=None,  # target to solve for as Python list (if None, use obj_target_np)
        obj_target_np=None,  # target to solve for (from obj_target if specified; default if both None: [1, 0, .., 0])
        graph_only_xy=None,  # if given, only plots certain axis pairs (if None, plots all possible 2D combinations)
        solution_found_threshold=None  # at which value of the objective function a searcher has "found a solution"
):
    """
    Samples a logarithm from the incoming algebra

    Similar to ex032_... calculates samples from the logarithm. Most parameters are passed through to the
    ex032_033_common.run_test_simulations_with_interim(...) method. See there for more info.
    """

    strucons_np = algebra[0]
    v_dimens = algebra[1]
    exp_0 = algebra[2]
    algebra_disp = algebra[3]

    # Prepare random number lookup tables
    rnd_count = 111119  # 111,119 random numbers in the tables (the size must be a prime number)
    rnd_dict = ex032_033_common.init_random_tables(rnd_count)
    random_seed = 12345  # random seed here only affects searcher position and random walk steps (not the algebra)

    if obj_target is not None:
        obj_target_np = np.array(obj_target).astype(np.float64)

    if solution_found_threshold is None:
        # this is a square, so the absolute error is the square root of this
        solution_found_threshold = 1.e-6

    subtitle = f"(random seed {random_seed}; searchers initialized in {v_dimens}D, orthogonal projection" \
               f", solution threshold {solution_found_threshold})"

    pos_0_np, pos_0_vecs_np = ex032_033_common.get_random_vectors(
        v_count, v_dimens, v_maxcoeff, cap_dimens=cap_dimens, cap_coeff=cap_coeff, random_seed=random_seed)
    exp_0_np = np.array(exp_0).astype(np.float64)

    pos_best_vecs_np_array = None
    exit_codes_np_array = None

    for x_axis_idx in range(v_dimens - 1):
        for y_axis_idx in range(x_axis_idx + 1, v_dimens):
            if graph_only_xy is not None:
                if (x_axis_idx, y_axis_idx) not in graph_only_xy:
                    continue

            # run the simulation once, then visualize the result in every axis combination pair
            xlabel = f"x[{x_axis_idx}]"
            ylabel = f"x[{y_axis_idx}]"

            pos_best_vecs_np_array, exit_codes_np_array = ex032_033_common.run_test_simulations_with_interim(
                pos_0_np, v_count, v_maxcoeff,
                v_dimens, strucons_np, algebra_disp, exp_poly_order,
                initial_num_particles, initial_tabu_1_steps, initial_rwss_med,
                rnd_dict, random_seed,
                max_iterations, show_interim,
                xlabel, ylabel,
                silent=silent,
                size_x=14, size_y=14,
                x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx,
                use_previous_simulation_for_plot=pos_best_vecs_np_array,
                prev_exit_codes_np_array=exit_codes_np_array,
                subtitle=subtitle,
                exp_0_np=exp_0_np,
                isleft_int=isleft_int,
                obj_target_np=obj_target_np,
                solution_found_threshold=solution_found_threshold
            )


# ========================================
# execute

main()
