# quixal - ex011_012_013_014_vector_multiplication.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Common part of tests:
#          ex011_complex_multiplication.py
#          ex012_split-complex_multiplication.py
#          ex013_(split-)quaternion_multiplication.py
#          ex014_(split-)octonion_multiplication.py
#
# This file creates the OpenCL context, prepares inputs, outputs, kernel, runs the
# program, runs the tests, and prints summary results.
#
# The first <num_friendly_tests> sample vector pairs will be printed explicitly at the end.
# They're supposed to be human-friendly, readily verifiable (or falsifiable) tests.
#
# The tests also test for the composition property of multiplication
# (see https://en.wikipedia.org/wiki/Composition_algebra), i.e., that
# the product of the norm of two numbers is equal to the norm of their product,
#     |a||b| = |a*b|.
# Here, "norm" includes conventional (positive definite) norm as well as split-forms.

import pprint
import random

import numpy as np
import pyopencl as cl

from helpers import qx_vector_cl_helper as v_cl
from helpers.common_helper import printi, get_context, get_platform_device_info, quadratic_form


def execute_vector_multiplication(
        v_dimens, v_count,
        num_tests, num_friendly_tests,
        a_np, b_np, strucons_np,
        verbose=True
):
    v_cl.v_dimens = v_dimens
    """
    For a prepared algebra and sample number pairs, executes pairwise multiplication in
    GPU using the OpenCL quixal library, and performs spot-wise tests in Python using numpy.
    
    Input parameters:
       v_dimens             dimensionality of the vector space
       v_count              number of vectors
       num_tests            number of spot-wise tests to execute (mostly silent)
       num_friendly_tests   number of special tests, to always show as demonstration at the end
       a_np, b_np           array of vectors to multiply
       strucons_np          structure constants representing this algebra
    """

    # reshape the input arrays as v_count numpy vectors of dimension v_dimens
    # (OpenCL code can only deal with one dimensional array parameters, which the OpenCL
    # code will immediately assemble into vectors; here we're doing the same step in
    # Python, so that we can test and visualize the result in vector form, i.e.,
    # intuitively).
    a_vecs_np = a_np.reshape((v_count, v_dimens))
    b_vecs_np = b_np.reshape((v_count, v_dimens))

    printi('Create OpenCL context, command queue, memory flags, input/output parameter buffer', verbose=verbose)
    cl_info = get_platform_device_info()
    printi(f'Available OpenCL platforms and devices:\n\n{pprint.pformat(cl_info)}\n', verbose=verbose)
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose=verbose)
    printi(f'Selected environment:\n\n{pprint.pformat(env)}\n', verbose=verbose)

    # Input parameters:
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
    # Return parameters:
    number_of_bytes = a_np.nbytes
    res_np = np.empty_like(a_np)
    res = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

    # ----------------------
    # Read static OpenCL files (.cl) into string.
    LIBQUIXAL_DIR = "../libquixal/"

    defines = f"""
    #define DIMENS {v_dimens}
    """

    with open(f'{LIBQUIXAL_DIR}qx_vector.h', 'r') as vector_h:
        prg_vector_h = vector_h.read()
    with open(f'{LIBQUIXAL_DIR}qx_vector.cl', 'r') as vector_cl:
        prg_vector_cl = vector_cl.read()
    with open('ex011_012_013_014_vector_multiplication.cl', 'r') as file_vector_multiplication_cl:
        prg_kernel_cl = file_vector_multiplication_cl.read()

    prg_text = defines \
               + prg_vector_h \
               + prg_vector_cl \
               + prg_kernel_cl

    # ----------------------
    printi(f'Build GPU program and create kernels:\n{prg_text}\n', verbose=verbose)
    prg = cl.Program(ctx, prg_text).build()
    knl = prg.vector_multiplication

    # ----------------------
    printi('Call kernel in GPU ...')
    knl(queue, [v_count], None, a_g, b_g, strucons_g, res)
    cl.enqueue_copy(queue, res_np, res)
    printi('... done.')
    # ----------------------

    # The result is still a one-dimensional vector, reshape back to the originally requested dimensionality:
    res_vecs_np = res_np.reshape((v_count, v_dimens))

    # Spot-check on CPU with Numpy
    print_num = 0
    num_tests_error = 0
    num_tests_success = 0
    comp_eucl_sum = 0
    comp_split_sum = 0
    comp_matrix2x2_sum = 0
    sig_eucl = np.full((v_dimens), 1.).astype(np.float64)
    sig_split = np.full((v_dimens), -1.).astype(np.float64)
    for dd in range(int(v_dimens / 2)):
        sig_split[dd] = 1.

    printi(f'Perform {num_tests} spot tests (print result sporadically on success; print all errors)')
    for idx_test in range(num_tests):
        # pick a random vector to test
        idx = random.randrange(v_count)
        pref = f'[test{idx_test + 1:6d}][index{idx:8d}]'
        a = a_vecs_np[idx]  # input vector a
        b = b_vecs_np[idx]  # input vector b

        # calculate in Python:
        result_vec = v_cl.qx_mult_vector(a, b, strucons_np)

        # the result of the formula from the GPU:
        res = res_vecs_np[idx]

        norm_diff = np.linalg.norm(res - result_vec)
        if norm_diff > 1e-9:
            printi(f'{pref} FAILED - params {a} * {b} GPU result {res} is not close to Python {result_vec}')
            num_tests_error += 1
        else:
            num_tests_success += 1
            if idx_test >= print_num:
                print_num += (idx_test + 1)  # don't print successes all the time
                printi(f'{pref} Success - params {a} * {b} GPU result {res} is close to Python {result_vec}')

        # add up the difference of the norms and the split-2-forms, to see whether the
        # algebra has composition property |a||b| = |a*b|:
        comp_eucl_sum += abs(
            quadratic_form(res, sig_eucl) - quadratic_form(a, sig_eucl) * quadratic_form(b, sig_eucl))
        comp_split_sum += abs(
            quadratic_form(res, sig_split) - quadratic_form(a, sig_split) * quadratic_form(b, sig_split))
        if v_dimens == 4:
            comp_matrix2x2_sum += abs(
                np.linalg.det(res.reshape((2, 2)))
                - np.linalg.det(a.reshape((2, 2))) * np.linalg.det(b.reshape((2, 2))))
        else:
            comp_matrix2x2_sum = 99.
    # -----------------------------
    # print time summary
    printi('------------------------------------------------')
    printi('SUMMARY')
    printi('------------------------------------------------')
    printi(f'Performed {v_count} multiplications in {v_dimens} dimensions.')
    printi(f'Number of tests: {num_tests}')
    printi(f'    Successes: {num_tests_success}')
    printi(f'    Errors:    {num_tests_error}')
    printi(f'Some simple tests:')
    for test_idx in range(num_friendly_tests):
        a = a_vecs_np[test_idx]
        b = b_vecs_np[test_idx]
        res = res_vecs_np[test_idx]
        printi(f'   {a} * {b} = {res}')
    if num_tests_error > 0:
        printi(f'\n\n***** TEST FAILED *****\n')
    if (comp_eucl_sum < 1.e-5):
        coef_list = [f'a{d}^2' for d in range(v_dimens)]
        form_disp = ' + '.join(coef_list)
        printi(f'The Euclidean quadratic form, Q_E(a) := {form_disp}, '
               f'has composition property in this algebra: Q(a) Q(b) = Q(a * b)')
    if (comp_split_sum < 1.e-5):
        coef_list_pos = [f'a{d}^2' for d in range(v_dimens) if d < (v_dimens / 2)]
        form_disp_pos = ' + '.join(coef_list_pos)
        coef_list_neg = [f'a{d}^2' for d in range(v_dimens) if d >= (v_dimens / 2)]
        form_disp_neg = ' - '.join(coef_list_neg)
        printi(f'The (hyperbolic) quadratic form with split-signature, Q_s(a) := {form_disp_pos} - {form_disp_neg}, '
               f'has composition property in this algebra: Q(a) Q(b) = Q(a * b)')
    if (comp_matrix2x2_sum < 1.e-5):
        printi(f'The algebra has a quadratic form that is the 2x2 matrix determinant'
               f' with composition property Q(a) Q(b) = Q(a * b)')
    printi(f'GPU: {device.name} [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}]')
