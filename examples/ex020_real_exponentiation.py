# quixal - ex020_real_exponentiation.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Sample from the exponential function in the reals, including some known results.
#
# The exponential function is calculated by approximating its polynomial series expansion
# (which is its Taylor series around 0 in the reals and complexes), up to a given polynomial order.
#
# The polynomial order determines the precision of the result. Because of the chosen polynomial expansion
# around 0, the larger the absolute of the argument x is, the larger the polynomial order is that you
# have to choose to achieve a certain precision.
#
# For example, for polynomial order 50 and an argument of x = 15, the contribution to exp(x) = exp(15) at order 50
# is approximately 15^50 / (50!) ~ 2 * 10^(-6) (or 2 millionth). With exp(15) ~ 3.3 * 10^6 (or 3.3 million),
# this yields an accuracy of less than 12 decimal digits (probably somewhere around 8 or 9 digits, due
# to the aggregate sum of all remaining polynomial terms).
#
# With that same polynomial order (50), the much smaller argument y = 8 would yield a contribution
# 8^50 / (50!) ~ 4.7 * 10^(-20) to the result exp(y) = exp(8) ~ 2981, which would yield a precision of
# over 20 decimal digits from that polynomial term. The quixal library uses float64 precision, which supports
# up to 16 significant digits. If you know that the absolute value of your argument is not greater than 8,
# it does not gain precision to calculate the polynomial terms at order 50.
#
# Links:
#   - Taylor series https://en.wikipedia.org/wiki/Taylor_series
#   - Exponential function https://en.wikipedia.org/wiki/Exponential_function

import random

import numpy as np
import pyopencl as cl
import time

from helpers import qx_exp_cl_helper as exp_cl
from helpers import qx_vector_cl_helper as v_cl
from helpers import strucons_helper as strucons
from helpers.common_helper import get_context, get_platform_device_info, build_program, printi

v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 8.  # maximum absolute value of an individual coefficient
exp_poly_order = 50  # order of the polynomial approximating the exponential function (here: to the 50th power)

# "structure constants" in the reals are just the real axis, [1.], itself:
strucons_np, v_dimens, exp_0, algebra = strucons.real_numbers()

num_tests = 1000  # how many spot-wise tests to do once finished
num_friendly_tests = 3  # output the first few tests human-friendly at the end (for visual inspection)

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Picking {v_count} random numbers from [-{v_maxcoeff}, {v_maxcoeff}]. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_count).astype(np.float64)
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff  # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range

# friendly tests
a_np[0] = 0  # calculate exp(0)
a_np[1] = 1.  # calculate exp(1.), the Euler number e
a_np[2] = -1.  # calculate 1/e

# ----------------------
# OpenCL context, command queue, memory flags, intpu/output, platforms, devices, build GPU program and create kernel

a_vecs_np = a_np.reshape((v_count, v_dimens))  # reshape them as v_count vectors of dimension v_dimens
v_cl.v_dimens = v_dimens
exp_cl.v_dimens = v_dimens

cl_info = get_platform_device_info()
ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context()
a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
number_of_bytes = a_np.nbytes
res_np = np.empty_like(a_np)
res = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

LIBQUIXAL_DIR = "../libquixal/"

prg = build_program(
    ctx,
    defines_list=[f'DIMENS {v_dimens}'],
    files_list=[f'{LIBQUIXAL_DIR}qx_vector.h', f'{LIBQUIXAL_DIR}qx_exp.h',
                f'{LIBQUIXAL_DIR}qx_vector.cl', f'{LIBQUIXAL_DIR}qx_exp.cl',
                'ex020_021_022_exponential_function.cl']
)
knl = prg.exponential_function

# ----------------------
printi('Call kernel in GPU ...')
USE_GLOBAL = 1
knl(queue, [v_count], None, a_g, np.int32(exp_poly_order), strucons_g, res, np.int32(USE_GLOBAL))
cl.enqueue_copy(queue, res_np, res)
printi('... done (check timestamp of this line for duration).')
# ----------------------

# Spot-check on CPU with Numpy
res_vecs_np = res_np.reshape((v_count, v_dimens))
print_num = 0
num_numpy_error = 0
num_numpy_success = 0
num_tests_error = 0
num_tests_success = 0
numpy_diff_accum = 0
printi(f'Perform {num_tests} spot tests (print result sporadically on success; print all errors)')
for idx_test in range(num_tests):
    # pick a random vector to test
    # formula in the test was the exponential function (using left-multiplication)
    idx = random.randrange(v_count)
    pref = f'[test{idx_test + 1:6d}][index{idx:8d}]'
    a = a_vecs_np[idx]  # input vector a

    # calculate in Python using the numpy library:
    result_numpy = np.exp(a[0], dtype=np.float64)

    # calculate in Python (with methods that mimic the GPU code):
    result_vec = exp_cl.qx_exp_left(a, exp_0, strucons_np, exp_poly_order)[0]

    # the result of the formula from the GPU:
    res = res_vecs_np[idx][0]

    # (1) compare numpy.exp(...) against GPU code (relative error)
    numpy_diff = abs(res - result_numpy) / result_numpy
    numpy_diff_accum += numpy_diff
    if numpy_diff > 1e-9:
        printi(f'{pref} FAILED - param {a} GPU result {res} is not close to numpy.exp({a}) = {result_numpy}')
        num_numpy_error += 1
    else:
        num_numpy_success += 1
        if idx_test >= print_num:
            printi(f'{pref} Success - param {a} GPU result {res} is close to numpy.exp({a}) = {result_numpy}')

    # (2) compare GPU code against Python methods that mimic it
    norm_diff = np.linalg.norm(res - result_vec)
    if norm_diff > 1e-9:
        printi(f'{pref} FAILED - param {a} GPU result {res} is not close to Python {result_vec}')
        num_tests_error += 1
    else:
        num_tests_success += 1
        if idx_test >= print_num:
            print_num += (idx_test + 1)  # don't print successes all the time
            printi(f'{pref} Success - param {a} GPU result {res} is close to Python {result_vec}')

# -----------------------------
# print time summary

printi('------------------------------------------------')
printi('SUMMARY')
printi('------------------------------------------------')
printi(f'Number of tests: 2x{num_tests}')
printi(f'    Successes: GPU {num_tests_success}, numpy {num_numpy_success}')
printi(f'    Errors:    GPU {num_tests_error}, numpy {num_numpy_error}')
numpy_diff_accum = numpy_diff_accum / num_tests
printi(f'Average relative difference between GPU code and numpy.exp(x): {numpy_diff_accum}')
printi(f'Some simple tests:')
for test_idx in range(num_friendly_tests):
    a = a_vecs_np[test_idx]
    res = res_vecs_np[test_idx]
    printi(f'   exp({a}) = {res}')
if num_tests_error > 0:
    printi(f'\n\n***** TEST FAILED *****\n')
printi(f'Calculated exponential function (up to polynomial order {exp_poly_order}) '
       f'for {v_count} vectors in {v_dimens} dimensions')
printi(f'GPU: {device.name} [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}]')
