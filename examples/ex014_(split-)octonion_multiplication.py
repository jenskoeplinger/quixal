# quixal - ex014_(split-)octonion_multiplication.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate random samples of octonion and split-octonion multiplication.
# This file performs the identical calculation as in ex011_... through ex_013_..., but here
# using the structure constants representing octonion and split-octonion multiplication in eight dimensions.
#
#   - Get two lists of 8D vectors, representing both octonions and split-octonions,
#   - define the structure constants for both octonion and split-octonion multiplication,
#   - for each pair of numbers, calculate the product in both algebras,
#   - test it against what would be expected from Python; include a few "friendly" tests that are easy to inspect;
#     some of those tests will have the identical result in both algebras, some will be different.
#
# For more information see:
#   - octonions https://en.wikipedia.org/wiki/Octonion
#   - split-octonions https://en.wikipedia.org/wiki/Split-octonion
#   - split-octonions and the rolling ball: Paper by J. C. Baez and J. Huerta, https://arxiv.org/abs/1205.2447
#
# The octonion multiplication table used here, written to basis elements (e0, e1, e2, e3, e4, e5, e5, e6, e7)
# with e0 the real axis, has the following associative 3-cycles:
#    123, 145, 176, 246, 257, 347, 365.
# For split-octonions, the same 3-cycles are used, with basis elements (e4, e5, e6, e7) being the split-complex
# bases, i.e.:
#    e0^2 = e4^e = e5^2 = e6^2 = e7^2 = 1
#    e1^2 = e2^2 = e3^2 = -1
#
# With this, both octonions and split-octonions have quaternions as subalgebra in the
# four-dimensional (e0, e1, e2, e3) subspace.

import random

import numpy as np
import time

from ex011_012_013_014_vector_multiplication import execute_vector_multiplication
from helpers.common_helper import printi

v_dimens = 8  # dimensionality the vector space; (split-)octonions are eight-dimensional
v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 5.  # maximum absolute value of an individual coefficient

num_tests = 1000  # how many spot-wise tests to do once finished
num_friendly_tests = 9  # output the first few tests human-friendly at the end (for visual inspection)

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Setting up {v_count} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_dimens * v_count).astype(np.float64)
b_np = np.random.rand(v_dimens * v_count).astype(np.float64)
# scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff
b_np = b_np * 2. * v_maxcoeff - v_maxcoeff

# Make the first few vector pairs special, so that we can test these simple examples just by looking at them
# 2 * 3 = 6 (in the real subspace; more precisely: 2 e0 * 3 e0 = 6 e0)
a_np[0:8] = [2, 0, 0, 0, 0, 0, 0, 0]
b_np[0:8] = [3, 0, 0, 0, 0, 0, 0, 0]
# e1 * e! = -e0 = -1
a_np[8:16] = [0, 1, 0, 0, 0, 0, 0, 0]
b_np[8:16] = [0, 1, 0, 0, 0, 0, 0, 0]
# e1 * e2 = e3
a_np[16:24] = [0, 1, 0, 0, 0, 0, 0, 0]
b_np[16:24] = [0, 0, 1, 0, 0, 0, 0, 0]
# e1 * e4 = e5
a_np[24:32] = [0, 1, 0, 0, 0, 0, 0, 0]
b_np[24:32] = [0, 0, 0, 0, 1, 0, 0, 0]
# e2 * e4 = e6
a_np[32:40] = [0, 0, 1, 0, 0, 0, 0, 0]
b_np[32:40] = [0, 0, 0, 0, 1, 0, 0, 0]
# e3 * e5 = -e6
a_np[40:48] = [0, 0, 0, 1, 0, 0, 0, 0]
b_np[40:48] = [0, 0, 0, 0, 0, 1, 0, 0]
# octonions:
#   e4 * e4 = -e0 = -1
# split-octonions:
#   e4 * e4 = e0 = 1
a_np[48:56] = [0, 0, 0, 0, 1, 0, 0, 0]
b_np[48:56] = [0, 0, 0, 0, 1, 0, 0, 0]
# octonions:
#   e4 * e7 = e3
# split-octonions:
#   e4 * e7 = -e3
a_np[56:64] = [0, 0, 0, 0, 1, 0, 0, 0]
b_np[56:64] = [0, 0, 0, 0, 0, 0, 0, 1]
# octonions:
#   e5 * e6 = -e3
# split-octonions:
#   e5 * e6 = e3
a_np[64:72] = [0, 0, 0, 0, 0, 1, 0, 0]
b_np[64:72] = [0, 0, 0, 0, 0, 0, 1, 0]

printi(f'\n\n---------------------------- (1) OCTONION ----------------------------\n')
printi(f'Set up structure constants modeling octonion multiplication:')
strucons_np = np.array([
    #  e0 * ...:
    1, 0, 0, 0, 0, 0, 0, 0,  # e0 * e0 = e0
    0, 1, 0, 0, 0, 0, 0, 0,  # e0 * e1 = e1
    0, 0, 1, 0, 0, 0, 0, 0,  # e0 * e2 = e2
    0, 0, 0, 1, 0, 0, 0, 0,  # e0 * e3 = e3
    0, 0, 0, 0, 1, 0, 0, 0,  # e0 * e4 = e4
    0, 0, 0, 0, 0, 1, 0, 0,  # e0 * e5 = e5
    0, 0, 0, 0, 0, 0, 1, 0,  # e0 * e6 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e0 * e7 = e7
    #  e1 * ...:
    0, 1, 0, 0, 0, 0, 0, 0,  # e1 * e0 = e1
    -1, 0, 0, 0, 0, 0, 0, 0,  # e1 * e1 = -e0
    0, 0, 0, 1, 0, 0, 0, 0,  # e1 * e2 = e3
    0, 0, -1, 0, 0, 0, 0, 0,  # e1 * e3 = -e2
    0, 0, 0, 0, 0, 1, 0, 0,  # e1 * e4 = e5
    0, 0, 0, 0, -1, 0, 0, 0,  # e1 * e5 = -e4
    0, 0, 0, 0, 0, 0, 0, -1,  # e1 * e6 = -e7
    0, 0, 0, 0, 0, 0, 1, 0,  # e1 * e7 = e6
    #  e2 * ...:
    0, 0, 1, 0, 0, 0, 0, 0,  # e2 * e0 = e2
    0, 0, 0, -1, 0, 0, 0, 0,  # e2 * e1 = -e3
    -1, 0, 0, 0, 0, 0, 0, 0,  # e2 * e2 = -e0
    0, 1, 0, 0, 0, 0, 0, 0,  # e2 * e3 = e1
    0, 0, 0, 0, 0, 0, 1, 0,  # e2 * e4 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e2 * e5 = e7
    0, 0, 0, 0, -1, 0, 0, 0,  # e2 * e6 = -e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e2 * e7 = -e5
    #  e3 * ...:
    0, 0, 0, 1, 0, 0, 0, 0,  # e3 * e0 = e3
    0, 0, 1, 0, 0, 0, 0, 0,  # e3 * e1 = e2
    0, -1, 0, 0, 0, 0, 0, 0,  # e3 * e2 = -e1
    -1, 0, 0, 0, 0, 0, 0, 0,  # e3 * e3 = -e0
    0, 0, 0, 0, 0, 0, 0, 1,  # e3 * e4 = e7
    0, 0, 0, 0, 0, 0, -1, 0,  # e3 * e5 = -e6
    0, 0, 0, 0, 0, 1, 0, 0,  # e3 * e6 = e5
    0, 0, 0, 0, -1, 0, 0, 0,  # e3 * e7 = -e4
    #  e4 * ...:
    0, 0, 0, 0, 1, 0, 0, 0,  # e4 * e0 = e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e4 * e1 = -e5
    0, 0, 0, 0, 0, 0, -1, 0,  # e4 * e2 = -e6
    0, 0, 0, 0, 0, 0, 0, -1,  # e4 * e3 = -e7
    -1, 0, 0, 0, 0, 0, 0, 0,  # e4 * e4 = -e0
    0, 1, 0, 0, 0, 0, 0, 0,  # e4 * e5 = e1
    0, 0, 1, 0, 0, 0, 0, 0,  # e4 * e6 = e2
    0, 0, 0, 1, 0, 0, 0, 0,  # e4 * e7 = e3
    #  e5 * ...:
    0, 0, 0, 0, 0, 1, 0, 0,  # e5 * e0 = e5
    0, 0, 0, 0, 1, 0, 0, 0,  # e5 * e1 = e4
    0, 0, 0, 0, 0, 0, 0, -1,  # e5 * e2 = -e7
    0, 0, 0, 0, 0, 0, 1, 0,  # e5 * e3 = e6
    0, -1, 0, 0, 0, 0, 0, 0,  # e5 * e4 = -e1
    -1, 0, 0, 0, 0, 0, 0, 0,  # e5 * e5 = -e0
    0, 0, 0, -1, 0, 0, 0, 0,  # e5 * e6 = -e3
    0, 0, 1, 0, 0, 0, 0, 0,  # e5 * e7 = e2
    #  e6 * ...:
    0, 0, 0, 0, 0, 0, 1, 0,  # e6 * e0 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e6 * e1 = e7
    0, 0, 0, 0, 1, 0, 0, 0,  # e6 * e2 = e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e6 * e3 = -e5
    0, 0, -1, 0, 0, 0, 0, 0,  # e6 * e4 = -e2
    0, 0, 0, 1, 0, 0, 0, 0,  # e6 * e5 = e3
    -1, 0, 0, 0, 0, 0, 0, 0,  # e6 * e6 = -e0
    0, -1, 0, 0, 0, 0, 0, 0,  # e6 * e7 = -e1
    #  e7 * ...:
    0, 0, 0, 0, 0, 0, 0, 1,  # e7 * e0 = e7
    0, 0, 0, 0, 0, 0, -1, 0,  # e7 * e1 = -e6
    0, 0, 0, 0, 0, 1, 0, 0,  # e7 * e2 = e5
    0, 0, 0, 0, 1, 0, 0, 0,  # e7 * e3 = e4
    0, 0, 0, -1, 0, 0, 0, 0,  # e7 * e4 = -e3
    0, 0, -1, 0, 0, 0, 0, 0,  # e7 * e5 = -e2
    0, 1, 0, 0, 0, 0, 0, 0,  # e7 * e6 = e1
    -1, 0, 0, 0, 0, 0, 0, 0  # e7 * e7 = -e0
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np
)
# ------------------------

printi(f'\n\n---------------------------- (2) SPLIT-OCTONION ----------------------------\n')
printi(f'Set up structure constants modeling split-octonion multiplication:')
strucons_np = np.array([
    #  e0 * ...:
    1, 0, 0, 0, 0, 0, 0, 0,  # e0 * e0 = e0
    0, 1, 0, 0, 0, 0, 0, 0,  # e0 * e1 = e1
    0, 0, 1, 0, 0, 0, 0, 0,  # e0 * e2 = e2
    0, 0, 0, 1, 0, 0, 0, 0,  # e0 * e3 = e3
    0, 0, 0, 0, 1, 0, 0, 0,  # e0 * e4 = e4
    0, 0, 0, 0, 0, 1, 0, 0,  # e0 * e5 = e5
    0, 0, 0, 0, 0, 0, 1, 0,  # e0 * e6 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e0 * e7 = e7
    #  e1 * ...:
    0, 1, 0, 0, 0, 0, 0, 0,  # e1 * e0 = e1
    -1, 0, 0, 0, 0, 0, 0, 0,  # e1 * e1 = -e0
    0, 0, 0, 1, 0, 0, 0, 0,  # e1 * e2 = e3
    0, 0, -1, 0, 0, 0, 0, 0,  # e1 * e3 = -e2
    0, 0, 0, 0, 0, 1, 0, 0,  # e1 * e4 = e5
    0, 0, 0, 0, -1, 0, 0, 0,  # e1 * e5 = -e4
    0, 0, 0, 0, 0, 0, 0, -1,  # e1 * e6 = -e7
    0, 0, 0, 0, 0, 0, 1, 0,  # e1 * e7 = e6
    #  e2 * ...:
    0, 0, 1, 0, 0, 0, 0, 0,  # e2 * e0 = e2
    0, 0, 0, -1, 0, 0, 0, 0,  # e2 * e1 = -e3
    -1, 0, 0, 0, 0, 0, 0, 0,  # e2 * e2 = -e0
    0, 1, 0, 0, 0, 0, 0, 0,  # e2 * e3 = e1
    0, 0, 0, 0, 0, 0, 1, 0,  # e2 * e4 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e2 * e5 = e7
    0, 0, 0, 0, -1, 0, 0, 0,  # e2 * e6 = -e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e2 * e7 = -e5
    #  e3 * ...:
    0, 0, 0, 1, 0, 0, 0, 0,  # e3 * e0 = e3
    0, 0, 1, 0, 0, 0, 0, 0,  # e3 * e1 = e2
    0, -1, 0, 0, 0, 0, 0, 0,  # e3 * e2 = -e1
    -1, 0, 0, 0, 0, 0, 0, 0,  # e3 * e3 = -e0
    0, 0, 0, 0, 0, 0, 0, 1,  # e3 * e4 = e7
    0, 0, 0, 0, 0, 0, -1, 0,  # e3 * e5 = -e6
    0, 0, 0, 0, 0, 1, 0, 0,  # e3 * e6 = e5
    0, 0, 0, 0, -1, 0, 0, 0,  # e3 * e7 = -e4
    #  e4 * ...:
    0, 0, 0, 0, 1, 0, 0, 0,  # e4 * e0 = e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e4 * e1 = -e5
    0, 0, 0, 0, 0, 0, -1, 0,  # e4 * e2 = -e6
    0, 0, 0, 0, 0, 0, 0, -1,  # e4 * e3 = -e7
    1, 0, 0, 0, 0, 0, 0, 0,  # e4 * e4 = e0 (compare to octonions: -e0)
    0, -1, 0, 0, 0, 0, 0, 0,  # e4 * e5 = -e1 (compare to octonions: e1)
    0, 0, -1, 0, 0, 0, 0, 0,  # e4 * e6 = -e2 (compare to octonions: e2)
    0, 0, 0, -1, 0, 0, 0, 0,  # e4 * e7 = -e3 (compare to octonions: e2)
    #  e5 * ...:
    0, 0, 0, 0, 0, 1, 0, 0,  # e5 * e0 = e5
    0, 0, 0, 0, 1, 0, 0, 0,  # e5 * e1 = e4
    0, 0, 0, 0, 0, 0, 0, -1,  # e5 * e2 = -e7
    0, 0, 0, 0, 0, 0, 1, 0,  # e5 * e3 = e6
    0, 1, 0, 0, 0, 0, 0, 0,  # e5 * e4 = e1 (compare to octonions: -e1)
    1, 0, 0, 0, 0, 0, 0, 0,  # e5 * e5 = e0 (compare to octonions: -e0)
    0, 0, 0, 1, 0, 0, 0, 0,  # e5 * e6 = e3 (compare to octonions: -e3)
    0, 0, -1, 0, 0, 0, 0, 0,  # e5 * e7 = -e2 (compare to octonions: e2)
    #  e6 * ...:
    0, 0, 0, 0, 0, 0, 1, 0,  # e6 * e0 = e6
    0, 0, 0, 0, 0, 0, 0, 1,  # e6 * e1 = e7
    0, 0, 0, 0, 1, 0, 0, 0,  # e6 * e2 = e4
    0, 0, 0, 0, 0, -1, 0, 0,  # e6 * e3 = -e5
    0, 0, 1, 0, 0, 0, 0, 0,  # e6 * e4 = e2 (compare to octonions: -e2)
    0, 0, 0, -1, 0, 0, 0, 0,  # e6 * e5 = -e3 (compare to octonions: e3)
    1, 0, 0, 0, 0, 0, 0, 0,  # e6 * e6 = e0 (compare to octonions: -e0)
    0, 1, 0, 0, 0, 0, 0, 0,  # e6 * e7 = e1 (compare to octonions: -e1)
    #  e7 * ...:
    0, 0, 0, 0, 0, 0, 0, 1,  # e7 * e0 = e7
    0, 0, 0, 0, 0, 0, -1, 0,  # e7 * e1 = -e6
    0, 0, 0, 0, 0, 1, 0, 0,  # e7 * e2 = e5
    0, 0, 0, 0, 1, 0, 0, 0,  # e7 * e3 = e4
    0, 0, 0, 1, 0, 0, 0, 0,  # e7 * e4 = e3 (compare to octonions: -e3)
    0, 0, 1, 0, 0, 0, 0, 0,  # e7 * e5 = e2 (compare to octonions: -e2)
    0, -1, 0, 0, 0, 0, 0, 0,  # e7 * e6 = -e1 (compare to octonions: e1)
    1, 0, 0, 0, 0, 0, 0, 0  # e7 * e7 = e0 (compare to octonions: -e0)
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np,
    verbose=False
)
# ------------------------
