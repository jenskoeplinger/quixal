// quixal - ex015_octooctonion_multiplication.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// Required constant:
//    DIMENS - the dimensionality of the vector space:
//
// Required include:
//    libquixal/qx_vector.cl
//
// Performs various tests in the algebra, given three vectors, and returns the results in a bitmap. In addition
// to testing the specific algebra property, it also returns an indication if one or both of the terms tested
// were zero (null-space analysis).
//
// Bitmap offset for test:
//   +0  (*1) commutativity                   compare: a * b             to: b * a
//   +1  (*2) associativity                            (a * b) * c           a * (b * c)
//   +2  (*4) alternativity                            (a * a) * b           a * (a * b)
//   +3  (*8) flexibility                              (a * b) * a           a * (b * a)
//   +4 (*16) power-associativity (3-factor test)      (a * a) * a           a * (a * a)
//   +5 (*32) power-associativity (4-factor test)      ((a * a) * a) * a     (a * a) * (a * a)
//
// Further bitmap offset based on null space analysis:
//    +0 (       *1) both results are nonzero
//    +8 (     *256) both results are zero
//   +16 (   *65536) the first test is zero but the second isn't (nontrivial null space)
//   +24 (*16777216) the second test is zero but the first isn't (nontrivial null space)
//
// Special one-off bitmap flags are:
//    14 (    16384) nilpotent             a * a = 0
//    22 (  4194304) interim result FYI    b * c = 0 (since the factor 'c' isn't evaluated elsewhere)


#define EQUAL_THRESHOLD 0.0000000000000001

#define OFFSET_COMMUTATIVE 1
#define OFFSET_ASSOCIATIVE 2
#define OFFSET_ALTERNATIVE 4
#define OFFSET_FLEXIBLE 8
#define OFFSET_POWERASSOCIATIVE_THREEFACTOR 16
#define OFFSET_POWERASSOCIATIVE_FOURFACTOR 32

#define ALG_BOTH_ZERO 256
#define ALG_FIRST_ZERO 65536
#define ALG_SECOND_ZERO 16777216

#define ALG_NILPOTENT 16384
#define ALG_INTERIM_BC_IS_ZERO_FYI 4194304


// Compares two vectors, and returns the following:
//   - 0 if they're different and both are nonzero
//   - equal_offset if they're the same and both are nonzero
//   - equal_offset * (1 + ALG_BOTH_ZERO) if they're the same and both are zero
//   - equal_offset * ALG_FIRST_ZERO if they're different and the first factor is zero
//   - equal_offset * ALG_SECOND_ZERO if they're different and the second factor is zero
int test_two_vectors(double *a_vec, double *b_vec, int equal_offset) {

  int alg_prop_test = 0;

  if (qx_norm_squared_diff(a_vec, b_vec) < EQUAL_THRESHOLD) {
    alg_prop_test += equal_offset; // test is commutative
    if (qx_norm_squared(a_vec) < EQUAL_THRESHOLD) {
      alg_prop_test += (equal_offset * ALG_BOTH_ZERO); // both factors are zero
    }
  } else {
    if (qx_norm_squared(a_vec) < EQUAL_THRESHOLD) {
      alg_prop_test += (equal_offset * ALG_FIRST_ZERO); // first factor is zero (but second factor isn't)
    } else if (qx_norm_squared(b_vec) < EQUAL_THRESHOLD) {
      alg_prop_test += (equal_offset * ALG_SECOND_ZERO); // second factor is zero (but first factor isn't)
    }
  }

  return alg_prop_test;
}


__kernel void vector_multiplication(
    __global const double *a_g,
    __global const double *b_g,
    __global const double *c_g,
    __global const double *strucons_g,
    __global int *alg_props)
{

  int gid = get_global_id(0);
  int idx = gid * DIMENS; // each value of gid corresponds to a vector with DIMENS coefficients

  // threshold for equality: square-norm has to be smaller than this value:

  // (1) get the input vectors from the global input (reshape from 1D array into 2D/4D/8D vector)

  double a_vec[DIMENS];
  double b_vec[DIMENS];
  double c_vec[DIMENS];

  int i = 0;
  while (i < DIMENS) {
    a_vec[i] = a_g[idx + i];
    b_vec[i] = b_g[idx + i];
    c_vec[i] = c_g[idx + i];
    i++;
  }

  // (2) prepare results vectors, clear them to all zeroes (as required by qx_mult_vector(_g)), and
  //     calculate the products to be tested

  // testing commutativity:
  double ab_vec[DIMENS]; // a * b
  double ba_vec[DIMENS]; // b * a
  qx_clear_vector(ab_vec);
  qx_clear_vector(ba_vec);

  qx_mult_vector_g(a_vec, b_vec, ab_vec, strucons_g);
  qx_mult_vector_g(b_vec, a_vec, ba_vec, strucons_g);

  // testing associativity:
  double ab_c_vec[DIMENS]; // (a * b) * c
  double bc_vec[DIMENS]; // b * c (interim)
  double a_bc_vec[DIMENS]; // a * (b * c)
  qx_clear_vector(ab_c_vec);
  qx_clear_vector(bc_vec);
  qx_clear_vector(a_bc_vec);

  qx_mult_vector_g(ab_vec, c_vec, ab_c_vec, strucons_g);
  qx_mult_vector_g(b_vec, c_vec, bc_vec, strucons_g); // (interim)
  qx_mult_vector_g(a_vec, bc_vec, a_bc_vec, strucons_g);

  // testing alternativity:
  double aa_vec[DIMENS]; // a * a (interim)
  double aa_b_vec[DIMENS]; // (a * a) * b
  double a_ab_vec[DIMENS]; // a * (a * b)
  qx_clear_vector(aa_vec);
  qx_clear_vector(aa_b_vec);
  qx_clear_vector(a_ab_vec);
  qx_mult_vector_g(a_vec, a_vec, aa_vec, strucons_g); // (interim)
  qx_mult_vector_g(aa_vec, b_vec, aa_b_vec, strucons_g);
  qx_mult_vector_g(a_vec, ab_vec, a_ab_vec, strucons_g);

  // testing flexibility:
  double ab_a_vec[DIMENS]; // (a * b) * a
  double a_ba_vec[DIMENS]; // a * (b * a)
  qx_clear_vector(ab_a_vec);
  qx_clear_vector(a_ba_vec);
  qx_mult_vector_g(ab_vec, a_vec, ab_a_vec, strucons_g);
  qx_mult_vector_g(a_vec, ba_vec, a_ba_vec, strucons_g);

  // testing power-associativity (3-factor test):
  double aa_a_vec[DIMENS]; // (a * a) * a
  double a_aa_vec[DIMENS]; // a * (a * a)
  qx_clear_vector(aa_a_vec);
  qx_clear_vector(a_aa_vec);
  qx_mult_vector_g(aa_vec, a_vec, aa_a_vec, strucons_g);
  qx_mult_vector_g(a_vec, aa_vec, a_aa_vec, strucons_g);

  // testing power-associativity (4-factor test):
  double aa_a_a_vec[DIMENS]; // ((a * a) * a) * a
  double aa_aa_vec[DIMENS]; // (a * a) * (a * a)
  qx_clear_vector(aa_a_a_vec);
  qx_clear_vector(aa_aa_vec);
  qx_mult_vector_g(aa_a_vec, a_vec, aa_a_a_vec, strucons_g);
  qx_mult_vector_g(aa_vec, aa_vec, aa_aa_vec, strucons_g);

  // (3) compare the various test results with one another, and update the result bitmap

  int alg_prop = 0;
  // Bitmap offset for test:
  //   +0 commutativity
  //   +1 associativity
  //   +2 alternativity
  //   +3 flexibility
  //   +4 power-associativity (3-factor test)
  //   +5 power-associativity (4-factor test)
  //
  // Further bitmap offset based on null space analysis:
  //    +0 both results are nonzero
  //    +8 both results are zero (symmetric null space)
  //   +16 the first test is zero but the second isn't (asymmetric null space)
  //   +24 the second test is zero but the first isn't (asymmetric null space)

  alg_prop += test_two_vectors(ab_vec, ba_vec, OFFSET_COMMUTATIVE);
  alg_prop += test_two_vectors(ab_c_vec, a_bc_vec, OFFSET_ASSOCIATIVE);
  alg_prop += test_two_vectors(aa_b_vec, a_ab_vec, OFFSET_ALTERNATIVE);
  alg_prop += test_two_vectors(ab_a_vec, a_ba_vec, OFFSET_FLEXIBLE);
  alg_prop += test_two_vectors(aa_a_vec, a_aa_vec, OFFSET_POWERASSOCIATIVE_THREEFACTOR);
  alg_prop += test_two_vectors(aa_a_a_vec, aa_aa_vec, OFFSET_POWERASSOCIATIVE_FOURFACTOR);

  // special "one-off" bitmap flags
  if (qx_norm_squared(aa_vec) < EQUAL_THRESHOLD) alg_prop += ALG_NILPOTENT;
  if (qx_norm_squared(bc_vec) < EQUAL_THRESHOLD) alg_prop += ALG_INTERIM_BC_IS_ZERO_FYI;

  // (4) return the algebra properties bitmap

  alg_props[gid] = alg_prop;
}


