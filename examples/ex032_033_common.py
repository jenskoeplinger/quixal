# quixal - ex032_033_common.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Helper code that is common for examples ex032... and ex033... (finding logarithms using
# the variable-neighborhood search algorithm, which in turn spawns a small swarm of particles with
# each iteration that probes whether it is better to do larger or smaller search steps).

import random

import math
import matplotlib.patches as mpatches
import numpy as np
import pyopencl as cl
import time

from helpers import qx_exp_cl_helper as exp_cl
from helpers import qx_vector_cl_helper as v_cl
from helpers.common_helper import get_context, get_platform_device_info, build_program, printi, show_plot


def run_test_simulations_with_interim(
        pos_0_np, v_count, v_maxcoeff,
        v_dimens, strucons_np, algebra, exp_poly_order,
        initial_num_particles, initial_tabu_1_steps, initial_rwss_med,
        rnd_dict, random_seed,
        max_iterations, show_interim,
        xlabel, ylabel,
        silent,
        size_x=7, size_y=7,
        x_axis_idx=0, y_axis_idx=1,
        use_previous_simulation_for_plot=None,
        prev_exit_codes_np_array=None,
        subtitle="",
        exp_0_np=None, isleft_int=1, obj_target_np=None,
        solution_found_threshold=None
):
    """
    Runs some test simulations with initial random walk step size, number of particles per swarm,
    and number of random walk steps per particle. The GPU code performs random search iterations
    without reporting back to the Python wrapper. In order to visualize progress of the searchers
    as the number of iteration increases, the "max_iterations" parameter will be increased one
    by one and the GPU code will be called repeatedly. This is of course wasteful, but provides us
    with interim results of the search that would otherwise be hidden within the algorithm.
    """

    if subtitle is not None and subtitle != "":
        subtitle = f"\n{subtitle}"

    if obj_target_np is None:
        obj_target_np_disp = "1"
    else:
        obj_target_np_disp = f'{obj_target_np}'

    title = make_title(
        f'{v_count} searchers (swarms), '
        f'up to {max_iterations} iteration{"s" if max_iterations != 1 else ""}\n'
        f'Objective: sample {algebra} inverses of exp(x) = {obj_target_np_disp}\n'
        f'(dynamic variable neighborhood search; init: #pps={initial_num_particles}, '
        f'#rws={initial_tabu_1_steps}, rwss_med={initial_rwss_med})'
        f'{subtitle}')

    if use_previous_simulation_for_plot is None:
        printi('Start simulation ...')

        pos_best_vecs_np_list = []
        exit_codes_list = []

        if show_interim:
            # perform (redundant) re-iterations each time, to visualize searchers (as in ex030...)
            iter_start = 0
        else:
            # let the GPU render all iterations, then plot the results (with some telemetry)
            iter_start = max_iterations - 1

        for this_iter in range(iter_start, max_iterations):
            this_max_iterations = this_iter + 1
            if silent:
                print('.', end='')
                if (this_max_iterations % 10 == 0):
                    print(f'{this_max_iterations}/{max_iterations}', end='')
            else:
                printi(f'iteration depth {this_max_iterations}/{max_iterations}...')
            pos_best_vecs_np, pos_best_error_np, exit_codes_np = \
                opencl_build_and_run(
                    pos_0_np, v_count, v_dimens, strucons_np, exp_poly_order,
                    initial_num_particles, initial_tabu_1_steps, initial_rwss_med,
                    rnd_dict,
                    v_maxcoeff, random_seed, this_max_iterations,
                    verbose=(this_max_iterations == 1),
                    silent=silent,
                    verbose_telemetry=(show_interim and (this_max_iterations == max_iterations)),
                    exp_0_np=exp_0_np, isleft_int=isleft_int, obj_target_np=obj_target_np,
                    solution_found_threshold=solution_found_threshold)
            # record this iteration's best positions
            pos_best_vecs_np_list.append(pos_best_vecs_np)
            exit_codes_list.append(exit_codes_np)

        pos_best_vecs_np_array = np.array(pos_best_vecs_np_list)
        exit_codes_np_array = np.array(exit_codes_list)
        if silent:
            print('')
        printi('Done.')

    else:
        pos_best_vecs_np_array = use_previous_simulation_for_plot
        exit_codes_np_array = prev_exit_codes_np_array

    if show_interim:
        # plot results with interim steps
        lims = v_maxcoeff * 1.3
        legend_handles = [
            mpatches.Patch(color='black', label='most recent search pos'),
            mpatches.Patch(color='grey', label='(greys) iteration trace of searcher'),
            mpatches.Patch(color='yellow', label='out-of-bounds')
        ]

        # add a mock iterations (> max_iterations) for values that ran out of bounds
        pos_in_range_np = np.copy(pos_best_vecs_np_array)
        pos_out_of_range_np = np.copy(pos_best_vecs_np_array)

        for this_iter in range(max_iterations):
            for this_searcher in range(v_count):
                if (pos_best_vecs_np_array[this_iter, this_searcher, :].max() > v_maxcoeff) \
                        or (pos_best_vecs_np_array[this_iter, this_searcher, :].min() < -v_maxcoeff):
                    pos_in_range_np[this_iter, this_searcher, :] = v_maxcoeff
                else:
                    pos_out_of_range_np[this_iter, this_searcher, :] = v_maxcoeff

        pos_best_vecs_np_array_sep = np.concatenate((pos_in_range_np, pos_out_of_range_np))

        show_plot(title, xlabel, ylabel, pos_best_vecs_np_array_sep,
                  size_x=size_x, size_y=size_y,
                  c=get_grays_for_iteration(max_iterations),
                  x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx,
                  xlim=[-lims, lims], ylim=[-lims, lims],
                  legend_handles=legend_handles)

    # plot end result with exit codes (including out-of-bounds)
    lims = v_maxcoeff * 1.3
    ec_colmap = {
        0: 'red',
        1: 'blue',
        2: 'yellow',
        3: 'lightgreen'
    }
    exit_code_cols = [ec_colmap[exit_code] for exit_code in exit_codes_np_array[-1]]

    legend_handles = [
        mpatches.Patch(color=ec_colmap[1], label='Solution'),
        mpatches.Patch(color=ec_colmap[2], label='Out-of-bounds'),
        mpatches.Patch(color=ec_colmap[3], label='Max iter abort'),
        mpatches.Patch(color=ec_colmap[0], label='Error')
    ]

    show_plot(title, xlabel, ylabel, pos_best_vecs_np_array[-1],
              size_x=size_x, size_y=size_y,
              s=4,
              c=exit_code_cols,
              x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx,
              xlim=[-lims, lims], ylim=[-lims, lims],
              legend_handles=legend_handles)

    return pos_best_vecs_np_array, exit_codes_np_array


def get_grays_for_iteration(iter_count):
    # Gray scale from 10% through black, getting darker with increasing iter_count.
    # Append "yellow" for values that ran out of bounds
    shades = [str(math.sqrt(((iter_count - (cnt + 1)) / 1.1) / iter_count))
              for cnt in range(0, iter_count)]
    shades.extend(['yellow'] * iter_count)
    return shades


def get_random_vectors(v_count, v_dimens, v_maxcoeff, nonreal=False,
                       cap_dimens=None, cap_coeff=None, random_seed=None):
    if random_seed is not None:
        np.random.seed(random_seed)

    # create vectors with random numbers
    a_np = np.random.rand(v_count * v_dimens).astype(np.float64)
    if nonreal:
        # only return nonreal values, i.e., set the real component to 0
        a_np[0::v_dimens] = 0

    a_np = a_np * 2. * v_maxcoeff - v_maxcoeff  # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range

    if cap_dimens is not None:
        # restrict certain dimensions to a minimum and maximum value
        for this_dimens in cap_dimens:
            a_np[this_dimens::v_dimens] = a_np[this_dimens::v_dimens] * (cap_coeff / v_maxcoeff)

    a_vecs_np = a_np.reshape((v_count, v_dimens))  # reshaped as v_count vectors of dimension v_dimens
    return a_np, a_vecs_np


def make_title(title):
    # primitive builder to print the title and echo it back
    printi(f'- {title}')
    return title


def init_random_tables(rnd_count, random_seed=None, randlong_high=1.e9, randint_high=1.e5):
    """
    Initializes the random number lookup tables.

    Inputs:
      - rnd_count - size of the pseudo-number tables
      - random_seed (optional) - set to specific number to guarantee the same pseudo-number tables
      - randint_high (optional) - upper bound for random integer (default: 1.e5)
      - randlong_high (optional) - upper bound for random long (default: 1.e9)
    """

    if random_seed is None:
        random_seed = int(time.time_ns() / 1000000000)

    printi(f'Setting up {rnd_count} random numbers (float32, float64, int32, and int64). Random seed: {random_seed}.')
    random.seed(random_seed)

    # create random number lookup tables as one-dimensional numpy arrays
    rnd_float = np.random.rand(rnd_count).astype(np.float32)
    rnd_double = np.random.rand(rnd_count).astype(np.float64)
    rnd_int_pre = np.random.rand(rnd_count).astype(np.float32)
    rnd_int = (rnd_int_pre * randint_high).astype(np.int32)
    rnd_long_pre = np.random.rand(rnd_count).astype(np.float64)
    rnd_long = (rnd_long_pre * randlong_high).astype(np.int64)

    return {'rnd_float': rnd_float,
            'rnd_double': rnd_double,
            'rnd_int': rnd_int,
            'rnd_long': rnd_long}


def opencl_build_and_run(pos_0_np, v_count, v_dimens, strucons_np, exp_poly_order,
                         initial_num_particles, initial_tabu_1_steps, initial_rwss_med,
                         rnd_dict, v_maxcoeff, random_seed, this_max_iterations,
                         verbose=True, silent=False, verbose_telemetry=False,
                         exp_0_np=None, isleft_int=1, obj_target_np=None,
                         solution_found_threshold=None):
    """
    OpenCL context, command queue, memory flags, input/output, platforms, devices, build GPU program and create kernel.

    Inputs:
      pos_0_np - list of v_count local search start vectors in dimension v_dimens, represented as 1D numpy array;
                 one particle swarm will start on each start vector
      strucons_np - structure constants describing the algebra, represented as 1D numpy array
      exp_poly_order - polynomial order up to which to approximate the power series
      initial_num_particles - initial number of particles per swarm (may change dynamically during search)
      initial_tabu_1_steps - initial number of random walk steps each particle will do with tabu size 1
                 (i.e. stepping back prohibited)
      initial_rwss_med - initial mid-point value for how far to go on each random walk step
      rnd_dict - dictionary with random number lookup tables (float32, float64, int32, and int64)
      v_maxcoeff - coefficient boundary (+/-, in any dimension)
      exp_0_np - if given, the vector that is the result of exp(0) (as numpy array)
      isleft_int - integer value whether left-multiplication is to be used (1) or right-multiplication (0)
      obj_target_np - the value to solve for, log(obj_target_np) = { x | exp(x) = obj_target_np }
      solution_found_threshold - square of the mean absolute error (difference between exp(y) and x) within y is
                      assumed to be a solution to exp(y) = x, i.e., a member of log(x)

    Outputs:
      pos_best_vecs_np - list of v_count best found positions
      pos_best_error - list of errors (values from the objective function)
      exit_codes_np - list of exit codes (1 - solution found, 2 - out of bounds, 3 - max iteration exceeded)
    """

    if silent:
        verbose = False

    # global defines

    v_cl.v_dimens = v_dimens
    exp_cl.v_dimens = v_dimens

    NUM_PARTICLES_MAX = int(initial_num_particles * 3)
    NUM_TABU_1_STEPS_MAX = int(initial_tabu_1_steps * 4)

    # value of exp(0)
    if exp_0_np is None:
        # default to (1, 0, ..., 0) (whatever that vector may mean in that algebra)
        exp_0_np = np.zeros(v_dimens, dtype=np.float64)
        exp_0_np[0] = 1.
    # target (objective)
    if obj_target_np is None:
        # default target: find x for which exp(x) = exp(0)
        obj_target_np = exp_0_np.copy()

    # coefficient out-of-bounds box
    outofbounds_abort_low_np = np.zeros(v_dimens, dtype=np.float64)
    outofbounds_abort_low_np.fill(-v_maxcoeff)
    outofbounds_abort_high_np = np.zeros(v_dimens, dtype=np.float64)
    outofbounds_abort_high_np.fill(v_maxcoeff)

    if solution_found_threshold is None:
        # this is a square, so the absolute error is the square root of this
        solution_found_threshold = 1.e-6

    # default parameters for variable neighborhood search (fixed for now)
    default_rwss_high_pct = 1.
    default_rwss_low_pct = 0.5
    iterimprove_sufficient_pct = 0.2
    iterimprove_substantial_pct = 0.5
    iterimprove_ema_weight = 0.1

    rnd_int = rnd_dict['rnd_int']
    rnd_long = rnd_dict['rnd_long']
    rnd_count = len(rnd_long)

    cl_info = get_platform_device_info()
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose)

    # input buffers
    obj_target_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=obj_target_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
    exp_0_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=exp_0_np)
    initial_pos_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=pos_0_np)
    outofbounds_abort_low_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=outofbounds_abort_low_np)
    outofbounds_abort_high_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=outofbounds_abort_high_np)
    rnd_int_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=rnd_int)
    rnd_long_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=rnd_long)

    # outputs
    pos_best_np = np.empty_like(pos_0_np)
    pos_best_g = cl.Buffer(ctx, mf.WRITE_ONLY, pos_best_np.nbytes)
    pos_best_error_np = np.empty(v_count, dtype=np.float64)
    pos_best_error_g = cl.Buffer(ctx, mf.WRITE_ONLY, pos_best_error_np.nbytes)

    # auxiliary iteration outputs (all int / np.int32):
    ao = {}
    aux_output_list = ['iteration_cnt', 'exit_code',
                       'num_rwss_unchanged', 'num_rwss_decreased', 'num_rwss_increased',
                       'num_iterimprove_insufficient', 'num_iterimprove_insufficient_ema',
                       'num_iterimprove_substantial', 'num_iterimprove_nothingfound']
    for aux_output in aux_output_list:
        ao[aux_output + '_np'] = np.empty(v_count, dtype=np.int32)
        ao[aux_output + '_g'] = cl.Buffer(ctx, mf.WRITE_ONLY, ao[aux_output + '_np'].nbytes)

    # code
    LIBQUIXAL_DIR = "../libquixal/"

    prg = build_program(
        ctx,
        defines_list=[f'DIMENS {v_dimens}', f'_RND_COUNT {rnd_count}',
                      f'NUM_PARTICLES_MAX {NUM_PARTICLES_MAX}', f'NUM_TABU_1_STEPS_MAX {NUM_TABU_1_STEPS_MAX}'],
        files_list=[f'{LIBQUIXAL_DIR}qx_helper.h',
                    f'{LIBQUIXAL_DIR}qx_vector.h',
                    f'{LIBQUIXAL_DIR}qx_exp.h',
                    f'{LIBQUIXAL_DIR}qx_random.h',
                    f'{LIBQUIXAL_DIR}qx_searchlog_swarm_rw_tabu.h',
                    f'{LIBQUIXAL_DIR}qx_searchlog_varneighborhood.h',
                    f'{LIBQUIXAL_DIR}qx_helper.cl',
                    f'{LIBQUIXAL_DIR}qx_vector.cl',
                    f'{LIBQUIXAL_DIR}qx_exp.cl',
                    f'{LIBQUIXAL_DIR}qx_random.cl',
                    f'{LIBQUIXAL_DIR}qx_searchlog_swarm_rw_tabu.cl',
                    f'{LIBQUIXAL_DIR}qx_searchlog_varneighborhood.cl',
                    'ex031_032_033_searchlog_varneighborhood.cl'],
        verbose=verbose, line_numbers=verbose
    )
    knl = prg.test_searchlog_varneighborhood

    # --------------------
    if not silent:
        printi(f'Call kernel in GPU {device.name}'
               f' [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}] ...')

    knl(queue, [v_count], None,
        obj_target_g,
        strucons_g, np.int32(exp_poly_order), exp_0_g, np.int32(isleft_int),
        np.int32(initial_num_particles), np.int32(initial_tabu_1_steps),
        initial_pos_g,
        np.float64(initial_rwss_med),
        np.float64(default_rwss_high_pct), np.float64(default_rwss_low_pct),
        np.float64(iterimprove_sufficient_pct), np.float64(iterimprove_substantial_pct),
        np.float64(iterimprove_ema_weight),
        np.int32(this_max_iterations),
        np.float64(solution_found_threshold),
        outofbounds_abort_low_g, outofbounds_abort_high_g,
        np.int32(random_seed),
        rnd_int_g, rnd_long_g,
        pos_best_g, pos_best_error_g,
        ao['iteration_cnt_g'],
        ao['exit_code_g'],
        ao['num_rwss_unchanged_g'],
        ao['num_rwss_decreased_g'],
        ao['num_rwss_increased_g'],
        ao['num_iterimprove_insufficient_g'],
        ao['num_iterimprove_insufficient_ema_g'],
        ao['num_iterimprove_substantial_g'],
        ao['num_iterimprove_nothingfound_g'])

    cl.enqueue_copy(queue, pos_best_np, pos_best_g)
    cl.enqueue_copy(queue, pos_best_error_np, pos_best_error_g)
    cl.enqueue_copy(queue, ao['exit_code_np'], ao['exit_code_g'])

    if not silent:
        printi('... done.')
    # --------------------

    # reshape the result as v_count vectors of dimension v_dimens
    pos_best_vecs_np = pos_best_np.reshape((v_count, v_dimens))

    if verbose_telemetry:
        printi('Verbose telemetry:')
        printi(f'pos_best_vecs_np:\n{pos_best_vecs_np}')
        for aux_output in aux_output_list:
            cl.enqueue_copy(queue, ao[f'{aux_output}_np'], ao[f'{aux_output}_g'])
            printi(f'{aux_output}:\n{ao[f"{aux_output}_np"]}')

    return pos_best_vecs_np, pos_best_error_np, ao['exit_code_np']
