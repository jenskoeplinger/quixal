# quixal - ex032_searchlog_varneighborhood_general.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# While examples ex030... and ex031... were using composition algebras where the results from the
# algorithm here could be compared against the expected, known outcome, this example runs various
# other algebras with arbitrary structure constants.
#
# As before, given a target (an "objective") y that is a vector in the algebra, it attempts to find
# values x such that exp(x) = y. It can be understood as generalization of the conventional logarithm.
#
# In order to make it easier to test other algebras (see also the next two example, ex033...)
# the common code for running the algorithm (including optionally calculating and visualizing the search tracers)
# is split into a separate helper file, ex032_033_common.py.

import math
import random
import time

import numpy as np

import ex032_033_common


def main():
    """
    Code starts executing here.
    """

    max_dimens = 4  # between 2 and max_dimens dimensions

    v_count = 1000  # how many vectors to test
    exp_poly_order = 100  # order of the polynomial approximating the exponential function

    algebras_to_test = [
        {"v_dimens": 3,
         "random_seed": 163492281,
         "show_interim": True},  # if True: make additional picture with traces of the searchers (slow!)
        {"v_dimens": 2,
         "random_seed": 1638492308,
         "show_interim": True},
        {"v_dimens": 2,
         "random_seed": 1638492329,
         "show_interim": True},
        {"v_dimens": 3,
         "random_seed": 1638541256,
         "show_interim": True},
        {"v_dimens": random.randint(2, max_dimens),
         "random_seed": int(time.time_ns() / 1000000000),
         "show_interim": True}
    ]

    # Prepare random number lookup tables
    rnd_count = 111119  # 111,119 random numbers in the tables (the size must be a prime number)
    rnd_dict = ex032_033_common.init_random_tables(rnd_count)

    # Initialize start values (may be overridden, in the order of test parameters provided)
    initial_num_particles = 3
    initial_tabu_1_steps = 10
    max_iterations = 100
    initial_rwss_med = 0.6
    v_maxcoeff = 5. * math.pi  # maximum absolute value of an individual coefficient
    silent = False
    cap_dimens = None  # project all results into the (X[0], X[1] plane)
    cap_coeff = None  # initialize all searchers anywhere in the (v_dimens)-dimensional vector space

    for this_test in range(len(algebras_to_test)):
        v_dimens = algebras_to_test[this_test]["v_dimens"]
        random_seed = algebras_to_test[this_test]["random_seed"]
        random.seed(random_seed)
        np.random.seed(random_seed)
        show_interim = algebras_to_test[this_test]["show_interim"]

        strucons_np = np.random.rand(v_dimens * v_dimens * v_dimens).astype(np.float64) * 2. - 1.
        # value of exp(0) is fixed unit vector in the 0-direction, [1, 0, ..., 0]
        algebra = f"random {v_dimens}D algebra"
        subtitle = f"(random seed {random_seed}; searchers initialized in {v_dimens}D, orthogonal projection)"

        pos_0_np, pos_0_vecs_np = ex032_033_common.get_random_vectors(
            v_count, v_dimens, v_maxcoeff, cap_dimens=cap_dimens, cap_coeff=cap_coeff, random_seed=random_seed)

        pos_best_vecs_np_array = None
        exit_codes_np_array = None

        for x_axis_idx in range(v_dimens - 1):
            for y_axis_idx in range(x_axis_idx + 1, v_dimens):
                # run the simulation once, then visualize the result in every axis combination pair
                xlabel = f"x[{x_axis_idx}]"
                ylabel = f"x[{y_axis_idx}]"

                pos_best_vecs_np_array, exit_codes_np_array = ex032_033_common.run_test_simulations_with_interim(
                    pos_0_np, v_count, v_maxcoeff,
                    v_dimens, strucons_np, algebra, exp_poly_order,
                    initial_num_particles, initial_tabu_1_steps, initial_rwss_med,
                    rnd_dict, random_seed,
                    max_iterations, show_interim,
                    xlabel, ylabel,
                    silent=silent,
                    size_x=14, size_y=14,
                    x_axis_idx=x_axis_idx, y_axis_idx=y_axis_idx,
                    use_previous_simulation_for_plot=pos_best_vecs_np_array,
                    prev_exit_codes_np_array=exit_codes_np_array,
                    subtitle=subtitle
                )

        silent = True


# ========================================
# execute

main()
