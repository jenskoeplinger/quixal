// quixal - ex020_021_022_exponential_function.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by:
//    - ex020_real_exponentiation.py
//    - ex021_composition_algebra_exp.py
//    - ex022_general_exponential_function.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space
//
// Required include:
//    libquixal/qx_vector.cl
//    libquixal/qx_exp.cl


__kernel void exponential_function(
    __global const double *a_g,
    int exp_poly_order,
    __global const double *strucons_g,
    __global double *res_g,
    int isglobal)
{

  int gid = get_global_id(0);
  int idx = gid * DIMENS;

  // (1) assemble the input vector from the global input

  double a_vec[DIMENS];

  int i = 0;
  while (i < DIMENS) {
    a_vec[i] = a_g[idx + i];
    i++;
  }

  // (2) perform the calculation

  double exp_0[DIMENS];
  qx_unit_vector(exp_0, 0); // pick exp(0) := (1, 0, ..., 0) by definition
  double result_vec[DIMENS];
  qx_clear_vector(result_vec);

  if (isglobal == 1) {
    // leave structure constants in global memory space, calculate right away

    qx_exp_left_g(a_vec, exp_0, result_vec, strucons_g, exp_poly_order);

  } else {
    // first copy structure constants into private memory space of each work item, then calculate

    double strucons[DIMENS * DIMENS * DIMENS];

    int i = 0;
    while (i < DIMENS * DIMENS * DIMENS) {
      strucons[i] = strucons_g[i];
      i++;
    }

    qx_exp_left(a_vec, exp_0, result_vec, strucons, exp_poly_order);
  }

  // (3) distribute the result into the global output

  i = 0;
  while (i < DIMENS) {
    res_g[idx + i] = result_vec[i];
    i++;
  }

}


