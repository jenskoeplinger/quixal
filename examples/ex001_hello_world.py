# quixal - ex001_hello_world.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate 1 + 2 in real-number approximation (and test that it is close to 3).

import pprint

import numpy as np
import pyopencl as cl

from helpers.common_helper import printi, get_context

# create inputs for calculating 1 + 2 = 3:
#   - inputs are a one-element vectors of one-dimensional numbers a_np "1." and b_np "2." (as 64-bit float)
#   - prepare the result vector res_np
a_np = np.array([1.]).astype(np.float64)
b_np = np.array([2.]).astype(np.float64)
res_np = np.empty_like(a_np)

# get some available OpenCL acceleration context; see helpers.common_helper.py for details
ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context()
printi(f'Selected environment:\n\n{pprint.pformat(env)}\n')

# Prepare OpenCL input buffers:
a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
# Prepare OpenCL return buffer:
number_of_bytes = a_np.nbytes
res_g = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

# ----------------------
# Read static OpenCL files (.cl) into string.
LIBQUIXAL_DIR = "../libquixal/"

defines = f"""
#define DIMENS 1
"""

with open(f'{LIBQUIXAL_DIR}qx_vector.h', 'r') as vector_h:
    prg_vector_h = vector_h.read()
with open(f'{LIBQUIXAL_DIR}qx_vector.cl', 'r') as vector_cl:
    prg_vector_cl = vector_cl.read()
with open('ex001_hello_world.cl', 'r') as file_hello_world_cl:
    prg_kernel_cl = file_hello_world_cl.read()

prg_text = defines + prg_vector_h + prg_vector_cl + prg_kernel_cl

# ----------------------
printi(f'Build GPU program and create kernel:\n{prg_text}\n')
prg = cl.Program(ctx, prg_text).build()
knl = prg.hello_world

# ----------------------
printi('Call kernel to do just one single calculation ([1]) ...')
knl(queue, [1], None, a_g, b_g, res_g)
cl.enqueue_copy(queue, res_np, res_g)
printi('...done.')
# ----------------------

printi(f'Test the result: {a_np} + {b_np} = {res_np}')
norm_difference = np.linalg.norm(res_np - (a_np + b_np))
if norm_difference > 1e-9:
    printi('\n\n***** TEST FAILED *****\n')
else:
    printi('Test success.')
