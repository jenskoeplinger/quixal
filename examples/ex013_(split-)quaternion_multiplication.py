# quixal - ex013_(split-)quaternion_multiplication.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate random samples of quaternion and split-quaternion multiplication.
# This file performs the identical calculation as in ex011_... and ex_012_..., but here
# using the structure constants representing quaternion and split-quaternion multiplication in four dimensions.
#
#   - Get two lists of 4D vectors, representing both quaternions and split-quaternions,
#   - define the structure constants for both quaternion and split-quaternion multiplication,
#   - for each pair of numbers, calculate the product in both algebras,
#   - test it against what would be expected from Python; include a few "friendly" tests that are easy to inspect;
#     some of those tests will have the identical result in both algebras, some will be different.
#
# For more information see:
#   - quaternions https://en.wikipedia.org/wiki/Quaternion
#   - split-quaternions https://en.wikipedia.org/wiki/Split-quaternion

import random
import time

import numpy as np

from ex011_012_013_014_vector_multiplication import execute_vector_multiplication
from helpers.common_helper import printi

v_dimens = 4  # dimensionality the vector space; (split-)quaternions are four-dimensional
v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 5.  # maximum absolute value of an individual coefficient

num_tests = 1000  # how many spot-wise tests to do once finished
num_friendly_tests = 5  # output the first few tests human-friendly at the end (for visual inspection)

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Setting up {v_count} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_dimens * v_count).astype(np.float64)
b_np = np.random.rand(v_dimens * v_count).astype(np.float64)
# scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff
b_np = b_np * 2. * v_maxcoeff - v_maxcoeff

# Make the first few vector pairs special, so that we can test these simple examples just by looking at them
#   - "2 * 3 = 6", here represented as [2, 0, 0, 0] * [3, 0, 0, 0] = [6, 0, 0, 0]
a_np[0] = 2
a_np[1] = 0
a_np[2] = 0
a_np[3] = 0
b_np[0] = 3
b_np[1] = 0
b_np[2] = 0
b_np[3] = 0
#   - "i * i = -1", here represented as [0, 1, 0, 0] * [0, 1, 0, 0] = [-1, 0, 0, 0]
a_np[4] = 0
a_np[5] = 1
a_np[6] = 0
a_np[7] = 0
b_np[4] = 0
b_np[5] = 1
b_np[6] = 0
b_np[7] = 0
#   - "i * j = k", here represented as [0, 1, 0, 0] * [0, 0, 1, 0] = [0, 0, 0, 1]
a_np[8] = 0
a_np[9] = 1
a_np[10] = 0
a_np[11] = 0
b_np[8] = 0
b_np[9] = 0
b_np[10] = 1
b_np[11] = 0
# quaternions:
#   - "j * j = -1", here represented as [0, 0, 1, 0] * [0, 0, 1, 0] = [-1, 0, 0, 0]
# split-quaternions:
#   - "j * j = +1", here represented as [0, 0, 1, 0] * [0, 0, 1, 0] = [1, 0, 0, 0]
a_np[12] = 0
a_np[13] = 0
a_np[14] = 1
a_np[15] = 0
b_np[12] = 0
b_np[13] = 0
b_np[14] = 1
b_np[15] = 0
# quaternions:
#   - "j * k = i", here represented as [0, 0, 1, 0] * [0, 0, 0, 1] = [0, 1, 0, 0]
# split-quaternions:
#   - "j * k = -i", here represented as [0, 0, 1, 0] * [0, 0, 0, 1] = [0, -1, 0, 0]
a_np[16] = 0
a_np[17] = 0
a_np[18] = 1
a_np[19] = 0
b_np[16] = 0
b_np[17] = 0
b_np[18] = 0
b_np[19] = 1

printi(f'\n\n---------------------------- (1) QUATERNION ----------------------------\n')
printi(f'Set up structure constants modeling quaternion multiplication:')
strucons_np = np.array([
    #  1 * ...:
    1, 0, 0, 0,  # 1 * 1 = 1
    0, 1, 0, 0,  # 1 * i = i
    0, 0, 1, 0,  # 1 * j = j
    0, 0, 0, 1,  # 1 * k = k
    #  i * ...:
    0, 1, 0, 0,  # i * 1 = i
    -1, 0, 0, 0,  # i * i = -1
    0, 0, 0, 1,  # i * j = k
    0, 0, -1, 0,  # i * k = -j
    #  j * ...:
    0, 0, 1, 0,  # j * 1 = j
    0, 0, 0, -1,  # j * i = -k
    -1, 0, 0, 0,  # j * j = -1
    0, 1, 0, 0,  # j * k = i
    #  k * ...:
    0, 0, 0, 1,  # k * 1 = k
    0, 0, 1, 0,  # k * i = j
    0, -1, 0, 0,  # k * j = -i
    -1, 0, 0, 0  # k * k = -1
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np
)
# ------------------------

printi(f'\n\n---------------------------- (2) SPLIT-QUATERNION ----------------------------\n')
printi(f'Set up structure constants modeling split-quaternion multiplication:')
strucons_np = np.array([
    #  1 * ...:
    1, 0, 0, 0,  # 1 * 1 = 1
    0, 1, 0, 0,  # 1 * i = i
    0, 0, 1, 0,  # 1 * j = j
    0, 0, 0, 1,  # 1 * k = k
    #  i * ...:
    0, 1, 0, 0,  # i * 1 = i
    -1, 0, 0, 0,  # i * i = -1
    0, 0, 0, 1,  # i * j = k
    0, 0, -1, 0,  # i * k = -j
    #  j * ...:
    0, 0, 1, 0,  # j * 1 = j
    0, 0, 0, -1,  # j * i = -k
    1, 0, 0, 0,  # j * j = 1 (compare to quaternions: -1)
    0, -1, 0, 0,  # j * k = -i (compare to quaternions: +i)
    #  k * ...:
    0, 0, 0, 1,  # k * 1 = k
    0, 0, 1, 0,  # k * i = j
    0, 1, 0, 0,  # k * j = i (compare to quaternions: -i)
    1, 0, 0, 0  # k * k = 1 (compare to quaternions: -1)
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np,
    verbose=False
)
# ------------------------

printi(f'\n\n---------------------------- (3) 2x2 MATRIX MULTIPLICATION (SPLIT-QUATERNION) ----------------------\n')
printi(f'Set up structure constants modeling 2x2 matrix multiplication:')

strucons_np = np.array([
    #  a * ...:
    1, 0, 0, 0,  # a * A -> [A]
    0, 1, 0, 0,  # a * B -> [B]
    0, 0, 0, 0,  # a * C = 0
    0, 0, 0, 0,  # a * D = 0
    #  b * ...:
    0, 0, 0, 0,  # b * A = 0
    0, 0, 0, 0,  # b * B = 0
    1, 0, 0, 0,  # b * C -> [A]
    0, 1, 0, 0,  # b * D -> [B]
    #  c * ...:
    0, 0, 1, 0,  # c * A -> [C]
    0, 0, 0, 1,  # c * B -> [D]
    0, 0, 0, 0,  # c * C = 0
    0, 0, 0, 0,  # c * D = 0
    #  d * ...:
    0, 0, 0, 0,  # d * A = 0
    0, 0, 0, 0,  # d * B = 0
    0, 0, 1, 0,  # d * C -> [C]
    0, 0, 0, 1  # d * D -> [D]
]).astype(np.float64)
printi(f'   Linearized: {strucons_np}')
strucons_vec_np = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='C')
printi(f'   Reshaped as three-dimensional structure constants:\n{strucons_vec_np}')

# ------------------------
execute_vector_multiplication(
    v_dimens, v_count,
    num_tests, num_friendly_tests,
    a_np, b_np, strucons_np,
    verbose=False
)
# ------------------------
