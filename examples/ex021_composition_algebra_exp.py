# quixal - ex021_composition_algebra_exp.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Sample from the exponential function in the composition algebras over the reals: (split-)complex,
# (split-)quaternion, and (split-)octonion. In these algebras, the exponential function traces out
# circles (complex, quaternion, octonion) and hyperbolae (in the split-algebras, respectively) in the
# plane spanned by the real axis and the argument.
#
# The result is visualized, also to get an idea of accuracy for larger arguments.
#
# Links:
#   - composition algebras https://en.m.wikipedia.org/wiki/Composition_algebra
#   - Euler's formula exp(ix) = cos(x) + i sin(x) https://en.m.wikipedia.org/wiki/Euler%27s_formula
import random

import math
import matplotlib.pyplot as plt
import numpy as np
import pyopencl as cl
import time

from helpers import qx_vector_cl_helper as v_cl
from helpers import strucons_helper as strucons
from helpers.common_helper import get_context, get_platform_device_info, build_program, printi, show_plot


# ========================================
# main()

def main():
    """
    Code starts executing here. All helper functions are afterwards for readability.
    """

    v_count = 10000  # how many vectors (one thousand)
    v_maxcoeff = 4. * math.pi  # maximum absolute value of an individual coefficient
    exp_poly_order = 50  # order of the polynomial approximating the exponential function (here: to the 50th power)

    random_seed = int(time.time_ns() / 1000000000)
    random.seed(random_seed)

    # --------------------

    # (1) complex numbers, random argument
    xlabel = 'real part of x, re(x)'
    ylabel = 'imaginary part of x, imag(x)'
    strucons_np, v_dimens, exp_0, algebra = strucons.complex_numbers()

    title = make_title(f'(1) argument samples x (random seed = {random_seed}, count = {v_count})')
    a_np, a_vecs_np = get_random_vectors(v_count, v_dimens, v_maxcoeff)
    show_plot(title, xlabel, ylabel, a_vecs_np)

    title = make_title(f'(1) result exp(x) ({algebra}), poly order {exp_poly_order}')
    res_np, res_vecs_np = opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order)
    show_plot(title, xlabel, ylabel, res_vecs_np)

    # (2) complex numbers, imaginary argument (real component is zero)
    title = make_title(f'(2) result exp(imag(x)) ({algebra}), poly order {exp_poly_order}')
    a_imag_np = a_np.copy()
    a_imag_np[0::v_dimens] = 0
    res_np, res_vecs_np = opencl_build_and_run(a_imag_np, v_count, v_dimens, strucons_np, exp_poly_order, verbose=False)
    show_plot(title, xlabel, ylabel, res_vecs_np)

    # (3) split-complex numbers, random argument
    ylabel = 'split-imaginary part'
    strucons_np, v_dimens, exp_0, algebra = strucons.split_complex_numbers()

    title = make_title(f'(3) result exp(x) ({algebra}), poly order {exp_poly_order}')
    res_np, res_vecs_np = opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, verbose=False)
    show_plot(title, xlabel, ylabel, res_vecs_np)
    title = f'{title} (zoomed-in)'
    show_plot(title, xlabel, ylabel, res_vecs_np, xlim=[-0.1, 3.9], ylim=[-2., 2.])

    # (4) split-complex numbers, split-imaginary argument (real component is zero)
    title = make_title(f'(4) result exp(split-imag(x)) ({algebra}), poly order {exp_poly_order}')
    res_np, res_vecs_np = opencl_build_and_run(a_imag_np, v_count, v_dimens, strucons_np, exp_poly_order, verbose=False)
    show_plot(title, xlabel, ylabel, res_vecs_np)
    title = f'{title} (zoomed-in)'
    show_plot(title, xlabel, ylabel, res_vecs_np, xlim=[-0.1, 3.9], ylim=[-2., 2.])

    # (5) check the sine function for accuracy, in various polynomial orders
    strucons_np, v_dimens, exp_0, algebra = strucons.complex_numbers()

    v_maxcoeff_test = 16. * math.pi
    a_np, a_vecs_np = get_random_vectors(v_count, v_dimens, v_maxcoeff_test)
    a_np[0::v_dimens] = 0  # force imaginary

    poly_orders = [10, 25, 50, 75, 100, 125]
    abs_diff_to_numpy_sine = []
    sine_func_list = []

    for this_poly_order in poly_orders:
        make_title(f'(5) calculate sine function ({algebra}), poly order {this_poly_order}')
        res_np, res_vecs_np = opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, this_poly_order, verbose=False)
        sine_func = np.column_stack((a_vecs_np[:, 1], res_vecs_np[:, 1]))
        sine_func_list.append(sine_func)

        # In polynomial order 100 you can already see some degradation (noise) for large values of x.
        # Going to polynomial order 150 then doesn't add precision anymore. Collect the difference to the
        # numpy sine function (the numpy sine function implementation is implemented cyclical, i.e.,
        # its precision doesn't degrade with larger absolute values of the argument). This way we can plot it later.

        numpy_sine = np.sin(a_vecs_np[:, 1])
        this_abs_diff_to_numpy_sine = np.abs(numpy_sine - res_vecs_np[:, 1])
        # limit to minimum precision in float64:
        this_abs_diff_to_numpy_sine = np.clip(this_abs_diff_to_numpy_sine, 1.e-17, None)
        abs_diff_to_numpy_sine.append(this_abs_diff_to_numpy_sine)

    # ... aggregate plot of all polynomial orders
    fig, ax = plt.subplots(figsize=(14, 7))
    title = make_title(f'(5) sine function approximation ({algebra}) by polynomial order')
    plt.title(title)
    xlabel = 'x (imaginary only)'
    plt.xlabel(xlabel)
    plt.ylabel('sin(|x|) == imag(exp(x))')
    plt.xlim([-v_maxcoeff_test, v_maxcoeff_test])
    plt.ylim([-5., 5.])
    ax.axhline(y=0, color='k', linewidth=(0.5))
    ax.axvline(x=0, color='k', linewidth=(0.5))

    colors = ['blue', 'orange', 'green', 'grey', 'red', 'yellow', 'purple']

    for this_poly_order_idx in reversed(range(len(poly_orders))):
        # overlay all sine functions
        plt.scatter(x=sine_func_list[this_poly_order_idx][:, 0], y=sine_func_list[this_poly_order_idx][:, 1],
                    s=1, c=colors[this_poly_order_idx],
                    label=f'poly order {poly_orders[this_poly_order_idx]}')

    plt.legend()
    plt.show()

    # (6) aggregate precision plot of the sine function, by polynomial order
    fig, ax = plt.subplots(figsize=(7, 7))
    title = make_title(f'(6) precision of the sine function (as compared to np.sin)')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel('| ( |exp(x)| - np.sin(|x|) ) |')
    ax.set_yscale('log')

    for this_poly_order_idx in reversed(range(len(poly_orders))):
        # draw the difference between sin(x) from quixal and np.sin(x), depending on polynomial order
        plt.scatter(x=a_vecs_np[:, 1], y=abs_diff_to_numpy_sine[this_poly_order_idx],
                    s=1, c=colors[this_poly_order_idx],
                    label=f'poly order {poly_orders[this_poly_order_idx]}')
    plt.legend()
    plt.show()

    # There's a systematic error that grows linear with increasing absolute value of the argument. This is likely
    # caused by truncation errors when calculating and adding up polynomial terms. Note that quixal can't make the
    # assumptions of a cyclical function, as is done in np.sin(), because this property is only valid in the
    # special case of a complex number subspace of an algebra.

    # (7) testing that exp(x) in the (split-)quaternion and (split-)octonion algebras lie in the (real, x) plane

    algebras = [strucons.quaternions(), strucons.split_quaternions(), strucons.octonions(), strucons.split_octonions()]
    exp_poly_order = 100
    v_maxcoeff = v_maxcoeff / 2.  # use smaller coefficients

    for this_algebra in algebras:
        strucons_np, v_dimens, exp_0, disp = this_algebra
        printi(f'Checking {disp}s ({v_dimens}D, exp(0)={exp_0}), '
               f'{v_count} vectors with coefficients in [-{v_maxcoeff}, {v_maxcoeff}], '
               f'polynomial order {exp_poly_order}...')
        a_np, a_vecs_np = get_random_vectors(v_count, v_dimens, v_maxcoeff)
        res_np, res_vecs_np = opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, verbose=False)

        # To test the degree of how much a vector x is outside the (real, x) plane, check that all nonreal
        # coefficients of the result of exp(x) are the same scalar multiple of the corresponding coefficients of x.
        # Tally up the absolute difference from that factor.

        # get the nonreal parts of the argument and the result
        a_vecs_nonreal_np = a_vecs_np.copy()
        a_vecs_nonreal_np[:, 0] = 0
        res_vecs_nonreal_np = res_vecs_np.copy()
        res_vecs_nonreal_np[:, 0] = 0

        # Get the expected factor by which all individual coefficients should be scaled (sum of all cocoefficients)
        expected_scale_factor = res_vecs_nonreal_np.sum(axis=1) / a_vecs_nonreal_np.sum(axis=1)
        # For each coefficient, calculate the out-of-plane error (difference to expected scaling factor)
        diff_to_expected = (a_vecs_nonreal_np.T * expected_scale_factor).T - res_vecs_nonreal_np
        # Calculate the absolute error by squaring the out-of-plane error, and dividing it by the square norm of
        # the result. This yields a relative error (which is desirable because the exponential function
        # can take on large values rather quickly)
        res_nonreal_norm = (res_vecs_nonreal_np * res_vecs_nonreal_np).sum(axis=1)
        rel_diff_squared_sum = (diff_to_expected * diff_to_expected).sum(axis=1) / res_nonreal_norm

        # check to make sure all vectors have a small out-of-plane error (less than 10^(-9) different from 0)
        zeros_np = np.zeros(v_count)
        all_close = np.allclose(rel_diff_squared_sum, zeros_np, atol=1.e-9)

        # display average error, and biggest error
        avg_error = rel_diff_squared_sum.mean()
        max_error = rel_diff_squared_sum.max()

        printi(f'   All results of exp(x) lie in the (real, x) plane: {all_close}')
        printi(f'   Average out-of-plane error: {avg_error}')
        printi(f'   Biggest out-of-plane error: {max_error}')

        if all_close:
            printi(f'   -> SUCCESS.')
        else:
            printi(f'   ******* ERROR ******* Test failed. Check out-of-plane errors.')


# ========================================
# helper functions


def get_random_vectors(v_count, v_dimens, v_maxcoeff, nonreal=False):
    # create vectors with random numbers
    a_np = np.random.rand(v_count * v_dimens).astype(np.float64)
    if nonreal:
        # only return nonreal values, i.e., set the real component to 0
        a_np[0::v_dimens] = 0
    a_np = a_np * 2. * v_maxcoeff - v_maxcoeff  # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
    a_vecs_np = a_np.reshape((v_count, v_dimens))  # reshaped as v_count vectors of dimension v_dimens
    return a_np, a_vecs_np


def make_title(title):
    # primitive builder to print the title and echo it back
    printi(f'- {title}')
    return title


def opencl_build_and_run(a_np, v_count, v_dimens, strucons_np, exp_poly_order, verbose=True):
    """
    OpenCL context, command queue, memory flags, input/output, platforms, devices, build GPU program and create kernel.

    Inputs:
      a_np - list of v_count argument vectors in dimension v_dimens, represented as 1D numpy array
      strucons_np - structure constants describing the algebra, represented as 1D numpy array
      exp_poly_order - polynomial order up to which to approximate the power series

    Outputs:
      res_np - result vectors, represented as 1D numpy array
      res_vecs_np - result vectors, reshaped as v_count vectors of dimension v_dimens
    """

    v_cl.v_dimens = v_dimens

    cl_info = get_platform_device_info()
    ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context(verbose)
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
    number_of_bytes = a_np.nbytes
    res_np = np.empty_like(a_np)
    res = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

    LIBQUIXAL_DIR = "../libquixal/"

    prg = build_program(
        ctx,
        defines_list=[f'DIMENS {v_dimens}'],
        files_list=[f'{LIBQUIXAL_DIR}qx_vector.h', f'{LIBQUIXAL_DIR}qx_exp.h',
                    f'{LIBQUIXAL_DIR}qx_vector.cl', f'{LIBQUIXAL_DIR}qx_exp.cl',
                    'ex020_021_022_exponential_function.cl'],
        verbose=verbose
    )
    knl = prg.exponential_function

    # --------------------
    printi(f'Call kernel in GPU {device.name}'
           f' [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}] ...')
    USE_GLOBAL = 1
    knl(queue, [v_count], None, a_g, np.int32(exp_poly_order), strucons_g, res, np.int32(USE_GLOBAL))
    cl.enqueue_copy(queue, res_np, res)
    printi('... done.')
    # --------------------

    # reshape the result as v_count vectors of dimension v_dimens
    res_vecs_np = res_np.reshape((v_count, v_dimens))

    return res_np, res_vecs_np


# ========================================
# execute

main()
