# quixal - ex022_general_exponential_function.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate random samples of the exponential function,
# from an algebra represented by random structure constants.
#
#   - Get a number of vectors
#   - create a random algebra (structure constants)
#   - for each vector, calculate the exponential function in that algebra from its Taylor polynomial
#     (see e.g. https://en.wikipedia.org/wiki/Taylor_series)
#   - test it against what would be expected from Python
#
# These steps are done twice, once using the global constant memory space for the structure
# constants, and once using the private memory space of each work item for the structure constants.

import pprint
import random
import time

import numpy as np
import pyopencl as cl

from helpers import qx_exp_cl_helper as exp_cl
from helpers import qx_vector_cl_helper as v_cl
from helpers.common_helper import printi, get_timediff_ms_us, get_context, get_platform_device_info, build_program

v_dimens = 11  # dimensionality the vector space
v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 1.  # maximum absolute value of an individual coefficient
exp_poly_order = 50  # order of the polynomial approximating the exponential function

v_cl.v_dimens = v_dimens
exp_cl.v_dimens = v_dimens
num_tests = 1000  # how many spot-wise tests to do once finished

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Setting up {v_count} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_dimens * v_count).astype(np.float64)
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff  # scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
a_vecs_np = a_np.reshape((v_count, v_dimens))  # reshape them as v_count vectors of dimension v_dimens
printi(f'Set up random structure constants for modelling an algebra in {v_dimens} dimensions.')
strucons_np = np.random.rand(v_dimens * v_dimens * v_dimens).astype(np.float64) * 2. - 1.  # all coeff's in [-1, 1]

total_time_before = time.time_ns()
printi('Create OpenCL context, command queue, memory flags, input/output parameter buffer')
cl_info = get_platform_device_info()
printi(f'Available OpenCL platforms and devices:\n\n{pprint.pformat(cl_info)}\n')
ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context()
printi(f'Selected environment:\n\n{pprint.pformat(env)}\n')

# Input parameters:
a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
strucons_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=strucons_np)
# Return parameters:
number_of_bytes = a_np.nbytes
# results from using global memory space for structure constants
res_np_global = np.empty_like(a_np)
res_global = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)
# results from using private memory space for structure constants
res_np_private = np.empty_like(a_np)
res_private = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

# ----------------------
# Read static OpenCL files (.cl) into string.
LIBQUIXAL_DIR = "../libquixal/"

prg = build_program(
    ctx,
    defines_list=[f'DIMENS {v_dimens}'],
    files_list=[f'{LIBQUIXAL_DIR}qx_vector.h', f'{LIBQUIXAL_DIR}qx_exp.h',
                f'{LIBQUIXAL_DIR}qx_vector.cl', f'{LIBQUIXAL_DIR}qx_exp.cl',
                'ex020_021_022_exponential_function.cl']
)
knl = prg.exponential_function

# ----------------------
printi('Call kernel in GPU ...')
time_before = time.time_ns()
USE_GLOBAL = 1
USE_PRIVATE = 0
printi('   (1) leave structure constants in global memory')
knl(queue, [v_count], None, a_g, np.int32(exp_poly_order), strucons_g, res_global, np.int32(USE_GLOBAL))
cl.enqueue_copy(queue, res_np_global, res_global)
printi('       ... done (check timestamp of this line for duration).')
printi('   (2) copy structure constants to private memory of each work item')
knl(queue, [v_count], None, a_g, np.int32(exp_poly_order), strucons_g, res_private, np.int32(USE_PRIVATE))
cl.enqueue_copy(queue, res_np_private, res_private)
printi('       ... done (check timestamp of this line for duration).')
# ----------------------

time_after = time.time_ns()
printi(f'GPU Runtime: {get_timediff_ms_us(time_before, time_after)}')
total_time_after = time.time_ns()
printi(f'Total runtime (incl overhead): {get_timediff_ms_us(total_time_before, total_time_after)}')

# The result is still a one-dimensional vector, reshape back to the originally requested dimensionality:
res_vecs_np_global = res_np_global.reshape((v_count, v_dimens))
res_vecs_np_private = res_np_private.reshape((v_count, v_dimens))

# Spot-check on CPU with Numpy
print_num = 0
num_tests_error = 0
num_tests_success = 0
printi(f'Perform {num_tests} spot tests (print result sporadically on success; print all errors)')
for idx_test in range(num_tests):
    # pick a random vector to test
    # formula in the test was the exponential function (using left-multiplication)
    idx = random.randrange(v_count)
    pref = f'[test{idx_test + 1:6d}][index{idx:8d}]'
    a = a_vecs_np[idx]  # input vector a

    # calculate in Python:
    exp_0 = v_cl.qx_unit_vec(a, 0)
    result_vec = exp_cl.qx_exp_left(a, exp_0, strucons_np, exp_poly_order)

    # the result of the formula from the GPU:
    for scope in ['global', 'private']:
        if scope == 'global':
            res = res_vecs_np_global[idx]
        else:
            res = res_vecs_np_private[idx]

        norm_diff = np.linalg.norm(res - result_vec)
        if norm_diff > 1e-9:
            printi(f'{pref} FAILED - scope {scope} param {a} GPU result {res} is not close to Python {result_vec}')
            num_tests_error += 1
        else:
            num_tests_success += 1
            if idx_test >= print_num:
                print_num += (idx_test + 1)  # don't print successes all the time
                printi(f'{pref} Success - scope {scope} param {a} GPU result {res} is close to Python {result_vec}')

# -----------------------------
# print time summary
printi('------------------------------------------------')
printi('SUMMARY')
printi('------------------------------------------------')
printi(f'Number of tests: 2x{num_tests}')
printi(f'    Successes: {num_tests_success}')
printi(f'    Errors:    {num_tests_error}')
if num_tests_error > 0:
    printi(f'\n\n***** TEST FAILED *****\n')
printi(f'Calculated exponential function (up to polynomial order {exp_poly_order}) '
       f'for {v_count} vectors in {v_dimens} dimensions')
printi(f'GPU runtime (incl read-out):       {get_timediff_ms_us(time_before, time_after)}')
printi(f'Total GPU runtime (incl overhead): {get_timediff_ms_us(total_time_before, total_time_after)}')
overhead_factor = int(100 * (total_time_after - total_time_before) / (time_after - time_before)) / 100.
printi(f'Total GPU with overhead (create context on GPU, compile code into GPU) is {overhead_factor} times GPU runtime '
       f'(including data transfer to/from OS RAM from/to GPU RAM)')
printi(f'GPU: {device.name} [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}]')
