// quixal - ex033_exponential_function.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by:
//    - ex033_searchlog_select.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space
//
// Required include:
//    libquixal/qx_helper.cl
//    libquixal/qx_vector.cl
//    libquixal/qx_exp.cl


__kernel void exponential_function(
    __global const double *a_g,
    int exp_poly_order,
    __global const double *strucons_g,
    __global const double *exp_0_g,
    __global double *res_g,
    int isleft_int)
{

  int gid = get_global_id(0);
  int idx = gid * DIMENS;

  // (1) assemble the input vector from the global input

  double a_vec[DIMENS];

  int i = 0;
  while (i < DIMENS) {
    a_vec[i] = a_g[idx + i];
    i++;
  }

  // (2) perform the calculation

  double exp_0[DIMENS];
  copy_double_array_from_global_const(exp_0_g, exp_0, DIMENS, 0);
  double result_vec[DIMENS];
  qx_clear_vector(result_vec);
  bool isleft = (isleft_int > 0); // work around OpenCL bool size weirdness

  qx_exp_g(a_vec, exp_0, result_vec, strucons_g, exp_poly_order, isleft);

  // (3) distribute the result into the global output

  i = 0;
  while (i < DIMENS) {
    res_g[idx + i] = result_vec[i];
    i++;
  }

}


