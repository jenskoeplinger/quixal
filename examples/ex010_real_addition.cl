// quixal - ex010_real_addition.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by ex010_real_addition.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space (here: 1)
//
// Required include:
//    libquixal/qx_vector.cl


__kernel void real_addition(
    __global const double *a_g,
    __global const double *b_g,
    __global double *res_g)
{

  // This code is using GPU parallelism, i.e., the code gets called many times, with a "global ID" that starts
  // at 0 and increments up to the number of calls we've requested. The global ID is provided by the GPU,
  // our code has to figure out what to do with this. Here, the global ID indexes the numbers (1D vectors)
  // we want to add.
  int gid = get_global_id(0);

  // (1) get the input vectors (here: just 1D, i.e., a single coefficient) from the global input

  double a_vec[1];
  double b_vec[1];

  a_vec[0] = a_g[gid];
  b_vec[0] = b_g[gid];

  // (2) perform the calculation

  double result_vec[1];
  qx_add_vector(a_vec, b_vec, result_vec);

  // (3) put the result into the global output

  res_g[gid] = result_vec[0];

}


