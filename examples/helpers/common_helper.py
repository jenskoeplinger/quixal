# quixal - common_helper.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Some Python helper functions for the OpenCL wrappers:
#
#   - get_context()                                Gets an OpenCL runtime context (platform and device)
#   - get_platform_device_info()                   Convenience method to collect OpenCL capabilities in this runtime
#   - get_timediff_ns_disp(timediff_ns)            Returns string of the most significant timediff (ns, us, ms, or s)
#   - get_timediff_ms_us(time_before, time_after)  Returns string of ms and us of the given time difference
#   - printi(msg)                                  Print msg string with current time info, and time delta since last
#   - build_program(ctx, defines_list, files_list) Assembles the GPU program string and builds it
#   - quadratic_form(vec, signature)               Calculates the quadratic form of vec with a given signature
#   - show_plot(...)                               Simple helper to show decent plots (scatter, line)

import os

import matplotlib.pyplot as plt
import numpy as np
import pyopencl as cl
import time

TIME_PRINTI_NS = time.time_ns()


def get_context(verbose=True):
    """
    Creates OpenCL device context and reads device properties.
    """

    GPU_PLATFORM = os.environ.get("GPU_PLATFORM")
    GPU_DEVICE = os.environ.get("GPU_DEVICE")
    if verbose:
        printi(f'OS environment variables GPU_PLATFORM={GPU_PLATFORM}, GPU_DEVICE={GPU_DEVICE}')

    if GPU_PLATFORM is None:
        GPU_PLATFORM = 0  # typically this is the runtime OS

    # setup context and queue
    if GPU_DEVICE is None:
        # create "some" context
        ctx = cl.create_some_context()
        device = ctx.devices[0]
    else:
        platforms = cl.get_platforms()
        devices = platforms[int(GPU_PLATFORM)].get_devices()
        device = devices[int(GPU_DEVICE)]

    ctx = cl.Context(devices=[device])
    queue = cl.CommandQueue(ctx)
    mf = cl.mem_flags

    # collect environment information on the selected platform and device
    dev = ctx.devices[0]

    env = dict()
    env["platform_name"] = dev.platform.name
    env["platform_version"] = dev.platform.version
    env["dev_name"] = dev.name
    env["dev_version"] = dev.version
    env["dev_opencl_c_version"] = dev.opencl_c_version
    env["dev_driver_version"] = dev.driver_version
    env["dev_max_work_group_size"] = dev.get_info(cl.device_info.MAX_WORK_GROUP_SIZE)

    return ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE


def get_platform_device_info():
    """
    Returns a dictionary with human-readable platform and device information.
    """

    cl_info = dict()
    platforms = cl.get_platforms()

    for platform_idx in range(len(platforms)):
        platform = platforms[platform_idx]
        platform_info = {
            'name': platform.name,
            'version': platform.version
        }
        cl_info[f'platform_{platform_idx}'] = platform_info
        devices = platform.get_devices()
        for device_idx in range(len(devices)):
            device = devices[device_idx]
            device_info = {
                'name': device.name,
                'version': device.version,
                'opencl_c_version': device.opencl_c_version,
                'driver_version': device.driver_version,
                'MAX_WORK_GROUP_SIZE': device.get_info(cl.device_info.MAX_WORK_GROUP_SIZE)
            }
            platform_info[f'device_{device_idx}'] = device_info

    return cl_info


def get_timediff_ns_disp(timediff_ns):
    if timediff_ns >= 10000000000:
        time_seconds = int(timediff_ns / 1000000000)
        return f'{time_seconds:4d}s'
    elif timediff_ns >= 10000000:
        time_millis = int(timediff_ns / 1000000)
        return f'{time_millis:4d}ms'
    elif timediff_ns >= 10000:
        time_micros = int(timediff_ns / 1000)
        return f'{time_micros:4d}us'
    else:
        return f'{timediff_ns:4d}ns'


def get_timediff_ms_us(time_before, time_after):
    time_diff = time_after - time_before
    time_millis = int((time_diff) / 1000000)
    time_diff -= time_millis * 1000000
    time_micros = int(time_diff / 1000)
    return (f'{time_millis:4d}ms {time_micros:3d}us')


def printi(msg, verbose=True):
    if not verbose:
        return
    global TIME_PRINTI_NS
    this_time_ns = time.time_ns()
    print(f'[{this_time_ns} (+{get_timediff_ms_us(TIME_PRINTI_NS, this_time_ns)})] {msg}')
    TIME_PRINTI_NS = this_time_ns


def build_program(ctx, defines_list=None, files_list=None, verbose=True, line_numbers=False):
    """
    Given an OpenCL context and a list of defines, files, assembles all into a string,
    and compiles it into a program on the GPU. Returns the program reference.

    Inputs:
      - cts - the OpenCL context
      - defines_list - list of strings for definitions, e.g. ['TARGET=100', 'VARIANCE=10']
      - files_list - list of files to be read and appended as strings, e.g. ['lib.h', 'lib.cl', 'kernel.cl']

    Returns:
      - reference to the compiled program
    """

    prg_text = ""

    if defines_list is not None:
        prg_text += '// DEFINES (compile-time constants)\n\n'
        for define_item in defines_list:
            prg_text += f'#define {define_item}\n'

        prg_text += '\n\n'

    if files_list is not None:
        prg_text += '// FILES (read and appended as raw strings each)\n\n'
        for file_name in files_list:
            with open(f'{file_name}', 'r') as file:
                file_contents_string = file.read()
            prg_text += f'// file: {file_name}\n\n{file_contents_string}\n'

    if line_numbers:
        prg_disp = '\n'.join([f'[{number:5d}] {line}' for (number, line) in enumerate(prg_text.splitlines())])
    else:
        prg_disp = prg_text

    printi(f'Build GPU program:\n{prg_disp}\n', verbose)

    prg = cl.Program(ctx, prg_text).build()

    return prg


def quadratic_form(vec, signature):
    """
    Calculates the quadratic form of vec with the given signature.

    Inputs:
      - vec: a vector
      - signature: a vector of +1 and -1 of the same dimension as vec

    Result:
      The pairwise product of vec, vec, and signature.

    For example:
      vec = [ 1., 2. ]
      signature = [ 1., 1. ]
      quadratic_form(vec, signature) = 1. * 1. + 2. * 2. = 5.

    If the signature is all 1s, then the result is the quadratic (Euclidean) norm. If the first half of elements
    in the signature are 1 and second half are -1, then this is the split-quadratic (hyperbolic) form.

    See:
      - quadratic form https://en.wikipedia.org/wiki/Quadratic_form
      - norm https://en.wikipedia.org/wiki/Norm_(mathematics)
      - split-form e.g. from https://en.wikipedia.org/wiki/Real_form_(Lie_theory)

    Quadratic forms are one of the defining properties of composition algebras
    (https://en.wikipedia.org/wiki/Composition_algebra).
    """

    return np.sum(np.multiply(np.multiply(vec, vec), signature))


def show_plot(title, xlabel, ylabel, array,
              s=1, c=None, alpha=1.,
              size_x=7, size_y=7,
              xlim=None, ylim=None,
              draw_zero_axes=True,
              draw_diagonals=True,
              linewidth=1.,
              axeswidth=0.5,
              draw_lines=False,
              x_axis_idx=0, y_axis_idx=1,
              legend_handles=None):
    """
    Simple helper to show a decent plot (scatter, line)
    """

    fig, ax = plt.subplots(figsize=(size_x, size_y))
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if draw_diagonals:
        if xlim is None or ylim is None:
            abs_max = abs(np.max(array.flatten()))
            if abs_max == 0:
                abs_max = 1.
            abs_max *= 1.1
        else:
            abs_max = abs(max(xlim[0], xlim[1], ylim[0], ylim[1]))
        plt.plot([-abs_max, abs_max], [-abs_max, abs_max], c='lightgrey', linewidth=axeswidth)
        plt.plot([-abs_max, abs_max], [abs_max, -abs_max], c='lightgrey', linewidth=axeswidth)
    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)

    if len(array.shape) == 2:
        # 2D array; interpret as list of (x, y)
        if draw_lines:  # lines
            plt.plot(array[:, x_axis_idx], array[:, y_axis_idx], c=c, alpha=alpha, linewidth=linewidth)
        else:  # scatter plot
            plt.scatter(x=array[:, x_axis_idx], y=array[:, y_axis_idx], s=s, c=c, alpha=alpha)
    elif len(array.shape) == 3:
        # 3D array; interpret first column as counter, then list of (x, y) each
        for cnt in range(array.shape[0]):
            if draw_lines:  # lines
                plt.plot(array[cnt, :, x_axis_idx], array[cnt, :, y_axis_idx], c=c, alpha=alpha, linewidth=linewidth)
            else:  # scatter plot
                if c is None:
                    this_col = None
                elif type(c) == list:
                    this_col = c[cnt]
                else:
                    this_col = c
                plt.scatter(array[cnt, :, x_axis_idx], array[cnt, :, y_axis_idx], s=s, c=this_col, alpha=alpha)
    else:
        printi(f'ERROR unexpected array shape: {array.shape}')

    if draw_zero_axes:
        ax.axhline(y=0, color='k', linewidth=axeswidth)
        ax.axvline(x=0, color='k', linewidth=axeswidth)

    if legend_handles is not None:
        plt.legend(handles=legend_handles)

    plt.show()
