# quixal - strucons_helper.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Provides structure constants and dimensionality of certain known algebras.
#
# In addition to the structure constants, returns the dimensionality, as well as
# the result of exp(0), i.e., the term in the power-series approximation of the
# exponential function exp(x) that does not depend on the argument x.
# Also returns a human-friendly string describing the algebra.
#
# A method "tensor_algs" performs the tensor product of two algebras, i.e., to use
# one algebra in the coefficients of another, and return structure constants (and dimension)
# that represent this construct as a single algebra.

import numpy as np


def real_numbers():
    """
    Real numbers here are simply the one-component axis [1.]
    """

    v_dimens = 1
    exp_0 = [1.]
    strucons_np = np.array([1.]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "real"


def complex_numbers():
    """
    Complex numbers (https://en.wikipedia.org/wiki/Complex_number)

    Basis notation (real, imaginary): (1, i)
    Characteristic product: i^2 = -1

    See also ex011_complex_multiplication.py
    """

    v_dimens = 2
    exp_0 = [1., 0]
    strucons_np = np.array([
        #  1 * ...:
        1, 0,  # 1 * 1 =  1
        0, 1,  # 1 * i =  i
        #  i * ...:
        0, 1,  # i * 1 =  i
        -1, 0  # i * i = -1
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "complex"


def split_complex_numbers():
    """
    Split-complex numbers (https://en.wikipedia.org/wiki/Split-complex_number)

    Basis notation (real, split-imaginary): (1, j)
    Characteristic product: j^2 = 1

    See also ex012_split-complex_multiplication.py
    """

    v_dimens = 2
    exp_0 = [1., 0]
    strucons_np = np.array([
        #  1 * ...:
        1, 0,  # 1 * 1 = 1
        0, 1,  # 1 * j = j
        # j * ...:
        0, 1,  # j * 1 = j
        1, 0  # j * j = 1 (compare to complex numbers: -1)
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "split-complex"


def quaternions():
    """
    Quaternions (https://en.wikipedia.org/wiki/Quaternion)

    Basis notation: (1, i, j, k)
    Characteristic products:
       i^2 = j^2 = k^2 = -1
       i * j = k
         (cyclic: j * k = i, k * i = j,
         anti-commutative: j * i = -k, k * j = -i, i * k = -j)

    See also ex013_(split-)quaternion_multiplication.py
    """

    v_dimens = 4
    exp_0 = [1., 0, 0, 0]
    strucons_np = np.array([
        #  1 * ...:
        1, 0, 0, 0,  # 1 * 1 = 1
        0, 1, 0, 0,  # 1 * i = i
        0, 0, 1, 0,  # 1 * j = j
        0, 0, 0, 1,  # 1 * k = k
        #  i * ...:
        0, 1, 0, 0,  # i * 1 = i
        -1, 0, 0, 0,  # i * i = -1
        0, 0, 0, 1,  # i * j = k
        0, 0, -1, 0,  # i * k = -j
        #  j * ...:
        0, 0, 1, 0,  # j * 1 = j
        0, 0, 0, -1,  # j * i = -k
        -1, 0, 0, 0,  # j * j = -1
        0, 1, 0, 0,  # j * k = i
        #  k * ...:
        0, 0, 0, 1,  # k * 1 = k
        0, 0, 1, 0,  # k * i = j
        0, -1, 0, 0,  # k * j = -i
        -1, 0, 0, 0  # k * k = -1
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "quaternion"


def split_quaternions():
    """
    Split-quaternions (https://en.wikipedia.org/wiki/Split-quaternion)

    Basis notation: (1, i, j, k)
    Characteristic products:
       i^2 = -1, j^2 = k^2 = 1
         (split-cyclic: i * j = k, j * k = -i, k * i = j, i.e., sign change when both factors are split-complex;
         anti-commutative)

    See also ex013_(split-)quaternion_multiplication.py
    """

    v_dimens = 4
    exp_0 = [1., 0, 0, 0]
    strucons_np = np.array([
        #  1 * ...:
        1, 0, 0, 0,  # 1 * 1 = 1
        0, 1, 0, 0,  # 1 * i = i
        0, 0, 1, 0,  # 1 * j = j
        0, 0, 0, 1,  # 1 * k = k
        #  i * ...:
        0, 1, 0, 0,  # i * 1 = i
        -1, 0, 0, 0,  # i * i = -1
        0, 0, 0, 1,  # i * j = k
        0, 0, -1, 0,  # i * k = -j
        #  j * ...:
        0, 0, 1, 0,  # j * 1 = j
        0, 0, 0, -1,  # j * i = -k
        1, 0, 0, 0,  # j * j = 1 (compare to quaternions: -1)
        0, -1, 0, 0,  # j * k = -i (compare to quaternions: +i)
        #  k * ...:
        0, 0, 0, 1,  # k * 1 = k
        0, 0, 1, 0,  # k * i = j
        0, 1, 0, 0,  # k * j = i (compare to quaternions: -i)
        1, 0, 0, 0  # k * k = 1 (compare to quaternions: -1)
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "split-quaternion"


def bicomplex():
    """
    Bicomplex numbers (https://en.wikipedia.org/wiki/Bicomplex_number)
    (i.e., complex numbers with complex coefficients, represented as 4D algebra over the reals)

    Basis notation: (1, i, j, k)
    Characteristic products:
       i^2 = k^2 = -1
       j^2 = 1
         (split-cyclic: i * j = k, j * k = i, k * i = -j;
         commutative)
    """

    v_dimens = 4
    exp_0 = [1., 0, 0, 0]
    strucons_np = np.array([
        #  1 * ...:
        1, 0, 0, 0,  # 1 * 1 = 1
        0, 1, 0, 0,  # 1 * i = i
        0, 0, 1, 0,  # 1 * j = j
        0, 0, 0, 1,  # 1 * k = k
        #  i * ...:
        0, 1, 0, 0,  # i * 1 = i
        -1, 0, 0, 0,  # i * i = -1
        0, 0, 0, 1,  # i * j = k
        0, 0, -1, 0,  # i * k = -j
        #  j * ...:
        0, 0, 1, 0,  # j * 1 = j
        0, 0, 0, 1,  # j * i = k
        1, 0, 0, 0,  # j * j = 1
        0, 1, 0, 0,  # j * k = i
        #  k * ...:
        0, 0, 0, 1,  # k * 1 = k
        0, 0, -1, 0,  # k * i = -j
        0, 1, 0, 0,  # k * j = i
        -1, 0, 0, 0  # k * k = -1
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "bicomplex"


def matrix_2x2():
    """
    Multiplying two 2x2 matrices with one another. The argument vectors
    (a, b, c, d) and (A, B, C, D) are understood as follows:

    / a b \   / A B \   / aA + bC    aB + bD \
    \ c d / * \ C D / = \ cA + dC    cB + dD /

    Note: 2x2 matrix multiplication over the real numbers is an alternative representation of the split-quaternion
    algebra. See method split_quaternions() above for more info.
    """

    v_dimens = 4
    exp_0 = [1., 0, 0, 1.]
    strucons_np = np.array([
        #  a * ...:
        1, 0, 0, 0,  # a * A -> [A]
        0, 1, 0, 0,  # a * B -> [B]
        0, 0, 0, 0,  # a * C = 0
        0, 0, 0, 0,  # a * D = 0
        #  b * ...:
        0, 0, 0, 0,  # b * A = 0
        0, 0, 0, 0,  # b * B = 0
        1, 0, 0, 0,  # b * C -> [A]
        0, 1, 0, 0,  # b * D -> [B]
        #  c * ...:
        0, 0, 1, 0,  # c * A -> [C]
        0, 0, 0, 1,  # c * B -> [D]
        0, 0, 0, 0,  # c * C = 0
        0, 0, 0, 0,  # c * D = 0
        #  d * ...:
        0, 0, 0, 0,  # d * A = 0
        0, 0, 0, 0,  # d * B = 0
        0, 0, 1, 0,  # d * C -> [C]
        0, 0, 0, 1  # d * D -> [D]
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "matrix_2x2"


def octonions():
    """
    Octonions (https://en.wikipedia.org/wiki/Octonion)

    Basis notation: (e0, e1, e2, e3, e4, e5, e6, e7), with e0 the real basis (unity, 1)
    Characteristic products:
       e1^2 = ... = e7^2 = -1
       (cyclic, with cycles abbreviated as 123, 145, 176, 246, 257, 347, 365, i.e., e1 * e2 = e3, and so on;
       anti-commutative)

    See also ex014_(split-)octonion_multiplication.py
    """

    v_dimens = 8
    exp_0 = [1., 0, 0, 0, 0, 0, 0, 0]
    strucons_np = np.array([
        #  e0 * ...:
        1, 0, 0, 0, 0, 0, 0, 0,  # e0 * e0 = e0
        0, 1, 0, 0, 0, 0, 0, 0,  # e0 * e1 = e1
        0, 0, 1, 0, 0, 0, 0, 0,  # e0 * e2 = e2
        0, 0, 0, 1, 0, 0, 0, 0,  # e0 * e3 = e3
        0, 0, 0, 0, 1, 0, 0, 0,  # e0 * e4 = e4
        0, 0, 0, 0, 0, 1, 0, 0,  # e0 * e5 = e5
        0, 0, 0, 0, 0, 0, 1, 0,  # e0 * e6 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e0 * e7 = e7
        #  e1 * ...:
        0, 1, 0, 0, 0, 0, 0, 0,  # e1 * e0 = e1
        -1, 0, 0, 0, 0, 0, 0, 0,  # e1 * e1 = -e0
        0, 0, 0, 1, 0, 0, 0, 0,  # e1 * e2 = e3
        0, 0, -1, 0, 0, 0, 0, 0,  # e1 * e3 = -e2
        0, 0, 0, 0, 0, 1, 0, 0,  # e1 * e4 = e5
        0, 0, 0, 0, -1, 0, 0, 0,  # e1 * e5 = -e4
        0, 0, 0, 0, 0, 0, 0, -1,  # e1 * e6 = -e7
        0, 0, 0, 0, 0, 0, 1, 0,  # e1 * e7 = e6
        #  e2 * ...:
        0, 0, 1, 0, 0, 0, 0, 0,  # e2 * e0 = e2
        0, 0, 0, -1, 0, 0, 0, 0,  # e2 * e1 = -e3
        -1, 0, 0, 0, 0, 0, 0, 0,  # e2 * e2 = -e0
        0, 1, 0, 0, 0, 0, 0, 0,  # e2 * e3 = e1
        0, 0, 0, 0, 0, 0, 1, 0,  # e2 * e4 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e2 * e5 = e7
        0, 0, 0, 0, -1, 0, 0, 0,  # e2 * e6 = -e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e2 * e7 = -e5
        #  e3 * ...:
        0, 0, 0, 1, 0, 0, 0, 0,  # e3 * e0 = e3
        0, 0, 1, 0, 0, 0, 0, 0,  # e3 * e1 = e2
        0, -1, 0, 0, 0, 0, 0, 0,  # e3 * e2 = -e1
        -1, 0, 0, 0, 0, 0, 0, 0,  # e3 * e3 = -e0
        0, 0, 0, 0, 0, 0, 0, 1,  # e3 * e4 = e7
        0, 0, 0, 0, 0, 0, -1, 0,  # e3 * e5 = -e6
        0, 0, 0, 0, 0, 1, 0, 0,  # e3 * e6 = e5
        0, 0, 0, 0, -1, 0, 0, 0,  # e3 * e7 = -e4
        #  e4 * ...:
        0, 0, 0, 0, 1, 0, 0, 0,  # e4 * e0 = e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e4 * e1 = -e5
        0, 0, 0, 0, 0, 0, -1, 0,  # e4 * e2 = -e6
        0, 0, 0, 0, 0, 0, 0, -1,  # e4 * e3 = -e7
        -1, 0, 0, 0, 0, 0, 0, 0,  # e4 * e4 = -e0
        0, 1, 0, 0, 0, 0, 0, 0,  # e4 * e5 = e1
        0, 0, 1, 0, 0, 0, 0, 0,  # e4 * e6 = e2
        0, 0, 0, 1, 0, 0, 0, 0,  # e4 * e7 = e3
        #  e5 * ...:
        0, 0, 0, 0, 0, 1, 0, 0,  # e5 * e0 = e5
        0, 0, 0, 0, 1, 0, 0, 0,  # e5 * e1 = e4
        0, 0, 0, 0, 0, 0, 0, -1,  # e5 * e2 = -e7
        0, 0, 0, 0, 0, 0, 1, 0,  # e5 * e3 = e6
        0, -1, 0, 0, 0, 0, 0, 0,  # e5 * e4 = -e1
        -1, 0, 0, 0, 0, 0, 0, 0,  # e5 * e5 = -e0
        0, 0, 0, -1, 0, 0, 0, 0,  # e5 * e6 = -e3
        0, 0, 1, 0, 0, 0, 0, 0,  # e5 * e7 = e2
        #  e6 * ...:
        0, 0, 0, 0, 0, 0, 1, 0,  # e6 * e0 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e6 * e1 = e7
        0, 0, 0, 0, 1, 0, 0, 0,  # e6 * e2 = e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e6 * e3 = -e5
        0, 0, -1, 0, 0, 0, 0, 0,  # e6 * e4 = -e2
        0, 0, 0, 1, 0, 0, 0, 0,  # e6 * e5 = e3
        -1, 0, 0, 0, 0, 0, 0, 0,  # e6 * e6 = -e0
        0, -1, 0, 0, 0, 0, 0, 0,  # e6 * e7 = -e1
        #  e7 * ...:
        0, 0, 0, 0, 0, 0, 0, 1,  # e7 * e0 = e7
        0, 0, 0, 0, 0, 0, -1, 0,  # e7 * e1 = -e6
        0, 0, 0, 0, 0, 1, 0, 0,  # e7 * e2 = e5
        0, 0, 0, 0, 1, 0, 0, 0,  # e7 * e3 = e4
        0, 0, 0, -1, 0, 0, 0, 0,  # e7 * e4 = -e3
        0, 0, -1, 0, 0, 0, 0, 0,  # e7 * e5 = -e2
        0, 1, 0, 0, 0, 0, 0, 0,  # e7 * e6 = e1
        -1, 0, 0, 0, 0, 0, 0, 0  # e7 * e7 = -e0
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "octonion"


def split_octonions():
    """
    Split-octonions (https://en.wikipedia.org/wiki/Split-octonion)

    Basis notation: (e0, e1, e2, e3, e4, e5, e6, e7), with e0 the real basis (unity, 1)
    Characteristic products:
       e1^2 = e2^2 = e3^2 = -1, e4^2 = ... = e7^2 = 1
       (split-cyclic, with cycles abbreviated as 123, 145, 176, 246, 257, 347, 365;
       anti-commutative)

    See also ex014_(split-)octonion_multiplication.py
    """

    v_dimens = 8
    exp_0 = [1., 0, 0, 0, 0, 0, 0, 0]
    strucons_np = np.array([
        #  e0 * ...:
        1, 0, 0, 0, 0, 0, 0, 0,  # e0 * e0 = e0
        0, 1, 0, 0, 0, 0, 0, 0,  # e0 * e1 = e1
        0, 0, 1, 0, 0, 0, 0, 0,  # e0 * e2 = e2
        0, 0, 0, 1, 0, 0, 0, 0,  # e0 * e3 = e3
        0, 0, 0, 0, 1, 0, 0, 0,  # e0 * e4 = e4
        0, 0, 0, 0, 0, 1, 0, 0,  # e0 * e5 = e5
        0, 0, 0, 0, 0, 0, 1, 0,  # e0 * e6 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e0 * e7 = e7
        #  e1 * ...:
        0, 1, 0, 0, 0, 0, 0, 0,  # e1 * e0 = e1
        -1, 0, 0, 0, 0, 0, 0, 0,  # e1 * e1 = -e0
        0, 0, 0, 1, 0, 0, 0, 0,  # e1 * e2 = e3
        0, 0, -1, 0, 0, 0, 0, 0,  # e1 * e3 = -e2
        0, 0, 0, 0, 0, 1, 0, 0,  # e1 * e4 = e5
        0, 0, 0, 0, -1, 0, 0, 0,  # e1 * e5 = -e4
        0, 0, 0, 0, 0, 0, 0, -1,  # e1 * e6 = -e7
        0, 0, 0, 0, 0, 0, 1, 0,  # e1 * e7 = e6
        #  e2 * ...:
        0, 0, 1, 0, 0, 0, 0, 0,  # e2 * e0 = e2
        0, 0, 0, -1, 0, 0, 0, 0,  # e2 * e1 = -e3
        -1, 0, 0, 0, 0, 0, 0, 0,  # e2 * e2 = -e0
        0, 1, 0, 0, 0, 0, 0, 0,  # e2 * e3 = e1
        0, 0, 0, 0, 0, 0, 1, 0,  # e2 * e4 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e2 * e5 = e7
        0, 0, 0, 0, -1, 0, 0, 0,  # e2 * e6 = -e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e2 * e7 = -e5
        #  e3 * ...:
        0, 0, 0, 1, 0, 0, 0, 0,  # e3 * e0 = e3
        0, 0, 1, 0, 0, 0, 0, 0,  # e3 * e1 = e2
        0, -1, 0, 0, 0, 0, 0, 0,  # e3 * e2 = -e1
        -1, 0, 0, 0, 0, 0, 0, 0,  # e3 * e3 = -e0
        0, 0, 0, 0, 0, 0, 0, 1,  # e3 * e4 = e7
        0, 0, 0, 0, 0, 0, -1, 0,  # e3 * e5 = -e6
        0, 0, 0, 0, 0, 1, 0, 0,  # e3 * e6 = e5
        0, 0, 0, 0, -1, 0, 0, 0,  # e3 * e7 = -e4
        #  e4 * ...:
        0, 0, 0, 0, 1, 0, 0, 0,  # e4 * e0 = e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e4 * e1 = -e5
        0, 0, 0, 0, 0, 0, -1, 0,  # e4 * e2 = -e6
        0, 0, 0, 0, 0, 0, 0, -1,  # e4 * e3 = -e7
        1, 0, 0, 0, 0, 0, 0, 0,  # e4 * e4 = e0 (compare to octonions: -e0)
        0, -1, 0, 0, 0, 0, 0, 0,  # e4 * e5 = -e1 (compare to octonions: e1)
        0, 0, -1, 0, 0, 0, 0, 0,  # e4 * e6 = -e2 (compare to octonions: e2)
        0, 0, 0, -1, 0, 0, 0, 0,  # e4 * e7 = -e3 (compare to octonions: e2)
        #  e5 * ...:
        0, 0, 0, 0, 0, 1, 0, 0,  # e5 * e0 = e5
        0, 0, 0, 0, 1, 0, 0, 0,  # e5 * e1 = e4
        0, 0, 0, 0, 0, 0, 0, -1,  # e5 * e2 = -e7
        0, 0, 0, 0, 0, 0, 1, 0,  # e5 * e3 = e6
        0, 1, 0, 0, 0, 0, 0, 0,  # e5 * e4 = e1 (compare to octonions: -e1)
        1, 0, 0, 0, 0, 0, 0, 0,  # e5 * e5 = e0 (compare to octonions: -e0)
        0, 0, 0, 1, 0, 0, 0, 0,  # e5 * e6 = e3 (compare to octonions: -e3)
        0, 0, -1, 0, 0, 0, 0, 0,  # e5 * e7 = -e2 (compare to octonions: e2)
        #  e6 * ...:
        0, 0, 0, 0, 0, 0, 1, 0,  # e6 * e0 = e6
        0, 0, 0, 0, 0, 0, 0, 1,  # e6 * e1 = e7
        0, 0, 0, 0, 1, 0, 0, 0,  # e6 * e2 = e4
        0, 0, 0, 0, 0, -1, 0, 0,  # e6 * e3 = -e5
        0, 0, 1, 0, 0, 0, 0, 0,  # e6 * e4 = e2 (compare to octonions: -e2)
        0, 0, 0, -1, 0, 0, 0, 0,  # e6 * e5 = -e3 (compare to octonions: e3)
        1, 0, 0, 0, 0, 0, 0, 0,  # e6 * e6 = e0 (compare to octonions: -e0)
        0, 1, 0, 0, 0, 0, 0, 0,  # e6 * e7 = e1 (compare to octonions: -e1)
        #  e7 * ...:
        0, 0, 0, 0, 0, 0, 0, 1,  # e7 * e0 = e7
        0, 0, 0, 0, 0, 0, -1, 0,  # e7 * e1 = -e6
        0, 0, 0, 0, 0, 1, 0, 0,  # e7 * e2 = e5
        0, 0, 0, 0, 1, 0, 0, 0,  # e7 * e3 = e4
        0, 0, 0, 1, 0, 0, 0, 0,  # e7 * e4 = e3 (compare to octonions: -e3)
        0, 0, 1, 0, 0, 0, 0, 0,  # e7 * e5 = e2 (compare to octonions: -e2)
        0, -1, 0, 0, 0, 0, 0, 0,  # e7 * e6 = -e1 (compare to octonions: e1)
        1, 0, 0, 0, 0, 0, 0, 0  # e7 * e7 = e0 (compare to octonions: -e0)
    ]).astype(np.float64)

    return strucons_np, v_dimens, exp_0, "split-octonion"


def tensor_algs(strucons_a_np, v_dimens_a, strucons_b_np, v_dimens_b):
    """
    Generates the structure constants from an algebra that is made by tensoring two algebras ("a" and "b").
    The dimensionality "v_dimens_x" of the resulting algebra "x" will be

       v_dimens_x = v_dimens_a * v_dimens_b

    The structure constants of the resulting algebra, "strucons_x_np", are formed by merging the structure constants
    from algebra "b" in the fast-moving index, and the structure constants from algebra "a" in the slow-moving index.
    """

    # prepare return variables
    v_dimens_x = v_dimens_a * v_dimens_b
    strucons_x_np = np.zeros(v_dimens_x * v_dimens_x * v_dimens_x, dtype=np.float64)

    # re-shape structure constants for incoming algebras ("a" and "b") for matrix product
    strucons_a_np_reshaped = strucons_a_np.reshape((v_dimens_a, v_dimens_a, v_dimens_a), order='F')
    strucons_b_np_reshaped = strucons_b_np.reshape((v_dimens_b, v_dimens_b, v_dimens_b), order='F')

    for idx_a_factor_l in range(v_dimens_a):
        for idx_b_factor_l in range(v_dimens_b):
            for idx_a_factor_r in range(v_dimens_a):
                for idx_b_factor_r in range(v_dimens_b):
                    # form product from algebra "a" (left-a * right-a)
                    vec_a_l = np.zeros(v_dimens_a, dtype=np.float64)
                    vec_a_l[idx_a_factor_l] = 1.
                    vec_a_r = np.zeros(v_dimens_a, dtype=np.float64)
                    vec_a_r[idx_a_factor_r] = 1.
                    vec_a_product = np.matmul(np.matmul(strucons_a_np_reshaped, vec_a_l), vec_a_r)

                    # form product from algebra "b" (left-b * right-b)
                    vec_b_l = np.zeros(v_dimens_b, dtype=np.float64)
                    vec_b_l[idx_b_factor_l] = 1.
                    vec_b_r = np.zeros(v_dimens_b, dtype=np.float64)
                    vec_b_r[idx_b_factor_r] = 1.
                    vec_b_product = np.matmul(np.matmul(strucons_b_np_reshaped, vec_b_l), vec_b_r)

                    # now, tensor these coefficients together (put "b" in the fast-moving index, then "right" factor)
                    strucons_x_idx_offset = idx_b_factor_r \
                                            + (v_dimens_b) * idx_a_factor_r \
                                            + (v_dimens_a * v_dimens_b) * idx_b_factor_l \
                                            + (v_dimens_b * v_dimens_a * v_dimens_b) * idx_a_factor_l
                    for idx_a_product in range(v_dimens_a):
                        for idx_b_product in range(v_dimens_b):
                            strucons_x_np[idx_b_product \
                                          + (v_dimens_b) * idx_a_product \
                                          + (v_dimens_a * v_dimens_b) * strucons_x_idx_offset] \
                                = vec_a_product[idx_a_product] * vec_b_product[idx_b_product]

    return strucons_x_np, v_dimens_x
