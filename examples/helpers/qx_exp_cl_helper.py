# quixal - qx_exp_cl_helper.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# File with Python methods to help validate the qx_exp.cl OpenCL code.
#
# Method names and signatures here match the OpenCL code as closely as reasonable.
# Their purpose is to write Python code that closely resembles OpenCL code for testing and verification,
# and are in no way written the way you should in Python with numpy (put mildly, they are not using best practices).
#
# See the corresponding OpenCL file for in-code documentation.
#
# Whereas the results of the OpenCL methods are written into memory addresses given by pointers,
# they are here returned from the Python functions.

from helpers import qx_vector_cl_helper as v_cl

v_dimens = None


def qx_exp_left(a, exp_0, strucons_np, exp_poly_order):
    return exp(a, exp_0, strucons_np, exp_poly_order, is_left=True)


def qx_exp_right(a, exp_0, strucons_np, exp_poly_order):
    return exp(a, exp_0, strucons_np, exp_poly_order, is_left=False)


def exp(a, exp_0, strucons_np, exp_poly_order, is_left):
    exp_a_this_p_ord = v_cl.qx_copy_vector(exp_0)
    exp_a = v_cl.qx_copy_vector(exp_0)

    for p_ord in range(1, exp_poly_order + 1):
        p_ord_d_inv = 1. / p_ord
        exp_a_this_p_ord = v_cl.qx_mult_scalar(exp_a_this_p_ord, p_ord_d_inv)

        if is_left:
            exp_a_this_p_ord = v_cl.qx_mult_vector(a, exp_a_this_p_ord, strucons_np)  # multiplies "a" from the left
        else:
            exp_a_this_p_ord = v_cl.qx_mult_vector(exp_a_this_p_ord, a, strucons_np)  # multiplies "a" from the right

        exp_a = v_cl.qx_add_vector(exp_a_this_p_ord, exp_a)

    return exp_a
