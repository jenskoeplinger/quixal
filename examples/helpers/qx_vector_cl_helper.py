# quixal - qx_vector_cl_helper.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# File with Python methods to help validate the qx_vector.cl OpenCL code.
#
# Method names and signatures here match the OpenCL code as closely as reasonable.
# Their purpose is to write Python code that closely resembles OpenCL code for testing and verification,
# and are in no way written the way you should in Python with numpy (put mildly, they are not using best practices).
#
# See the corresponding OpenCL file for in-code documentation.
#
# Whereas the results of the OpenCL methods are written into memory addresses given by pointers,
# they are here returned from the Python functions.

import numpy as np

v_dimens = None


def qx_add_vector(a, b):
    return a + b


def qx_clear_vector(a):
    return np.zeros_like(a)


def qx_copy_vector(a):
    return np.copy(a)


def qx_mult_dot(a, b):
    return np.dot(a, b)


def qx_mult_scalar(a, scalar):
    return a[:] * scalar


def qx_mult_vector(a, b, strucons_np):
    strucons = strucons_np.reshape((v_dimens, v_dimens, v_dimens), order='F')
    return np.matmul(np.matmul(strucons, a), b)


def qx_unit_vec(a, dimens):
    a = qx_clear_vector(a)
    a[dimens] = 1.
    return a
