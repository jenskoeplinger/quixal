// quixal - ex011_012_013_014_vector_multiplication.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by:
//          ex011_complex_multiplication.py
//          ex012_split-complex_multiplication.py
//          ex013_(split-)quaternion_multiplication.py
//          ex014_(split-)octonion_multiplication.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space:
//                 2 for (split-)complex,
//                 4 for (split-)quaternion, and
//                 8 for (split-)octonion.
//
// Required include:
//    libquixal/qx_vector.cl


__kernel void vector_multiplication(
    __global const double *a_g,
    __global const double *b_g,
    __global const double *strucons_g,
    __global double *res_g)
{

  // Using GPU parallelism and two-/four-/eight-dimensional vectors. Input and output parameters are
  // a one-dimensional array, i.e., the two-/four-/eight-dimensional vectors here (in private memory
  // scope) need to be re-shaped on global input and output.

  int gid = get_global_id(0);
  int idx = gid * DIMENS; // each value of gid corresponds to a vector with DIMENS coefficients

  // (1) get the input vectors from the global input (reshape from 1D array into 2D/4D/8D vector)

  double a_vec[DIMENS];
  double b_vec[DIMENS];

  int i = 0;
  while (i < DIMENS) {
    a_vec[i] = a_g[idx + i];
    b_vec[i] = b_g[idx + i];
    i++;
  }

  // (2) perform the calculation (using structure constants in global memory space)

  double result_vec[DIMENS];

  // Important! for the qx_mult_vector[_g](...) functions the result vector must be initialized all zeroes
  qx_clear_vector(result_vec);
  qx_mult_vector_g(a_vec, b_vec, result_vec, strucons_g);

  // (3) distribute the result into the global output (reshape from 2D/4D/8D vector back into 1D array)

  i = 0;
  while (i < DIMENS) {
    res_g[idx + i] = result_vec[i];
    i++;
  }

}


