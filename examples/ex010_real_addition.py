# quixal - ex010_real_addition.py
#
# Copyright (c) 2022 Jens Koeplinger
# Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
#
# Python wrapper to calculate random samples of real-number addition
# (here approximated by one-dimensional float64 vectors).
#
#   - Get two lists of random numbers (two lists of 1D vectors)
#   - take a number (1D vector) from each list, and add the pair
#   - put the result into a result list
#   - numbers at the same list index correspond to each other
#   - use GPU parallelism to do many calculations
#   - test it against what would be expected from Python
#
# Calculation is computed in parallel, i.e., taking advantage of GPU parallelization.

import pprint
import random
import time

import numpy as np
import pyopencl as cl

from helpers.common_helper import printi, get_context, get_platform_device_info

v_dimens = 1  # dimensionality the vector space (1D = single coefficient per vector)
v_count = 1000000  # how many vectors (one million)
v_maxcoeff = 100.  # maximum absolute value of an individual coefficient

# create vectors with random numbers
random_seed = int(time.time_ns() / 1000000000)
printi(f'Setting up {v_count} random vectors of dimension {v_dimens}. Seed: {random_seed}.')
random.seed(random_seed)
a_np = np.random.rand(v_dimens * v_count).astype(np.float64)
b_np = np.random.rand(v_dimens * v_count).astype(np.float64)
# scale coefficients to the [-v_maxcoeff, v_maxcoeff] range
a_np = a_np * 2. * v_maxcoeff - v_maxcoeff
b_np = b_np * 2. * v_maxcoeff - v_maxcoeff

printi('Create OpenCL context, command queue, memory flags, input/output parameter buffer')
cl_info = get_platform_device_info()
printi(f'Available OpenCL platforms and devices:\n\n{pprint.pformat(cl_info)}\n')
ctx, queue, mf, env, device, GPU_PLATFORM, GPU_DEVICE = get_context()
printi(f'Selected environment:\n\n{pprint.pformat(env)}\n')

# Input parameters:
a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
# Return parameters:
number_of_bytes = a_np.nbytes
res_np = np.empty_like(a_np)
res = cl.Buffer(ctx, mf.WRITE_ONLY, number_of_bytes)

# ----------------------
LIBQUIXAL_DIR = "../libquixal/"

defines = f"""
#define DIMENS {v_dimens}
"""

# Read static OpenCL files (.cl) into strings
with open(f'{LIBQUIXAL_DIR}qx_vector.h', 'r') as vector_h:
    prg_vector_h = vector_h.read()
with open(f'{LIBQUIXAL_DIR}qx_vector.cl', 'r') as vector_cl:
    prg_vector_cl = vector_cl.read()
with open('ex010_real_addition.cl', 'r') as file_real_addition_cl:
    prg_kernel_cl = file_real_addition_cl.read()

prg_text = defines \
           + prg_vector_h \
           + prg_vector_cl \
           + prg_kernel_cl

printi(f'Build GPU program and create kernels:\n{prg_text}\n')
prg = cl.Program(ctx, prg_text).build()
knl = prg.real_addition

# ----------------------
printi('Call kernel in GPU ...')
knl(queue, [v_count], None, a_g, b_g, res)
cl.enqueue_copy(queue, res_np, res)
printi('...done (see time diff of this message for run time).')
# ----------------------

# Test with Numpy

# -----------------------------
# print time summary
printi('------------------------------------------------')
printi('SUMMARY')
printi('------------------------------------------------')
res_test = a_np + b_np
diff_test = res_test - res_np
printi(f'Result samples ({v_count} total):\n\n{a_np}\n     +\n{b_np}\n     =\n{res_np}\n')
printi(f'Test all {v_count} results: Subtract the GPU result from the numpy control:\n\n{diff_test}\n')
norm_difference = np.linalg.norm(diff_test)
if norm_difference > 1e-9:
    printi('\n\n***** TEST FAILED *****\n')
else:
    printi('Test success.')
printi(f'GPU: {device.name} [GPU_DEVICE={GPU_DEVICE}] on {device.platform.name} [GPU_PLATFORM={GPU_PLATFORM}]')
