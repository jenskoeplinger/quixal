// quixal - ex030_searchlog_swarm_rw_tabu.cl
//
// Copyright (c) 2022 Jens Koeplinger
// Licensed under The MIT License (MIT). See file `LICENSE` for detail text.
//
// OpenCL code called by ex030_searchlog_swarm_rw_tabu.py
//
// Required constant:
//    DIMENS - the dimensionality of the vector space
//    _RND_COUNT - size of the random number tables (random.cl)
//    NUM_PARTICLES_MAX - maximum number of particles per swarm
//
// Required include:
//    libquixal/qx_vector.cl
//    libquixal/qx_exp.cl
//    libquixal/qx_random.cl
//    libquixal/qx_searchlog_swarm_rw_tabu.cl


__kernel void test_searchlog_swarm_rw_tabu(
    __global const double *pos_0_g,
    int exp_poly_order,
    int num_particles,
    int tabu_1_steps,
    double random_walk_step_size,
    int random_seed,
    __global const double *strucons_g,
    __global const int *rnd_int_g,
    __global const long *rnd_long_g,
    __global double *pos_best_g,
    __global double *pos_best_error_g,
    __global int *pos_best_particle_idx_g)
{

  int gid = get_global_id(0);
  int idx = gid * DIMENS;

  // (1) assemble the input vector from the global input

  double pos_0_vec[DIMENS];

  int i = 0;
  while (i < DIMENS) {
    pos_0_vec[i] = pos_0_g[idx + i];
    i++;
  }

  // (2) Set up randomizer

  struct qx_rnd_data rnd_data;
  rnd_data.rnd_int_g = rnd_int_g;
  rnd_data.rnd_long_g = rnd_long_g;
  qx_rnd_init(&rnd_data, gid + random_seed);

  // (3) Set up variables and functions to call into qx_searchlog_swarm_rw_tabu.cl

  double exp_0[DIMENS];
  qx_unit_vector(exp_0, 0);
  bool isleft = true;
  double obj_target[DIMENS];
  qx_copy_vector(exp_0, obj_target);

  struct qx_log_swarm_rw_tabu swarm;
  swarm.random_walk_step_sizes[0] = random_walk_step_size; // rwss_med
  swarm.random_walk_step_sizes[1] = random_walk_step_size; // rwss_high; try "* 10." for a ten times larger step size
  swarm.random_walk_step_sizes[2] = random_walk_step_size; // rwss_low; try "/ 10." for a ten times finer step size
  swarm.tabu_1_steps = tabu_1_steps;
  swarm.strucons_g = strucons_g;
  swarm.exp_poly_order = exp_poly_order;
  qx_copy_vector(exp_0, swarm.exp_0);
  swarm.isleft = isleft;
  qx_copy_vector(obj_target, swarm.obj_target);
  swarm.num_particles = num_particles;

  qx_copy_vector(pos_0_vec, swarm.pos_best);

  double function_pos_0[DIMENS];
  qx_clear_vector(function_pos_0);
  qx_exp_g(pos_0_vec, exp_0, function_pos_0, strucons_g, exp_poly_order, isleft);
  double pos_0_error = f_err_square_diff(function_pos_0, obj_target);

  qx_searchlog_exec_swarm_rw_tabu(&swarm, pos_0_vec, pos_0_error, &rnd_data);

  // (4) distribute the results into the global output

  i = 0;
  while (i < DIMENS) {
    pos_best_g[idx + i] = swarm.pos_best[i];
    i++;
  }

  pos_best_error_g[gid] = swarm.pos_best_error;
  pos_best_particle_idx_g[gid] = swarm.pos_best_particle_idx;

}


