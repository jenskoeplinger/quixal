# quixal - "Quixotic algebra" v0.1.4

An [OpenCL](https://en.m.wikipedia.org/wiki/OpenCL) library providing cross-platform
[GPU-accelerated](https://en.m.wikipedia.org/wiki/Graphics_processing_unit) methods for
finite-dimensional [algebras](https://en.m.wikipedia.org/wiki/Algebra_over_a_field), represented by
arbitrary [structure constants](https://en.m.wikipedia.org/wiki/Structure_constants), to calculate functions in one
variable, and sample from their inverses. Examples are written in
[Python](https://www.python.org/), and use the [`pyopencl`](https://documen.tician.de/pyopencl/) wrapper for GPU-native
code.

![Sine function approximation in the complexes by polynomial order](images/ex021/sine_approx_by_poly_order.png)

(image created in [`examples/ex021_composition_algebra_exp.py`](examples/ex021_composition_algebra_exp.py))

##### Table of contents

* quixal: "Quixotic algebra" v0.1.4
    * TL:DR how to run
    * About
    * Motivation
* Library methods
    * Basic algebra functions
        * qx_vector
        * qx_exp
    * Logarithm search
        * qx_searchlog_swarm_rw_tabu
        * qx_searchlog_varneighborhood
    * Supplemental
        * qx_helper
        * qx_random
* Examples
    * Addition
    * Multiplication
    * Exponentiation
    * Logarithm
* Install and set-up
    * OpenCL
    * Python 3 and PyOpenCL
* Algorithm to sample from inverses of a function
* Idea sink for future versions
* License
* References

## TL:DR how to run

Do this:

* [OpenCL](https://en.m.wikipedia.org/wiki/OpenCL) drivers
  (e.g. internet search: "How to install OpenCL on <platform; e.g.: Linux, Mac, Windows>?").
* The files in the [`libquixal`](libquixal/) directory are the includes for your OpenCL kernel. That's essentially it.

If you want working examples:

* The [`examples`](examples/) directory has examples, starting very simple
  ([`ex001_hello_world.py`](examples/ex001_hello_world.py)), using the
  Python [`pyopencl`](https://documen.tician.de/pyopencl/) wrapper ("How to install Python 3 on <platform>?",
  "How to install the Python 3 library <pyopencl, matplotlib, numpy, pprint>?").
* See below for a high-level intro in all examples, and then inside the code for more details.
* The [`images`](images/) directory has outputs from some of these examples.

## About

In linear algebra, given a
finite-dimensional [algebra (over a field)](https://en.m.wikipedia.org/wiki/Algebra_over_a_field)
represented by arbitrary [structure constants](https://en.m.wikipedia.org/wiki/Structure_constants), this library
provides GPU-accelerated code to calculate functions in one variable, and sample from their inverses.

The [vector space](https://en.m.wikipedia.org/wiki/Vector_space) is approximated by
[tuples](https://en.m.wikipedia.org/wiki/Tuple) with elements of
64-bit [floating point](https://en.m.wikipedia.org/wiki/Floating-point_arithmetic)
precision. Functions are defined through coefficients of a
[power-series](https://en.m.wikipedia.org/wiki/Power_series) approximation, i.e., through polynomials of finite order in
one variable.

In arbitrary algebras, given a value from
the [image of a general function](https://en.m.wikipedia.org/wiki/Image_(mathematics)), there may be either one
corresponding inverse value in the domain of the function, a discrete number of inverses, or infinitely many. For
example, the [square function](https://en.m.wikipedia.org/wiki/Square_(algebra)) in
the [real numbers](https://en.m.wikipedia.org/wiki/Real_number) generally has two inverses for any value in its image (
except 0 which is its own inverse). In contrast, the
[complex](https://en.m.wikipedia.org/wiki/Complex_number)
[exponential function](https://en.m.wikipedia.org/wiki/Exponential_function)
has infinitely many inverses for all values in its image space
(the [branches of the complex logarithm](https://en.wikipedia.org/wiki/Complex_logarithm)).

![Testing a subcomponent of the inverse sampling algorithm on the complex logarithm](images/ex030/test_swarm_rw_tabu_complex_log1_p20t20i50_465px.png)

(image created by [`examples/ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py))

## Motivation

From [Wiktionary](https://en.wiktionary.org/wiki/quixotic):

> quixotic (...) - 1. Possessing or acting with the desire to do noble and romantic deeds, without thought
> of realism and practicality; exceedingly idealistic.

The algebra methods in this library are sound. Surely they are useful. The name is also a nod towards Geoffrey M.
Dixon's book "Division Algebras, Lattices, Physics, Windmill Tilting" (CreateSpace Independent Publishing Platform,
August 2011) and the inspiring and frustrating experiences when trying out unusual things.

Irrespectively of uses of the term "quixotic" elsewhere, this library is pronounced after
the [original Spanish](https://en.m.wikipedia.org/wiki/Don_Quixote):

> /kiˈxal/ - /kiˈxotik/ algebra

# Library methods

Files in the [`libquixal`](libquixal/) directory are pairs of OpenCL code (`.cl` extension) and required header files
(`.h` extension). Header files may also include structs and constants.

Note that some additional constants may need to be provided where stated:
`DIMENS`, `NUM_PARTICLES_MAX`, `NUM_TABU_1_STEPS_MAX`, `_RND_COUNT`.

__Suggestion__: Start with the _example code_ (described further below, code in the [`examples`](examples/) folder), to
see how to use these methods.

## Basic algebra functions

* __qx_vector__: Vector addition, multiplication, dot product.
* __qx_exp__: Exponential map (approximation from Taylor polynomials).

### qx_vector

* `qx_add_vector(double *a, double *b, double *sum_ab)` adds vector `a` to `b` and returns it in `sum_ab`.
* `qx_clear_vector(double *a)` fills all coefficients of vector `a` with zeros.
* `qx_conjugate(double *a, double *a_bar)` inverts the sign of coefficients in `a` (except `a[0]` which is unchanged).
  Returns the result in `a_bar`.
* `qx_copy_vector(double *a, double *copy)` copies all coefficients of vector `a` into vector `copy`.
* `qx_invert(double *a)` inverts all coefficients of vector `a`.
* `qx_mult_dot(double *a, double *b)` performs the dot product (inner product) of vectors `a` and `b` (result is
  returned as double).
* `qx_mult_pairs(double *a, double *b, double *prod_abpairs)` performs pairwise coefficient multiplication.
* `qx_mult_scalar(double *a, double scalar, double *prod_ascalar)` multiplies vector `a` with `scalar` and returns it in
  vector `prod_ascalar`.
* `qx_mult_vector(double *a, double *b, double *prod_ab, double *strucons)` makes the vector product of `a` and `b`,
  given the structure constants `strucons` (in private memory space) and returns it in `prod_ab`.
    * NOTE: You must initialize the result vector `prod_ab` as a zero vector. Otherwise, the result of this vector
      product will be added to whatever coefficients were in `prod_ab` prior to this call.
* `qx_mult_vector_g(double *a, double *b, double *prod_ab, __global const double *strucons_g)` is the same as
  `qx_mult_vector`, but structure constants `strucons_g` are in global memory space.
    * NOTE: You must initialize the result vector `prod_ab` as a zero vector. Otherwise, the result of this vector
      product will be added to whatever coefficients were in `prod_ab` prior to this call.
* `qx_mult_vector_anticomm_g(double *a, double *b, double *prod_abplusba, __global const double *strucons_g)`
  convenience method that calculates the anticommutator, `a*b + b*a` (using `qx_mult_vector_g`).
* `qx_mult_vector_comm_g(double *a, double *b, double *prod_abminusba, __global const double *strucons_g)`
  convenience method that calculates the commutator, `a*b - b*a` (using `qx_mult_vector_g`).
* `qx_norm_squared(*a)` calculates the Euclidean square-norm of the vector `a`.
* `qx_norm_squared_diff(double *a, double *b)` is the Euclidean square-norm of the difference vector of `a` and `b`.
* `qx_unit_vector(double *a, int dimens)` fills vector `a` with zeroes, except for the coefficient at dimension
  `dimens` which will be filled with 1.

### qx_exp

* `qx_exp(double *a, double *exp_0, double *exp_a, double *strucons, int exp_poly_order, bool isleft)` calculates the
  exponential map of `a`, using `exp(0) := exp_0`, structure constants of th algebra in `strucons`, using left- or
  right-multiplication depending on the `isleft` flag, by approximating the Taylor polynomial up to
  order `exp_poly_order`. The result is returned in `exp_a`. For example, if choosing left-multiplication
  (`isleft=true`) then the first terms in the exponential map are calculated as:

`exp_a := exp_0 + a * exp_0 + (1/2!) a * (a * exp_0) + (1/3!) a * (a * (a * exo_0)) + ...`

* `qx_exp_g (double *a, double *exp_0, double *exp_a, __global const double *strucons_g, int exp_poly_order, bool isleft)`
  is the same as `qx_exp(...)`, only reading the structure constants from global memory.
* `qx_exp_left(...)`, `qx_exp_left_g(...)`, `qx_exp_right(...)`, and `qx_exp_right_g(...)` are convenience methods to
  make it more readable whether you're using left- or right-multiplication in the algebra.

## Logarithm search

* __qx_searchlog_swarm_rw_tabu__: Starting at a given seed coordinate, spawns a swarm of searchers that each perform a
  number of random-walk steps (with tabu size 1, i.e., stepping back is disallowed). This forms a single step in the
  subsequent iteration.
* __qx_searchlog_varneighborhood__: From a given seed coordinate, performs a number of iteration steps of swarm search,
  i.e., of `qx_searchlog_swarm_rw_tabu`. Based on the outcome from each iteration step, the local search parameters are
  adjusted.

For a detailed documentation of the algorithm, including rationale and alternatives considered, see
[`README_algorithm.md`](README_algorithm.md).

### qx_searchlog_swarm_rw_tabu

Entry point is:

```text
void qx_searchlog_exec_swarm_rw_tabu(
        struct qx_log_swarm_rw_tabu *swarm,
        double *pos_0,
        double pos_0_error,
        struct qx_rnd_data *rnd_data)
```

See e.g. [`examples/ex030_searchlog_swarm_rw_tabu.cl`](examples/ex030_searchlog_swarm_rw_tabu.cl) how to set up all
required data:

* `qx_log_swarm_rw_tabu *swarm` holds information on the particle swarm that is spawned, such as references to all
  particles, random-walk step sizes, positions, tracking data for best positions, as well as algebra definitions.
* `qx_particle_rw` is the struct that holds information on a single particle, e.g. its random-walk step size, how many
  steps to execute, and tracking data for best positions.
* `f_err_square_diff(double *function_pos, double *obj_target)` is the objective function (error function) that returns
  a value depending on the difference between the objective (obj_target) and the current sample
  (function_pos).

### qx_searchlog_varneighborhood

* The algorithm is documented in detail in the file [`README_algorithm.md`](README_algorithm.md).
* Entry point is `qx_searchlog_exec_varneighborhood(&searcher, &rnd_data)`.
* The searcher struct is initialized by calling `void qx_searchlog_init_varneighborhood(...)`.

See e.g. [`examples/ex031_032_033_searchlog_varneighborhood.cl`](examples/ex031_032_033_searchlog_varneighborhood.cl)
for how to set up the many arguments. See [`qx_searchlog_varneighborhood.h`](libquixal/qx_searchlog_varneighborhood.h),
`struct qx_log_varneighborhood`, for more information on these parameters.

## Supplemental

* __qx_helper__: Vector/array initialization and copy helpers.
* __qx_random__: Helpers for accessing given random number tables.

### qx_helper

Convenience helpers for initializing an array with a given value, copying one array into another, with variations for
global/private memory, double/int type.

```text
void init_int_array(int *array, int size, int value);
void copy_int_array(int *from, int *to, int size);
void copy_int_array_from_global_const(__global const int *from_g, int *to, int size, int gid);
void copy_int_array_to_global(int *from, __global int *to_g, int size, int gid);
void copy_double_array_from_global_const(__global const double *from_g, double *to, int size, int gid);
void copy_double_array_to_global(double *from, __global double *to_g, int size, int gid);
```

### qx_random

Functions for pseudo-random number lookup, from given random number tables.

Required constant: `_RND_COUNT` - number of entries in the random lookup lists; is a constant due to speed of modulo
division. Make sure this is a _prime number_ so that the random number generator may step through the entire table using
different step sizes at will, and still eventually use the entire table.

The `struct qx_rnd_data` is defined in `random_helper.h`, and holds the random tables, as well as index to the current
random entry, and step size after obtaining an entry.

Methods:

* `qx_rnd_init(struct qx_rnd_data *rnd_data, int init_id)` helps initialize the table index and step size
* `qx_rnd_get_double(struct qx_rnd_data)` returns the next random double (`float64`) from the table
* `qx_rnd_get_float(struct qx_rnd_data)` returns the next random float (`float32`) from the table
* `qx_rnd_get_int(struct qx_rnd_data)` return the next random int (`int32`) from the table
* `qx_rnd_get_int_lehmer(int random_seed)` returns a random integer (`int32`) corresponding to the given seed
* `qx_rnd_get_long(struct qx_rnd_data)` return the next random long (`int64`) from the table

See e.g. [`examples/ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py) from method
`init_random_tables(rnd_count)` how to set up the random number tables.

# Examples

All examples are written in Python, using the `pyopencl` wrapper of the OpenCL cross-platform GPU computation API. I've
tried to order them in a way that makes them easier to follow, with increasing complexity.

## Addition

* [`ex001_hello_world.py`](examples/ex001_hello_world.py) - All this does is ask the GPU to calculate 1 + 2, using the
  quixal include [`qx_vector.cl`](libquixal/qx_vector.cl). This example grabs the first available OpenCL compute device.
* [`ex010_real_addition.py`](examples/ex010_real_addition.py) - Adds one million randomly generated pairs of numbers (1D
  vectors of float64, approximating the real numbers) using the quixal include [`qx_vector.cl`](libquixal/qx_vector.cl)
  , GPU parallelism, and tests the output. This example also prompts you of all available GPU devices (and platforms) so
  that you can set the optional `GPU_DEVICE` (and `GPU_PLATFORM`) environment variables in case the default selection
  isn't what you want.

## Multiplication

* [`ex011_complex_multiplication.py`](examples/ex011_complex_multiplication.py) - Multiplies one million randomly
  generated pairs of complex numbers (2D vectors of float64, approximating the complex numbers) and test the output. The
  tests include some simple, immediately verifiable results, like `i * i = -1` and `(1 + i) * (1 + i) = 2i`. Use this
  example to see how to set up the structure constants for complex multiplication.
* [`ex012_split-complex_multiplication.py`](examples/ex012_split-complex_multiplication.py) - same functionality as the
  previous example, but now in the [split-complex numbers](https://en.wikipedia.org/wiki/Split-complex_number) (which
  are also two-dimensional like the complexes, however, the square of their nonreal basis element `j` is positive
  real, `j * j = 1`, in contrast to the imaginary basis in the complex numbers, `i`, which squares to `i * i = -1`). Yet
  they have similar algebraic properties as compared to the regular complex numbers in most of their space. See in the
  comments of that file for more details and references.
* [`ex013_(split-)quaternion-multiplication.py`](examples/ex013_(split-)quaternion_multiplication.py) - same
  functionality as `ex011_...` and `ex012_...`, now using the
  four-dimensional [quaternion](https://en.wikipedia.org/wiki/Quaternion)
  and [split-quaternion](https://en.wikipedia.org/wiki/Split-quaternion) algebras. Quaternions have widespread use in
  computer graphics and 3D algorithms, since you can take the extra fourth dimension to model general orientation in 3D
  without poles (i.e., without points on the surface of a sphere in 3D where one of your coordinates has no meaning;
  like longitudes on the North- and South Pole of Earth). Split-quaternions are a special algebra because they have
  similar algebraic properties as the quaternions in most of their space. In fact, the algebra created by 2x2 matrix
  multiplication over the reals is split-quaternion. See in the comments of the file for details and references.
* [`ex014_(split-)octonion-multiplication.py`](examples/ex014_(split-)octonion_multiplication.py) - same as `ex011_...`
  through `ex013_...`, now using the eight-dimensional [octonion](https://en.wikipedia.org/wiki/Octonion) and
  [split-octonion](https://en.wikipedia.org/wiki/Split-octonion) algebras. Octonions are special since they are the
  highest-dimensional algebra in which you can still add, subtract, multiply and divide any two numbers across their
  entire space (except for division by zero, as usual). They're used e.g. in Engineering wherever fast
  finite-dimensional orthogonal transformations are needed, such as the Hadamard transformation in signal processing, or
  OFDM in cellular and Wi-Fi radio encoding. Split-octonions can
  [model two balls rolling on one another](https://arxiv.org/abs/1205.2447), and turning without slipping, if their
  radii are exactly in relation 3 to 1. See in the comments of the file for details and references.
* [`ex015_octooctonion_multiplication.py`](examples/ex015_octooctonion_multiplication.py) - This specialized example is
  a deep-dive into algebraic properties from certain "tensored" algebras. Here, "tensoring" is meant to pick two
  algebras `A` and `B`, and use elements from algebra `A` as coefficients to algebra `B`. This can be written as `A(x)B`
  . The resulting vector space of `A(x)B` has the dimensionality of the product of the dimensions of vectors spaces
  from `A` and `B`, and elements in `A(x)B` can be understood as the Cartesian product between all elements in `A`
  and `B`. As an example, the "[bicomplex numbers](https://en.wikipedia.org/wiki/Bicomplex_number)"
  can be understood as two tensored copies of the complexes, C(x)C. Some of these algebras occur in
  the "[Freudenthal magic square](https://en.wikipedia.org/wiki/Freudenthal_magic_square)", for the construction of
  certain [Lie algebra](https://en.wikipedia.org/wiki/Lie_algebra)
  (pronounced /liː/ "Lee" after [Marius Sophus Lie](https://en.wikipedia.org/wiki/Sophus_Lie))
  and their [Lie groups](https://en.wikipedia.org/wiki/Simple_Lie_group). In particular, octonions are used in the
  construction of the exceptional Lie groups F_4, E_6, E_7, and E_8. Complex octonions and complex
  quad-octonions (["Dixon algebra"](https://arxiv.org/abs/1002.1497), RxCxHxO) reflect observed symmetries in physics.
  The code picks random samples from several algebras, and analyzes these samples for some basic algebraic properties (
  commutativity, associativity, alternativity, flexibility, power-associativity, nilpotence, and null spaces). See the
  in-code documentation for more information and details.
* [`ex015b_octooctonion_special.py`](examples/ex015b_octooctonion_special.py) is a specialized example that constructs a
  matrix commutator algebra similar to a [particular paper](https://arxiv.org/abs/2204.04996v1). See in-code
  documentation for details on the partial algebra that is probed. Note that in the paper, the authors construct an
  action algebra that first lets one octonion act on the result from the others' action, i.e., they're not truly
  performing octooctonion multiplication as it is done here. The code is initially copied from
  example `ex015_octooctonion_multiplication`, with most of it unchanged (to keep the example separate, but
  self-contained). All algebra elements are trivially nilpotent here since it is from
  a [Lie algebra](https://en.wikipedia.org/wiki/Lie_algebra), and with that, higher powers of any element are zero as
  well. Hopefully you don't mind this trivial output. It bugs me slightly that generation of the random samples is done
  in Python, slow and CPU-heavy, which takes the vast majority of compute time. One day I (or someone else?!)
  codes this in the GPU as well so that it all completes in a snap. __Note__: This is a 256-dimensional algebra, i.e.,
  it requires `256 * 256 * 256 = 2^24 = 16777216` structure constants. At 64-bit floating point precision, this
  requires `16777216 * 8 = 2^27 = 134217728` bytes of RAM (128 MBytes) to be transferred to the GPU compute shared
  memory. Some weaker gaming cards may have that (tested on NVidia GeForce GTX 860M on Linux), while some
  higher-performing productivity-oriented cards may run out of resources (tested on NVidia Quadro NVS 290 on Linux). I'm
  happy that Intel provides an OpenCL GPU compute environment for some of their OS/CPU combinations (tested Intel Core
  i9-9980HK on Mac OS) with generous memory and strong compute cores.

## Exponentiation

* [`ex020_real_exponentiation.py`](examples/ex020_021_022_exponential_function.cl) - The exponential function is
  calculated by approximating its polynomial series expansion (which is
  its [Taylor series](https://en.wikipedia.org/wiki/Taylor_series) around 0 in the reals), up to a given polynomial
  order. The result from the GPU calculation is tested against an implementation in Python using numpy that mimics the
  GPU calculation, as well as against the `numpy.exp(...)` function.
* [`ex021_composition_algebra_exp.py`](examples/ex021_composition_algebra_exp.py) -
  [Euler's formula](https://en.wikipedia.org/wiki/Euler%27s_formula) in the complexes, `exp(ix) = cos(x) + i sin(x)`, is
  a famous example of relating analysis (converging power series) and geometry (change of direction of a vector). This
  example calculates the exponential function of certain arguments in the
  so-called "[composition algebras](https://en.wikipedia.org/wiki/Composition_algebra)" over the reals, namely the
  (split-)complex, (split-)quaternion, and (split-)octonion algebras. Results are visualized, to confirm the expected
  result, and to get an idea of accuracy limits from finite polynomial powers and the library in general.

![Precision of sine function approximation by polynomial order](images/ex021/sine_approx_precision_by_poly_order_465px.png)

(image created in [`examples/ex021_composition_algebra_exp.py`](examples/ex021_composition_algebra_exp.py))

* [`ex022_general_exponential_function.py`](examples/ex022_general_exponential_function.py) - Calculates the generalized
  exponential function of one million vectors in a random eleven-dimensional algebra. Next to demonstrating calculations
  in an algebra represented by random structure constants, this example performs this calculation twice, once by reading
  the structure constants from the "`__global const`" GPU memory space, and once by first copying them into the private
  memory space of each individual work item.

## Logarithm

* [`ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py) - This example is testing the
  following subcomponents of the algorithm that samples from inverses of a
  function: [Local search](https://en.wikipedia.org/wiki/Local_search_(optimization)),
  [particle swarms](https://en.wikipedia.org/wiki/Particle_swarm_optimization),
  and [tabu search](https://en.wikipedia.org/wiki/Tabu_search) (primitive tabu size 1, i.e., stepping back is
  prohibited). While these subcomponents alone are not sufficient to sample inverses in general algebras from general
  functions, they nevertheless work in certain well-behaved scenarios. Given the objective to sample from inverses
  of `exp(x) = 1` in the complex numbers, the first test starts with a single particle per swarm doing a
  single [random walk](https://en.wikipedia.org/wiki/Random_walk)
  step, for a few iterations. Subsequent tests then increase these parameters (number of particles per swarm, random
  walk steps per iteration, and number of overall local search iterations) to confirm proper functioning and convergence
  on the expected result. The remaining tests sample solutions for `exp(x) = 1` in the split-complex, (split-)
  quaternion, and (split-)octonion algebras to check against known solutions in different algebras and dimensionality.
    * Sample images from the subcomponent test, looking for samples `x` satisfying `exp(x) = 1` in the complex
      numbers:  
      ![Search for solutions to exp(x) = 1 in the complexes (1 particles per swarm, random walk steps 1)](images/ex030/test_swarm_rw_tabu_complex_log1_p01t01i30_465px.png)
      ![Search for solutions to exp(x) = 1 in the complexes (10 particles per swarm, random walk steps 1)](images/ex030/test_swarm_rw_tabu_complex_log1_p10t01i30_465px.png)
      ![Search for solutions to exp(x) = 1 in the complexes (1 particles per swarm, random walk steps 10)](images/ex030/test_swarm_rw_tabu_complex_log1_p01t10i30_465px.png)
      ![Search for solutions to exp(x) = 1 in the complexes (10 particles per swarm, random walk steps 10)](images/ex030/test_swarm_rw_tabu_complex_log1_p10t10i30_465px.png)
      ![Search for solutions to exp(x) = 1 in the complexes (20 particles per swarm, random walk steps 20)](images/ex030/test_swarm_rw_tabu_complex_log1_p20t20i50_465px.png)
    * Same test ([`ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py)), but now using
      split-complex algebra. There is only one solution (`x=0`):  
      ![Search for solutions to exp(x) = 1 in the split-complexes (20 particles per swarm, random walk steps 20)](images/ex030/test_swarm_rw_tabu_split-complex_log1_p20t20i50_465px.png)
    * In the four-dimensional (split-)quaternion, and eight-dimensional (split-)octonion algebras, you have to make a
      decision on how to visualize the extra dimensions. Here, (split-)quaternion inverses for `exp(x) = 1` are
      visualized by orthogonal projection into the 2D plane, i.e., by simply ignoring the coefficients of the extra
      dimensions that are not the axes of the plot. This results in some fuzziness, because we don't know how close (or
      far away) from the plane of visualization each point is:  
      ![Search for solutions to exp(x) = 1 in the quaternions (20 particles per swarm, random walk steps 20)](images/ex030/test_swarm_rw_tabu_quaternion_log1_p20t20i50_ij_465px.png)
      ![Search for solutions to exp(x) = 1 in the split-quaternions (20 particles per swarm, random walk steps 20)](images/ex030/test_swarm_rw_tabu_split-quaternion_log1_p20t20i50_ij_465px.png)
    * In the eight-dimensional (split-)octonions, the plots would become even more fuzzy if you were to do the same
      approach, because it would project all six extra dimensions into the 2D plane of visualization. In order to
      emphasize the structure of solutions that indeed lie in the plane, and leave fuzziness where they're not, all
      searchers are now initialized near the plane of the plot, and not across the entire 8D space. This way, the
      algorithm mostly finds solutions that lie in that plane as well, because they are closest to the searchers.
      Conversely, where there is no nearby solution in the plane of visualization, the searchers start drifting into the
      extra dimensions, which results in fuzziness. The plots confirm the local neighborhood requirement of the
      algorithm, and the (known) structure of the solution spaces in these algebras:  
      ![Search for solutions to exp(x) = 1 in the octonions (10 particles per swarm started in the (e3, e4) plane, random walk steps 10)](images/ex030/test_swarm_rw_tabu_octonion_log1_p10t10i30_e3e4_465px.png)
      ![Search for solutions to exp(x) = 1 in the split-octonions (10 particles per swarm started in the (e3, e4) plane, random walk steps 10)](images/ex030/test_swarm_rw_tabu_split-octonion_log1_p10t10i30_e3e4_465px.png)
    * Run test ([`ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py)) to see more scenarios.
* [`ex031_searchlog_varneighborhood.py`](examples/ex031_searchlog_varneighborhood.py) - This example tests the complete
  algorithm that searches for inverses of the exponential function, i.e., the generalized logarithm. It follows
  essentially the order of tests from example
  [`ex030_searchlog_swarm_rw_tabu.py`](examples/ex030_searchlog_swarm_rw_tabu.py), as regression test. Some parameters
  are tweaked here to account for the difference in algorithm: With each iteration, the random walk step sizes are
  adjusted for different particles in the swarm, depending on the improvement that was achieved in the earlier
  iteration(s). Also, the number of particles per swarm, as well as the number of random walk steps for each particle,
  are dynamic as well in certain situations. The implementation of the algorithm gives an exit code with every searcher:
  Solution found (within a given margin of error), searcher ran out-of-bounds, and maximum number of iterations exceeded
  without having found a solution. There is also some aggregate "telemetry" that tallies up certain decisions made
  during iteration.  
  __Note__: All iterations are now calculated in the GPU in a single call. In order to generate the charts that show
  traces of each search, the result at iteration `iter` has to be computed by re-running all iterations `1..iter`
  with every call. This makes the example much slower. If you are only interested in the final result, without seeing
  the chart with the intermitted searcher traces, set the `"show_interim": True` parameter to `False`.  
  ![Search for solutions to exp(x) = 1 in the quaternions (3 initial particles per swarm, initial random walk steps 5)](images/ex031/test_varneighborhood_quaternion_log1_p3t5i30_ij_900px.png)
  ![Search for solutions to exp(x) = 1 in the split-quaternions (3 initial particles per swarm, initial random walk steps 5)](images/ex031/test_varneighborhood_split-quaternion_log1_p3t5i30_ij_900px.png)
* [`ex032_searchlog_varneighborhood_general.py`](examples/ex032_searchlog_varneighborhood_general.py) - After generating
  algebras represented by random structure constants, this example searches for `x` that satisfy `exp(x) = 1`, i.e., the
  generalized logarithm of `1` as in the previous examples. In order to be reproducible, the first sample algebras are
  generated with a specific random seed each. The goal of this test is to confirm the ability of the algorithm to find
  solutions in a reasonable amount of time, cover the entire search space, have searchers converge on solutions, and
  escape local minima or shallow gradients in the objective function. As before, it is very slow to generate the chart
  that shows the traces for each searcher. If you're only interested in the final result (with final exit codes and
  telemetry), set the `"show_interim": True` parameter to `False`. You can always copy down the random seed from the
  result chart, and then re-run that example with the tracer chart as well (`True`).  
  ![Search for solutions to exp(x) = 1 in a random 3D algebra - tracers](images/ex032/test_ex032_rnd163492281_3d_log1_p3t10i100_e0e1_tracers_900px.png)
  ![Search for solutions to exp(x) = 1 in a random 3D algebra - result](images/ex032/test_ex032_rnd163492281_3d_log1_p3t10i100_e0e1_result_900px.png)
* [`ex033_searchlog_select.py`](examples/ex033_searchlog_select.py) - The combination of select exponential maps and
  logarithms may give an idea on the geometry that's encoded in a given algebra. This example graphs select exponential
  maps, `exp( b alpha )` with fixed vector `b` in the algebra and real `alpha`, and samples from select logarithms. You
  can compare the exponential maps with the logarithms, and find the respective inverses. There are four algebras
  prepared here: The [bicomplex numbers](https://en.wikipedia.org/wiki/Bicomplex_number)
  (complex numbers with complex coefficients, understood as four-dimensional algebra over the reals),
  split-quaternions (as before), a "nuReal" 3D algebra that's investigated by John A Shuster, and the 6D algebra part of
  a system proposed in online discussion by user "lalo.sigma". See the code for more details and references.
  ![exp( (0,1,1,0) alpha ) in the bicomplex numbers](images/ex033/1_bicomplex_exp_i1i2_orbit.png)
  ![exp( (0,0,1,0) alpha ) in the split-quaternions (hyperbolic cosine and sine)](images/ex033/2_split-quaternion_exp_i2_orbit.png)
  ![exp( (-1,0,1) alpha ) in the nuReal numbers](images/ex033/3_nuReal_exp_(-1,0,1)_orbit.png)
  ![exp( x ) for uniformly distributed random x in the nuReal numbers](images/ex033/3_nuReal_exp_random_x0x2.png)
  ![exp( (0,0,0,1,0,0) alpha ) in lalo's 6D algebra](images/ex033/4_lalo1_exp_i3_orbit_zoom.png)
  ![exp( (0,0,0,-1,0,1) alpha ) in lalo's 6D algebra](images/ex033/4_lalo1_exp_(0,0,0,-1,0,1)_orbit_periodic.png)
  ![exp( (0,0,0,-1,1,1) alpha ) in lalo's 6D algebra](images/ex033/4_lalo1_exp_(0,0,0,-1,1,1)_orbit_semi-periodic.png)

# Install and set-up

Right now you'll have to set up a runtime environment for your platform, and probably the best way to go about this is
by doing internet searches for what's needed. See the [`README_idea_sink.md`](README_idea_sink.md)
for providing a declared runtime, e.g., as a [docker container](https://en.wikipedia.org/wiki/Docker_(software))
or [ansible script](https://en.wikipedia.org/wiki/Ansible_(software)). Doesn't exist yet, sorry.

## OpenCL

__Install__: Do an internet search for "How to install OpenCL on <platform; e.g.: Linux, Mac, Windows>?"

__Details__: This library in the [`libquixal`](libquixal/) folder is written for includes in
an [OpenCL](https://en.m.wikipedia.org/wiki/OpenCL) kernel. OpenCL is a cross-platform framework that aims at writing
code for GPU acceleration that is hardware independent and portable:

* You can run your code directly in an OpenCL runtime (as is done in the example code here),
* or you first compile it into the
  intermediary [SPIR](https://en.wikipedia.org/wiki/Standard_Portable_Intermediate_Representation)
  language, apply optimization methods there (if you want), and then compile it into hardware-specific code,
* or you transpile it into a hardware-specific language (with [NVidia CUDA](https://en.wikipedia.org/wiki/CUDA) being
  the easiest to do since it's very similar).

Disadvantages of using OpenCL directly, the way it is done here, include:

* Performance bottlenecks are hard to predict, even harder than when coding native, due to the differences in hardware
  architecture across vendors. Is your code bottle-necked through the type of memory access? Does the floating-point
  unit (not) support certain functions natively? Are code size or register use beyond the hardware's built-in
  optimization limits?
* Performance profilers are effective for hardware-specific tuning, which is not available through OpenCL.
* Without hardware-specific tuning, OpenCL code can perform poorly on some GPU compute platforms.

The rationale for choosing OpenCL is to provide GPU-ready code here that runs "out of the box" on most platforms. If a
research problem would indeed require significant compute time, then this may be a good starting point to evaluate
compute platforms for this particular problem. When considering a hardware platform, you may want to favor one with
strong compute cores (i.e., for each core a large number of wide registers, strong floating-point unit, large private
memory and memory cache, and so on), over one with lots of weaker compute cores (which each may lose time e.g. due to
register swapping, floating-point operations taking many cycles, private-memory shuffling).

I've tested the [`examples`](examples/) here on the following mix of hardware, old and new:

* AMD Radeon Pro 5500M,
* Intel Core i9-9980HK,
* NVidia GeForce GTX 860M,
* NVidia Quadro NVS 290.

## Python 3 and PyOpenCL

OpenCL is originally provided as a C++ library by its maintainers, but I found
the [`pyopencl`](https://documen.tician.de/pyopencl/) wrapper easier to work with. For one, it hides some of the gnarly
compile-time and exception-catching logic. I'm also quicker with writing visualizations in Python.

__Install__: You'll need to have a Python 3 runtime environment, and the following additional packages:

* `pprint`,
* `numpy`,
* `matplotlib`, and
* `pyopencl`.

# Algorithm to sample from inverses of a function

See file __[`README_algorithm.md`](README_algorithm.md)__.

# Idea sink for future versions

See file __[`README_idea_sink.md`](README_idea_sink.md)__.

# License

Licensed under The MIT License (MIT). See file [`LICENSE`](LICENSE) for detail text.

From https://tldrlegal.com/license/mit-license (retrieved 12 October 2021):

> A short, permissive software license. Basically, you can do whatever you want as long as you
> include the original copyright and license notice in any copy of the software/source.

# References

All in-text web links retrieved 12 Oct 2021 through 23 April 2022. For many detail references, see the in-code
documentation of the examples listed above. Sundry links from there (alphabetically):

* [Bicomplex numbers](https://en.wikipedia.org/wiki/Bicomplex_number)
* [Complex numbers](https://en.wikipedia.org/wiki/Complex_number)
* [Composition algebra](https://en.wikipedia.org/wiki/Composition_algebra)
* "Dixon Algebra" (Rx)CxHxO after G. M. Dixon, "Division Algebras: Octonions Quaternions Complex Numbers and the
  Algebraic Design of Physics" (1994), Kluwer; used also from the
  ["Towards a unified theory of ideals" (2010 paper by C. Furey)](https://arxiv.org/abs/1002.1497) line of research
* [Euler's formula](https://en.m.wikipedia.org/wiki/Euler%27s_formula)
* [Exponential function](https://en.wikipedia.org/wiki/Exponential_function)
* [Freudenthal magic square](https://en.wikipedia.org/wiki/Freudenthal_magic_square)
* [Ideal of an algebra (ring theory)](https://en.wikipedia.org/wiki/Ideal_(ring_theory))
* [Lie algebra](https://en.wikipedia.org/wiki/Lie_algebra)
    * ["An octonionic construction of E8 and the Lie algebra magic square" (2022 paper by R. A. Wilson, T, Dray, and C. Manogue)](https://arxiv.org/abs/2204.04996v1)
* [Lie group](https://en.wikipedia.org/wiki/Simple_Lie_group)
* [Null space (kernel of an algebra)](https://en.wikipedia.org/wiki/Kernel_(linear_algebra))
* [Octonions](https://en.wikipedia.org/wiki/Octonion)
* [Particle swarm optimization](https://en.wikipedia.org/wiki/Particle_swarm_optimization)
* [Power-associativity](https://en.wikipedia.org/wiki/Power_associativity)
* [Quaternions](https://en.wikipedia.org/wiki/Quaternion)
* [Random walk](https://en.wikipedia.org/wiki/Random_walk)
* [Split-complex numbers](https://en.wikipedia.org/wiki/Split-complex_number)
* [Split-octonions](https://en.wikipedia.org/wiki/Split-octonion)
    * [Split-octonions and the rolling ball (2012 paper by J. C. Baez and J. Huerta)](https://arxiv.org/abs/1205.2447)
* [Split-quaternions](https://en.wikipedia.org/wiki/Split-quaternion)
* [Tabu search](https://en.wikipedia.org/wiki/Tabu_search)
* [Taylor series](https://en.wikipedia.org/wiki/Taylor_series)
* [Zero divisors of an algebra](https://en.wikipedia.org/wiki/Zero_divisor)

