# quixal - "Quixotic algebra": Algorithm to sample from inverses of a function

This file describes the algorithm that is implemented for finding inverses of the exponential map.

##### Table of Contents

* Requirements
* Design
* Alternatives considered
    * Desired in a future version
    * Considered
    * Decided against

# Requirements

The algorithm that samples for inverse values of a given function must satisfy the following needs:

* Functions `f` are modeled as finite-dimensional polynomials in one variable. Function domain and image are represented
  by `n`-dimensional tuples approximating a real-valued vector space, i.e., `f : R^n -> R^n`. Real numbers are
  approximated by `float64` variables. Given a target value `y` from the function's image space, an inverse `x` is any
  value from the domain of a function `f` that satisfies `f(x) = y`. The goal of the algorithm is to sample from the set
  of all `{ x | f(x) = y, f : R^n -> R^n }`.
* Inverses to functions in a general algebra - where they exist - are generally multi-valued. Due to computational
  limitations of the power-series expansion, given a value from the image of a function, the search space where to look
  for inverses is necessarily limited. We want to sample solutions from the entire search space with a bias towards
  local neighborhoods. That is, we prefer finding small solution spaces that may exist across the entire search space,
  at the expense of under-representing large, dominant solution spaces in the total count of solutions found.
* The algorithm will propose candidates for a solution, and score these candidates according to some objective function.
  The better the score is, the closer the candidate needs to be to an actual solution. The algorithm will iteratively
  attempt to improve the scores of the objective function. When the score of a candidate is within a given threshold of
  the ideal score that corresponds to "no error", the candidate will be considered a solution. Candidates that don't
  reach the threshold to the ideal score will be discarded. At the end of each search, the algorithm will tally up the
  number of unique solutions found, together with some informational aggregates (e.g., number of iterations it took to
  find each solution, number of discarded candidates).
* The algorithm must efficiently detect whether a candidate is near a solution, far away, or unsure.
* The algorithm needs to help candidates converge onto solutions with little chance of accidentally escaping what would
  be good paths.
* When a candidate isn't near a solution, the algorithm needs to increase its random-search variability with increasing
  iteration count. The same needs to happen when the algorithm is unsure, i.e., when there is decreasing improvement in
  the objective function of a candidate over time.
* The algorithm nevertheless must be able to explore the entire search space. For example, the logarithm of 1 in the
  complex numbers, `log(1)`, has infinitely many solutions `Z * 2pi * i` where `Z` is any whole number. If the search
  space is the real `[-15, 15]` interval and the imaginary `[-15, 15]` interval, the algorithm must eventually find all
  five discrete solutions for `Z` in `{-2, -1, 0, 1, 2}`, for all searchers. Conversely, searching for the same in the
  quaternions, the algorithm must find representative samples from the solution space of `log(1)`, i.e., sampled from
  the infinitely many solutions on imaginary sphere surfaces spanned by the three nonreal axes, here with radius `2pi`
  and `4pi`. In the corners of the search space with coefficient ranges `[-15, 15]`, it also needs to find solutions
  from the spheres with radius `6pi` and `8pi`, since `15^2 + 15^2 + 15^2 ~ 26^2 > (8pi)^2`.

# Design

Recap: We're given a polynomial function `f : R^n -> R^n`, an algebra represented by arbitrary structure constants, and
a target `y` from the image of `f`. The goal is to find all `x` in a given search space that satisfy `f(x) = y`.
Such `x` are called "inverse of the function `f`" (at `y`). The reals `R` are approximated by `float64` variables, and
equality `f(x) = y` is approximated by a given threshold.

The search algorithm implements the following [metaheuristic search](https://en.wikipedia.org/wiki/Metaheuristic)
strategies:

* _Local search_: The algorithm uses a number of independent searchers, which are initialized with random uniform
  distributed vectors within the given search space. It will then iteratively attempt to improve the score (objective
  function, `obj`) at the location of each searcher. The score is calculated from the absolute difference between the
  function value `f(x_c)` at each candidate position `x_c` and the target `y`, i.e., `obj := |f(x_c) - y|`. At the
  beginning of every iteration, each searcher starts at the vector that was determined to be the most promising from the
  previous iteration. While workers are initially scattered out across the search space, they remain fully independent
  and each explore their respective local neighborhood. Different searchers may eventually find the same solution `x`,
  in particular when the number of possible solutions to `f(x) = y` in the search space is smaller than the number of
  searchers. Conversely, if `y` is not in the image of `f` for any `x` in the search space, no solution may be found.
* _Particle swarm optimization_: At the beginning of an iteration step, each searcher spawns a swarm of three (or more)
  particles, which independently perform a brief random walk (RW) each. Particles will have different random-walk step
  sizes (RWSS). One of the particles will be dedicated to have the midpoint RWSS (`rwss_med`) value, one will have a
  higher RWSS, `rwss_high`, and one has a lower RWSS, `rwss_low`. If there are more than three particles, their
  respective RWSS will be spread out between `rwss_high` and `rwss_low`.
* _Tabu search_ (short-term memory only): Each particle independently executes a number of successive RW steps, collects
  samples of the objective function along the way, without abandoning the walk until a fixed number of steps. Going back
  in the direction of the previous step is disallowed (tabu size 1). The position with the best objective function
  value (i.e., with the lowest error) is the result of the tabu search. There is no long-term memory that would last
  across searcher iterations.
* _Variable neighborhood search_: After collecting the search results from all particles in the swarm, the RWSS values
  will be adjusted, depending on which neighborhood search size showed the best promise for improvement. To do so, all
  samples of the objective function are ordered, and the best improvement as compared to the previous iteration
  determines the swarm's common next iteration's starting vector `pos_0`. If ...
    * ... the improvement was sufficient and was done by the particle with RWSS `rwss_high`, then the next iteration's
      midpoint value `rwss_med` is set to this value. The new values for `rwss_high` and `rwss_low` on the next
      iteration are re-computed from the new midpoint.
    * ... the improvement was sufficient and was done with `rwss_med`, then the values for `rwss_med`, `rwss_low`,
      and `rwss_high`
      remain unchanged.
    * ... the improvement was sufficient and was done with `rwss_low`, then the next iteration's midpoint
      value `rwss_med` is set to this value. The new values for `rwss_high` and `rwss_low` on the next iteration are
      re-computed from the new midpoint.
    * ... nothing was found (i.e., the starting vector `pos_0` still yields the best value for the objective function),
      the new value for `rwss_med` is raised to `rwss_high`.
    * ... the improvement was not sufficient (i.e., the improvement per iteration is too small as compared to targeted
      precision), the values for `rwss_med` remains unchanged, `rwss_high` is raised, and `rwss_low` is lowered.
* An [exponential moving average](https://en.wikipedia.org/wiki/Moving_average) of improvements per iteration allows to
  compare the current iteration's improvement with the trend of the past few. If the current improvement was not
  sufficient, however, the moving average is still in the sufficient range, the number of particles per swarm is
  increased, as well as the number of random walk steps per particle. The cap is set by a factor proportional to the
  dimensionality of the vector space, since this is a known driver of difficulty for finding improvements.
* The values for `rwss_high` (and `rwss_low`) will always be above (and below) `rwss_med` by a certain percentage each.
  The percentage of `rwss_high` above `rwss_med` increases when an iteration doesn't find a sufficient improvement
  during an iteration, as does the percentage of `rwss_low` below `rwss_med`. Once an iteration improvement is
  substantial, the percentages above (and below) will be adjusted towards their default values, together with the number
  of particles per swarm, as well as the number of random walk steps per particle.
* The exact thresholds for when an improvement is "sufficient" or "substantial" are a tuning parameters, as are the
  default RWSS and percentages above (below) for `rwss_high` (`rwss_low`), number of random walk steps, number of
  searchers in a swarm (at least three), the overall total number of independent searchers, and threshold at which a
  candidate `x_c` is to be considered a solution of `f(x) = y`.

# Alternatives considered

## Desired in a future version

* __Simulated annealing__. Searchers will sample the objective function at random positions in the neighborhood of their
  recent position, following a temperature schedule of sorts that reduces the search radius with increasing iteration
  depth. While this method may easily get stuck in local minima of the objective function with no path to an actual
  solution, it may yield results where a more controlled algorithm may falsely assume to be stuck in a local minimum,
  where in fact there is a solution nearby. This may particularly be significant in large dimensions and algebras with
  sharp gradients of the objective function. It would be great to have this as an option available in a future version
  of the code.

## Considered

* __Ant colony optimization__. If there are successive improvements per step within a searcher of a swarm, that searcher
  may be singled out for a deeper random walk. For example, its best point from the initial random walk sequence may
  become a new ad-hoc starting point for a new swarm, each of which perform random walks as in the chosen design. This
  may help in situations where there is a clear gradient towards a solution.
* __Iterated local search__. Rather than trying to escape a local minimum in the objective function by raising
  the `RWSS` values (raising the local temperature, so to speak), it may be more advantageous to just give up on that
  searcher and start over at a new random position. This may help in situations where escape from a local minimum (
  with not path to an actual solution) is slow.
* __Evolutionary computation__. There are many tuning parameters in this algorithm (initial `RWSS`, number of particles
  per swarm, number of random walk steps, thresholds for variable neighborhood search adjustments, to name a few).
  Implement an algorithm that would suggest certain optimized parameters to start with, given an algebra.
* __Guided local search__. This may provide a better way to adjust the variable neighborhood parameters from a given
  lack of improvement of the objective function per iteration.
* __Scatter search__. Rather than starting all particles in a swarm at the previously best position, introduce some
  randomization for the individual particle start positions as well. This may help in situations where there are sharp
  gradients in the objective function that may lead the algorithm to falsely conclude on being stuck in a local minimum
  of the objective function, where in fact a path to a solution exists.

## Decided against

* __Construction heuristics / greedy random seeding / GRASP__. The algorithm should be biased towards exploring the
  entire search space, at the expense of possibly over-representing small solution spaces in the solution-found count (
  as compared to larger, dominant solution spaces). Early optimization seems to counter that desired bias.
* __Gradient descend__. While this may be beneficial in certain well-behaved algebras (like the normed division
  algebras), gradients in the exponential function may become very steep in general algebras. Descending a gradient may
  lead the algorithm in a dominating direction, at the expense of a possibly large detour towards a solution. This is
  already the case in simple number systems, e.g., when calculating `log(1)` in the complexes.
* __Dynamically adjusted polynomial order__. Starting calculation initially using a lower dimension of the polynomial
  could improve computation time, however, the lower the polynomial order the closer the solutions are towards the
  origin. This goes against the desire to explore the entire solution space, and in particular when there is
  periodicity (as e.g. the sine/cosine periodicity of the exponential map in the complexes).

